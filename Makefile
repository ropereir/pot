install:
	make -C pot/build install
	make -C pots/build install
	make -C ompt/build install
	make -C pthread/build install
#	make -C pmpi/build install

clean:
	make -C pot/build clean
	make -C pots/build clean
	make -C ompt/build clean
	make -C pthread/build clean
#	make -C pmpi/build install
