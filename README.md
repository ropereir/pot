# Parallel Object Transitioner (POT)

The POT suite consists of 3 components: the kernel, plugins, and the simulator.

## POT Kernel
The POT Kernel generates and export traces

Useful environment variables
- `POT_DIR` - a string to the directory to write the trace
- `POT_MEMORY` - an unsigned integer, the number of bytes allocated per-thread for events buffering
- `POT_MEMORY_TOUCH` - whether '0' or '1' (default '1') - so the event buffer is written on thread creation to avoid minor page faults when recording later
- `POT_TIME` - 'us' or 'ns' - setting the time measurement precision (default 'ns')
- `POT_CONF` - a configuration file
- `POT_VERBOSE` - an integer for verbosity level (set to 6 for max verbosity)

## POT Plugins

### OMPT
An OMPT tool that forwards OpenMP events to the POT Lib
```bash
OMP_TOOL_LIBRARIES=path/to/libpot_ompt.so ./a.out
```
Interfaces currently supported are:
- parallel regions
- single/masked/master
- task construct
- taskwait construct
- taskgroup
- cancel taskgroup

Unsupported interfaces
- team, teams, league... (the simulator has no such notions, it assumes there is only one team)
- cancelling tasks with a detach clause

### PMPI
A MPI tool that forwards MPI events to the POT Lib.
Set `LD_PRELOAD=$POT/lib/libpot_pmpi.so`

### Pthreads
A library that overrides pthread interface to trace them
Set `LD_PRELOAD=$POT/lib/libpot_pthread.so`

## POT - Simulator (POTS)
A post-processing CLI and environment to analyses traces
```bash
pots -i tracedir -p gantt,time-breakdown --help
```

### HOW TO - create a new analysis pass
- Dupplicate an existing pass file and modify it
- Edit `pots/src/pass.c` and  `pots/src/pass.h` to register your pass

### HOW TO - use configuration files
Generate the configuration file using
```
pots -x conf.json
```
Then edit `conf.json` overriding options as you want, and launch again using
```
POT_CONF=conf.json pots [...]
```

## TODO
Current efforts should focus on
- Tracing the hardware topology and adding a pass that virtualizing it
- Add different level of paralleization in the simulator - and have each pass declare which level of parallelization it supports
    - per-unix-process (= current mode) - events are grouped by unix processes and processed sequentially
    - per-thread - events are grouped by posix threads and processed sequentially
    - per-mpi-process - events are grouped by mpi processes and processed sequentially
- Dev/Ops
    - Add unit tests - at least several LULESH run configuration, which should cover most of the code
    - Automatically increment POT version when pushing
    - Save commit when pushing a new version, so POT print a header with commit hash on launch
    - Add `pots/completions/pots` as part of the installation and in the modulefile
- Quality of life
    - Move configuration from postprocess project to POT kernel
    - Compress trace files
- Features
    - Add support for PAPI event counters
        - add an 'hw-counter' record
        - add passes to perform reduction on hwcounters
    - Expand OMPT interfaces support representing any OpenMP construct
    - Add support for distributed post-processing
    - Add support for Cuda (CUPTI)
    - Improve support for MPI (pmpi project)
