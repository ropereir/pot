#$bots/bin/bash

echo "threads:i:bench:ref:so:pot:tikki:omptrace:omptracing:hpctoolkit:eztrace:donothing"

bots=/home/rpereira/repo/pot/benchmarks/bots-kastors/bots

for threads in 16 ; do
    export OMP_NUM_THREADS=$threads
    export OMP_PLACES=$(python -c "print(','.join(['{' + str(2*i+1) + '}' for i in range($OMP_NUM_THREADS)]))")
    for i in {1..10} ; do
        # for bench in health nqueens sort sparselu strassen uts fib fft alignment ; do
        for bench in                                                                    \
            "128 sort.clang.omp-tasks -n 67108864"                                      \
            "512 nqueens.clang.omp-tasks-manual -n 12"                                  \
            "4 alignment.clang.single-omp-tasks -f $bots/inputs/alignment/prot.100.aa"  \
            "16  strassen.clang.omp-tasks-manual -n 4096"                               \
            "16  fib.clang.omp-tasks-manual -n 36"                                      \
            "512 health.clang.omp-tasks-manual -f $bots/inputs/health/medium.input"     \
            "16 sparselu.clang.single-omp-tasks -n 32 -m 256"                           \
            "512 uts.clang.omp-tasks -f $bots/inputs/uts/tiny.input"                    \
            "512 fft.clang.omp-tasks -n 67108864"                                       \
        ; do

            # parse options
            args=( $bench )
            memory=${args[0]}
            app=${args[1]}

            if test $app = "sort.clang.omp-tasks" && test $threads -eq 1 ; then
                continue ;
            fi

#            module unload llvm/main
#            module load llvm/europar-2024
#
#            export KMP_USE_TIME_BREAKDOWN=1
#            export KMP_TIME_BREAKDOWN_MODE=0
#
#            # REF online instrumentation
#            if ! test $app = "uts.clang.omp-tasks" && ! test $app = "fft.clang.omp-tasks" ; then
#                out=$($bots/bin/$app ${args[@]:2})
#                so=$(echo "$out" | grep "Time Program " | cut -d "=" -f 2 | cut -d " " -f 2 | xargs)
#            else
#                so=
#            fi
#
#            export KMP_USE_TIME_BREAKDOWN=0
#            export KMP_TIME_BREAKDOWN_MODE=0
#
            module unload llvm/europar-2024
            module load llvm/main

            # REF non instrumented
            out=$($bots/bin/$app ${args[@]:2})
            ref=$(echo "$out" | grep "Time Program " | cut -d "=" -f 2 | cut -d " " -f 2 | xargs)

            # POT
            out=$(LD_PRELOAD=$POT/lib/libpot.so POT_DIR=pot-trace POT_MEMORY=$[memory*1024*1024] OMP_TOOL_LIBRARIES=$POT/lib/libpot_ompt.so $bots/bin/$app ${args[@]:2})
            pot=$(echo "$out" | grep "Time Program " | cut -d "=" -f 2 | cut -d " " -f 2 | xargs)
            rm -rf pot-trace

            # Eztrace
            module load eztrace
            out=$(eztrace -t ompt $bots/bin/$app ${args[@]:2})
            eztrace=$(echo "$out" | grep "Time Program " | cut -d "=" -f 2 | cut -d " " -f 2 | xargs)
            rm -rf "${app}_trace"

            # Tikki
            out=$(KAAPI_RECORD_TRACE=1 KAAPI_DISPLAY_PERF=1 KAAPI_RECORD_MASK=omp,compute KAAPI_TASKPERF_EVENTS=work OMP_TOOL_LIBRARIES=/home/rpereira/repo/tikki/build/tool/libtikki.so $bots/bin/$app ${args[@]:2} 2> /dev/null)
            tikki=$(echo "$out" | grep "Time Program " | cut -d "=" -f 2 | cut -d " " -f 2 | xargs)

            # OMPTrace
            if ! test $app = "uts.clang.omp-tasks" ; then
                out=$(OMP_TOOL_LIBRARIES=/home/rpereira/repo/omptrace/build/libomptrace.so $bots/bin/$app ${args[@]:2})
                omptrace=$(echo "$out" | grep "Time Program " | cut -d "=" -f 2 | cut -d " " -f 2 | xargs)
                rm -rf OMPTRACE.graphml
            else
                omptrace=
            fi
#
#            # OMPTracing
#            rm *.json *.gv
#            if ! test $app = "uts.clang.omp-tasks" ; then
#                out=$(OMP_TOOL_LIBRARIES=/home/rpereira/repo/omptracing/build/libomptracing.so $bots/bin/$app ${args[@]:2})
#                omptracing=$(echo "$out" | grep "Time Program " | cut -d "=" -f 2 | cut -d " " -f 2 | xargs)
#            else
#                omptracing=
#            fi
#
            # HPCToolkit
            if ! test $app = "uts.clang.omp-tasks" ; then
                module load libmonitor
                cd /home/rpereira/repo/hpctoolkit/builddir
                out=$(meson devenv hpcrun $bots/bin/$app ${args[@]:2})
                hpctoolkit=$(echo "$out" | grep "Time Program " | cut -d "=" -f 2 | cut -d " " -f 2 | xargs)
                #rm -rf hpctoolkit-*
                cd - > /dev/null
                module unload libmonitor
            else
                hpctoolkit=
            fi

            # Do Nothing
            out=$(OMP_TOOL_LIBRARIES=/home/rpereira/repo/ompt-do-nothing/build/libdonothing_ompt.so $bots/bin/$app ${args[@]:2})
            donothing=$(echo "$out" | grep "Time Program " | cut -d "=" -f 2 | cut -d " " -f 2 | xargs)

            # output
            name=$(echo "$app" | cut -d " " -f 1)

            # Output measurements
            echo "$threads:$i:$name:$ref:$so:$pot:$tikki:$omptrace:$omptracing:$hpctoolkit:$eztrace:$donothing"
        done
    done
done
