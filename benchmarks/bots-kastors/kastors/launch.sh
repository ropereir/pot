#/bin/bash

echo "threads:i:bench:ref:so:pot:tikki:omptrace:omptracing:hpctoolkit:eztrace:donothing"

kastors=/home/rpereira/repo/pot/benchmarks/bots-kastors/kastors

module load python

for threads in 16 ; do
    export OMP_NUM_THREADS=$threads
    export OMP_PLACES=$(python -c "print(','.join(['{' + str(2*i+1) + '}' for i in range($OMP_NUM_THREADS)]))")
    for i in {1..10} ; do

        for bench in                                            \
            "16 strassen/strassen_taskdep -n 8192"              \
            "128 sparselu/sparselu_taskdep -n 128 -m 64"        \
            "128 jacobi/jacobi_block_taskdep -n 32768 -b 256"   \
        ; do
            # parse options
            args=( $bench )
            memory=${args[0]}
            app=${args[1]}

#            module unload llvm/main
#            module load llvm/europar-2024
#
#            export KMP_USE_TIME_BREAKDOWN=1
#            export KMP_TIME_BREAKDOWN_MODE=0
#
#            # REF online instrumentation
#            out=$($kastors/$app ${args[@]:2})
#            so=$(echo "$out" | grep "Experience" | cut -d ":" -f 3 | xargs)
#
#            export KMP_USE_TIME_BREAKDOWN=0
#            export KMP_TIME_BREAKDOWN_MODE=0
#
            module unload llvm/europar-2024
            module load llvm/main

            # REF non instrumented
            out=$($kastors/$app ${args[@]:2})
            ref=$(echo "$out" | grep "Experience" | cut -d ":" -f 3 | xargs)

            # POT
            out=$(POT_DIR=pot-trace POT_MEMORY=$[memory*1024*1024] OMP_TOOL_LIBRARIES=$POT/lib/libpot_ompt.so $kastors/$app ${args[@]:2})
            pot=$(echo "$out" | grep "Experience" | cut -d ":" -f 3 | xargs)
            rm -rf pot-trace

            # EZtrace
            module load eztrace
            out=$(OMP_TOOL_LIBRARIES=$EZTRACE/lib/libeztrace-ompt.so $kastors/$app ${args[@]:2})
            eztrace=$(echo "$out" | grep "Experience" | cut -d ":" -f 3 | xargs)
            rm -rf "${app}_trace"


            # Tikki
            out=$(KAAPI_RECORD_TRACE=1 KAAPI_DISPLAY_PERF=1 KAAPI_RECORD_MASK=omp,compute KAAPI_TASKPERF_EVENTS=work OMP_TOOL_LIBRARIES=/home/rpereira/repo/tikki/build/tool/libtikki.so $kastors/$app ${args[@]:2} 2> /dev/null)
            tikki=$(echo "$out" | grep "Experience" | cut -d ":" -f 3 | xargs)

            # OMPTrace
            if ! test $app = "jacobi/jacobi_block_taskdep" ; then
                out=$(OMP_TOOL_LIBRARIES=/home/rpereira/repo/omptrace/build/libomptrace.so $kastors/$app ${args[@]:2})
                rm -rf OMPTRACE.graphml
            else
                omptrace=
            fi

#            # OMPTracing
#            rm *.json *.gv
#            if ! test $app = "jacobi/jacobi_block_taskdep" ; then
#                out=$(OMP_TOOL_LIBRARIES=/home/rpereira/repo/omptracing/build/libomptracing.so $kastors/$app ${args[@]:2})
#                omptracing=$(echo "$out" | grep "Experience" | cut -d ":" -f 3 | xargs)
#            else
#                omptracing=
#            fi
#
            # HPCToolkit
            module load libmonitor
            cd /home/rpereira/repo/hpctoolkit/builddir
            out=$(meson devenv hpcrun $kastors/$app ${args[@]:2})
            hpctoolkit=$(echo "$out" | grep "Experience" | cut -d ":" -f 3 | xargs)
            rm -rf hpctoolkit-*
            cd - > /dev/null
            module unload libmonitor

            # Do Nothing
            out=$(OMP_TOOL_LIBRARIES=/home/rpereira/repo/ompt-do-nothing/build/libdonothing_ompt.so $kastors/$app ${args[@]:2})
            donothing=$(echo "$out" | grep "Experience" | cut -d ":" -f 3 | xargs)

            # output
            name=$(echo "$app" | cut -d " " -f 1)

            # Output measurements
            echo "$threads:$i:$name:$ref:$so:$pot:$tikki:$omptrace:$omptracing:$hpctoolkit:$eztrace:$donothing"
        done # Kastors
    done # i
done # threads
