import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys

if len(sys.argv) != 2:
    print("usage: plot.py [FILE.CSV]")
    sys.exit(1)

df = pd.read_csv(sys.argv[1], delimiter=":")

#df = pd.read_csv("./results-no-task-schedule.csv", delimiter=":")
#df.dropna()
# df = df[df["bench"] != "jacobi/jacobi_block_taskdep"]
# df = df[df["bench"] != "uts.clang.omp-tasks"]

x = df[df["threads"] == 16].groupby(["threads", "bench"]).mean()

for version in ("ref", "so", "pot", "tikki", "omptrace", "hpctoolkit", "donothing", "omptracing", "eztrace", "thapi"):
    if version not in x:
        continue
    print(version)
    print(round(x[version] / x["ref"], 2))
    print(round((x[version] / x["ref"]).mean(), 3))
#    print((x[version] / x["ref"]).fillna(50).mean().round(2))
