# include <stdio.h>
# include <stdlib.h>
# include <stdint.h>
# include <time.h>

# include <omp.h>

# define CUTOFF (15)

static int
fib(int n)
{
    if (n < 2)
        return n;

    int i, j;

    if (n > CUTOFF)
    {
        #pragma omp task shared(i)
            i = fib(n - 1);

        #pragma omp task shared(j)
            j = fib(n - 2);

        # pragma omp taskwait
    }
    else
    {
        i = fib(n - 1);
        j = fib(n - 2);
    }

    return i + j;
}

static int
cmpint(const void * a, const void * b)
{
    return *((double *)a) < *((double *)b) ? -1 : 1;
}

# define ITER (20)

int
main(int argc, char ** argv)
{
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s [N]\n", argv[0]);
        return 1;
    }

    int n = atoi(argv[1]);

    // collect times
    double times[ITER] = { 0 };

    #pragma omp parallel
    {
        #pragma omp single
        {
            // warm up
            fib(n);

            for (int i = 0 ; i < ITER ; ++i)
            {
                double t0 = omp_get_wtime();
                fib(n);
                double tf = omp_get_wtime();
                times[i] = tf - t0;
                printf("times[%d] = %lf\n", i, times[i]);
            }
        }
    }

    // show results
    qsort(times, ITER, sizeof(double), cmpint);

    // normal law (mu, sigma)
    double mu = 0;
    for (int i = 0 ; i < ITER ; ++i)
        mu += times[i];
    mu = mu / (double) ITER;

    double sigma = 0;
    for (int i = 0 ; i < ITER ; ++i)
    {
        sigma += (times[i] - mu) * (times[i] - mu);
    }
    sigma = sigma / (double) (ITER - 1);

    printf("(mu, sigma) = (%lf, %lf) ; min=%lf ; med=%lf ; max=%lf\n", mu, sigma, times[0], times[ITER / 2], times[ITER - 1]);



    return 0;
}
