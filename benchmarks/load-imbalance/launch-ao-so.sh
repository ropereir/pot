#!/bin/bash

echo "g:expected_work:expected_overhead:expected_idle:pot_work:pot_overhead:pot_idle"
#for g in 1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 ; do
#for g in 1 2 4 8 16 32 64 128 256 512 1024 ; do
for g in 1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 ; do

    #out=$(POT_DIR=$trace POT_MEMORY=$[16 * 1024 * 1024] OMP_TOOL_LIBRARIES=$POT/lib/libpot_ompt.so ./a.out $g | grep "be close to")

    r=$(KMP_USE_TIME_BREAKDOWN=1 KMP_TIME_BREAKDOWN_MODE=0 ./a.out $g)

    out=$(echo "$r" | grep "be close to")
    work=$(echo "$out" | cut -d "(" -f 3 | cut -d "," -f 1)
    overhead=0
    idle=$(echo "$out" | cut -d "(" -f 3 | cut -d " " -f 2 | tr -d ")")

    out=$(echo "$r" | grep "with mode")
    w=$(echo "$out" | cut -d "=" -f 2 | cut -d "," -f 1)
    o=$(echo "$out" | cut -d "=" -f 3 | cut -d "," -f 1)
    i=$(echo "$out" | cut -d "=" -f 4 | cut -d " " -f 1)

    echo "$g:$work:$overhead:$idle:$w:$o:$i"
done

# for g in 1 2 4 8 16 32 64 128 256 512 1024 ; do
for g in 1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 ; do

    #out=$(POT_DIR=$trace POT_MEMORY=$[16 * 1024 * 1024] OMP_TOOL_LIBRARIES=$POT/lib/libpot_ompt.so ./a.out $g | grep "be close to")

    r=$(KMP_USE_TIME_BREAKDOWN=1 KMP_TIME_BREAKDOWN_MODE=1 ./a.out $g)

    out=$(echo "$r" | grep "be close to")
    work=$(echo "$out" | cut -d "(" -f 3 | cut -d "," -f 1)
    overhead=0
    idle=$(echo "$out" | cut -d "(" -f 3 | cut -d " " -f 2 | tr -d ")")

    out=$(echo "$r" | grep "with mode")
    w=$(echo "$out" | cut -d "=" -f 2 | cut -d "," -f 1)
    o=$(echo "$out" | cut -d "=" -f 3 | cut -d "," -f 1)
    i=$(echo "$out" | cut -d "=" -f 4 | cut -d " " -f 1)

    echo "$g:$work:$overhead:$idle:$w:$o:$i"
done
