#!/bin/bash

bin=./a.out

export OMP_NUM_THREADS=8

echo "g:expected_work:expected_overhead:expected_idle:work:overhead:idle"
#for g in 1 2 4 8 16 32 64 128 256 512 1024 ; do # 2048 4096 8192 16384 32768 65536 131072 ; do
for g in 1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 ; do
    trace="pot-trace-$(hostname)"
    rm -rf $trace

    # echo "POT_DIR=$trace POT_MEMORY=$[16 * 1024 * 1024] OMP_TOOL_LIBRARIES=$POT/lib/libpot_ompt.so $bin $g"
    out=$(POT_DIR=$trace POT_MEMORY=$[16 * 1024 * 1024] OMP_TOOL_LIBRARIES=$POT/lib/libpot_ompt.so $bin $g | grep "be close to")
    work=$(echo "$out" | cut -d "(" -f 3 | cut -d "," -f 1)
    overhead=0
    idle=$(echo "$out" | cut -d "(" -f 3 | cut -d " " -f 2 | tr -d ")")

    out=$(pots -i $trace -p time-breakdown | grep "(work, overhead, idle)")
    pot=$(echo "$out" | cut -d "=" -f 3 | tr -d " " | tr "," ":" | tr -d "(" | tr -d ")")

    echo "$g:$work:$overhead:$idle:$pot"
done
