# include <omp.h>
# include <stdio.h>
# include <stdlib.h>
# include <time.h>

# define ITER (10000)

static inline uint64_t
get_nanotime(void)
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (uint64_t)(ts.tv_sec * 1000000000) + (uint64_t) ts.tv_nsec;
}

int
main(int argc, char ** argv)
{
    uint64_t ns = 512;
    if (argc == 2)
        ns = strtoul(argv[1], NULL, 10);

    printf("g (ns.)=%lu\n", ns);

    uint64_t n = 0;
    # pragma omp parallel
    {
        # pragma omp single nowait
            n = omp_get_num_threads();

        for (int iter = 0 ; iter < ITER ; ++iter)
        {
            int i = omp_get_thread_num();
            # pragma omp task default(none) firstprivate(i) shared(ns)
            {
                uint64_t t0 = get_nanotime();
                uint64_t target_ns = (i+1) * ns;
                while ((get_nanotime() - t0) < target_ns);
            }

            # pragma omp barrier
        }
    }
    printf("both (work, idle)  should be close to (%lf, %lf) s.\n", ITER*n*(n+1)/2 * ns / 1e9, ITER*n*(n-1)/2 * ns / 1e9);
    return 0;
}
