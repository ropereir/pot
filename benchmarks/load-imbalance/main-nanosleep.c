# include <omp.h>
# include <stdio.h>
# include <time.h>

# define ITER (10000)
# define MAX_THREADS (128)

int
main(int argc, char ** argv)
{
    int US = 10;
    if (argc == 2)
        US = atoi(argv[1]);

    printf("US=%d\n", US);

    struct timespec ts[MAX_THREADS];
    for (int i = 0 ; i < MAX_THREADS ; ++i)
    {
        ts[i].tv_sec = ((i + 1) * US) / 1000000;
        ts[i].tv_nsec = (((i + 1) * US) % 1000000) * 1000;
    }

    int n = 0;
    # pragma omp parallel
    {
        # pragma omp single
        {
            n = omp_get_num_threads();

            for (int iter = 0 ; iter < ITER ; ++iter)
            {
                for (int i = 0 ; i < n ; ++i)
                    # pragma omp task default(none) firstprivate(i) shared(ts)
                        nanosleep(ts + i, NULL);

                # pragma omp taskwait
            }
        }
    }
    printf("both (work, idle)  should be close to (%lf, %lf) s.\n", ITER*n*(n+1)/2 * US / 1e6, ITER*n*(n-1)/2 * US / 1e6);
    return 0;
}
