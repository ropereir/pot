# include <omp.h>
# include <stdio.h>
# include <unistd.h>
# include <time.h>

# define ITER (10000)

static inline uint64_t
get_nanotime(void)
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (uint64_t)(ts.tv_sec * 1000000000) + (uint64_t) ts.tv_nsec;
}

# define GET_NANOTIME_COST_NS (22)

# define WORK(I)    do {                    \
                        uint32_t x = 0;     \
                        while (++x < I);    \
                    } while (0)

// approximate the number of work iterations required to execute 'us' of work
static inline uint64_t
compute_work_iter(uint64_t ns)
{
    // rough calibration
    uint64_t test_iter = 1000000000;
    WORK(test_iter);

    uint64_t t0 = get_nanotime();
    WORK(test_iter);
    uint64_t tf = get_nanotime();

    uint64_t dt = tf - t0;
    double ns_per_iter = dt / (double)test_iter;

    uint64_t iter = (uint64_t) (ns / (double)ns_per_iter);

    t0 = get_nanotime();
    WORK(iter);
    tf =  get_nanotime();

    printf("work takes %lu ns\n", tf - t0);
    return iter;
}

# define MAX_THREADS (512)

int
main(int argc, char ** argv)
{
    uint64_t ns = 16;
    if (argc == 2)
        ns = strtoul(argv[1], NULL, 10);
    printf("g (ns) = %lu\n", ns);

    uint64_t work_iter = compute_work_iter(ns);
    printf("work_iter=%lu\n", work_iter);

    double work_per_threads[MAX_THREADS] = {0};

    uint64_t n;
    uint64_t t0 = get_nanotime();
    # pragma omp parallel
    {
        # pragma omp single
        {
            n = omp_get_num_threads();
            for (int iter = 0 ; iter < ITER ; ++iter)
            {
                for (int i = 0 ; i < n ; ++i)
                    # pragma omp task firstprivate(i)
                        WORK((i+1) * work_iter);

                # pragma omp taskwait
            }
        }
    }
    uint64_t tf = get_nanotime();
    uint64_t dt = tf - t0;

    printf("total time = %lf s.\n", dt * 1e-9);
    printf("both (work, idle)  should be close to (%lf, %lf) s.\n", ITER*n*(n+1)/2 * ns / 1e9, ITER*n*(n-1)/2 * ns / 1e9);

    return 0;
}
