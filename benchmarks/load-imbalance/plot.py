import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys

if len(sys.argv) != 2:
    print("usage: {} [CSV-FILE]".format(sys.argv[0]))
    sys.exit(1)

df = pd.read_csv(sys.argv[1], delimiter=":")
dfmed = df.groupby(["g"]).median()
dfmin = df.groupby(["g"]).min()
dfmax = df.groupby(["g"]).max()

N_POINTS=16
X0=16

fig, ax = plt.subplots()

ax.set_xscale('log', base=2)
ax.set_yscale('log', base=2)

X = np.array(df["g"])
Ys = [
    dfmed["work"],
    dfmed["idle"],
    dfmed["overhead"],
    dfmed["expected_work"],
    dfmed["expected_idle"],
]

labels = [
    "work",
    "idle",
    "overhead",
    "expected_work",
    "expected_idle"
]

for i in range(len(labels)):
    # ax.errorbar(X, Ys[i], label=labels[i], yerr=Yerr[i])
    ax.plot(X, Ys[i], label=labels[i])

ax.set_xticks(X)
ax.set_xlabel("Granularity g (in ns.)")

ax.set_ylabel("Time (in s.)")
ax.legend()





fig, ax = plt.subplots()

ax.set_xscale('log', base=2)

Ys = [
    100.0 * abs(dfmed["work"] - dfmed["expected_work"]) / dfmed["expected_work"],
    100.0 * abs(dfmed["idle"] - dfmed["expected_idle"]) / dfmed["expected_idle"],
]

labels = [
    "work",
    "idleness",
]

for i in range(len(labels)):
    ax.plot(X, Ys[i], label=labels[i])

print(Ys)
ax.set_xticks(X)
ax.set_xlabel("Granularity g (in ns.)")

ax.set_ylabel("Divergence (%)")
ax.set_ylim(0, 100)

ax.legend()



plt.show()
