import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

dfx=pd.read_csv("./results-s80.csv", delimiter=":")

# mask = dfx["threads"] == 1
# dfx = dfx[~mask]
# mask = dfx["t"] == 1
# dfx = dfx[~mask]

# app:trace:precision:threads:s:i:t:anomalies:work:overhead:idle:ntasks_total:ntasks_explicit:edges_max:edges_reported:elapsed

for app in ("lulesh-omp-t-clang++","lulesh-omp-t-mpc_cxx"):
#for app in ("lulesh-omp-t-clang++",):

    dfapp = dfx[dfx["app"] == app]

    for s in (80,):

        dfs=dfapp[dfapp["s"] == s]

        #for precision in ("us", "ns"):
        for precision in ("ns",):

            dfs=dfs[dfs["precision"] == precision]

            # WITH TRACING ON
            df=dfs[dfs["trace"] == "on"]

            X=np.array(df["threads"], dtype=int)
            Y=np.array(df["t"], dtype=int)

            work=np.array(df["work"])
            idle=np.array(df["idle"])
            overhead=np.array(df["overhead"])
            edges_max=np.array(df["edges_max"])
            edges_reported=np.array(df["edges_reported"])
            elapsed_time=np.array(df["elapsed"])
            ntasks=np.array(df["ntasks_explicit"])

            # WITH TRACING OFF
            df=dfs[dfs["trace"] == "off"]
            ref_elapsed_time=np.array(df["elapsed"])

            # generate curves
            xs=sorted(list(set(X)))
            ys=sorted(list(set(Y)))

            WORK_PERCENT = np.zeros((len(ys), len(xs)))
            IDLE_PERCENT = np.zeros((len(ys), len(xs)))
            OVERHEAD_PERCENT = np.zeros((len(ys), len(xs)))
            TOTAL_TIME = np.zeros((len(ys), len(xs)))
            WC_TIME = np.zeros((len(ys), len(xs)))
            EDGES_PERCENT = np.zeros((len(ys), len(xs)))
            ELAPSED_TIME_DIFF = np.zeros((len(ys), len(xs)))
            ELAPSED_TIME = np.zeros((len(ys), len(xs)))
            ELAPSED_TIME_RATIO = np.zeros((len(ys), len(xs)))
            AVG_WORK = np.zeros((len(ys), len(xs)))

            for i in range(len(X)):
                x = X[i] - 1
                y = Y[i] // 8
                TOTAL_TIME[y][x] = work[i] + idle[i] + overhead[i]
                WC_TIME[y][x] = (work[i] + idle[i] + overhead[i]) / (x + 1)
                WORK_PERCENT[y][x] = 100 * work[i] / (work[i] + idle[i] + overhead[i])
                IDLE_PERCENT[y][x] = 100 * idle[i] / (work[i] + idle[i] + overhead[i])
                OVERHEAD_PERCENT[y][x] = 100 * overhead[i] / (work[i] + idle[i] + overhead[i])
                EDGES_PERCENT[y][x] = 100 * edges_reported[i] / edges_max[i]
                ELAPSED_TIME_DIFF[y][x] = elapsed_time[i] - ref_elapsed_time[i]
                ELAPSED_TIME_RATIO[y][x] = elapsed_time[i] / ref_elapsed_time[i]
                ELAPSED_TIME[y][x] = elapsed_time[i]
                AVG_WORK[y][x] = work[i] / ntasks[i]

            # cvals  = [0.020899, 19, 24.0742]
            # colors = ["white","white","black"]
            # norm   = plt.Normalize(min(cvals),max(cvals))
            # tuples = list(zip(map(norm,cvals), colors))
            # cmap   = matplotlib.colors.LinearSegmentedColormap.from_list("", tuples)

            plots = [
                {
                    'data': WORK_PERCENT,
                    'label': "Work time %",
                    'vmin': 0,
                    'vmax': 100,
                },
                {
                    'data': IDLE_PERCENT,
                    'label': "Idle time %",
                    'vmin': 0,
                    'vmax': 100,
                },
                {
                    'data': OVERHEAD_PERCENT,
                    'label': "Overhead time %",
                    'vmin': 0,
                    'vmax': 100,
                },
                {
                    'data': EDGES_PERCENT,
                    'label': "Edges %",
                    'vmin': 0,
                    'vmax': 100,
                },
                {
                    'data': TOTAL_TIME,
                    'label': 'Cumulated time total (s.)',
                    'vmin': None,
                    'vmax': None,
                },
                {
                    'data': WC_TIME,
                    'label': 'Cumulated time averaged per thread (s.)',
                    'vmin': None,
                    'vmax': None,
                },
                {
                    'data': ELAPSED_TIME_DIFF,
                    'label': 'Slowdown to non-instrumented execution (in s.)',
                    'vmin': None,
                    'vmax': None,
                },
                {
                    'data': ELAPSED_TIME_RATIO,
                    'label': 'Slowdown to non-instrumented execution (in %)',
                    'vmin': None,
                    'vmax': None,
                },
                {
                    'data': ELAPSED_TIME,
                    'label': 'Elapsed time reported by LULESH (in s.)',
                    'vmin': None,
                    'vmax': None,
                },
                {
                    'data': AVG_WORK,
                    'label': 'Average work per task (in s.)',
                    'vmin': None,
                    'vmax': None,
                },
            ]

            fig_id = 0
            for plot in plots:
                cmap = "hot"

                fig, ax = plt.subplots()
                interpolation='nearest'
                #interpolation='bilinear'
                #interpolation='gaussian'
                heatmap = ax.imshow(plot['data'], cmap=cmap, interpolation=interpolation, vmin=plot['vmin'], vmax=plot['vmax'])

                # levels=[0, 20, 40, 60, 80]
                # ctr = ax.contour(plot['data'], levels, colors='k', origin='upper')

                # Show all ticks and label them with the respective list entries
                ax.set_xlabel("threads")
                xl = [0] + list(range(3, len(xs), 2))
                ax.set_xticks(xl)
                ax.set_xticklabels([str(xs[i]) for i in xl])

                ax.set_ylabel("t")
                yl = [0] + list(range(3, len(ys), 2))
                ax.set_yticks(yl)
                ax.set_yticklabels([str(ys[i]) for i in yl])
                ax.invert_yaxis()

                cbar = fig.colorbar(heatmap)
                cbar.set_label(plot['label'])

                ax.title.set_text("{} - {} \nfor s={} and precision={}".format(plot["label"], app, s, precision))

                #fig.savefig("figures/{}-{}-{}.svg".format(app, s, fig_id))
                fig_id += 1
plt.show()
