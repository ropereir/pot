#!/bin/bash

# CONF
dst=/home/rpereira/repo/pot/benchmarks/lulesh/with-pot/results
appdir=/home/rpereira/repo/LULESH/
tool=/home/rpereira/install/pot/lib/libpot_ompt.so
postprocess=/home/rpereira/install/pot/bin/pot_post_process

# LAUNCH
passes=time-breakdown,stats
export OMP_TOOL_LIBRARIES=$tool
export POT_MEMORY=$[2 * 1024*1024*1024 + 0 * 1024*1024 + 0 * 1024]

echo "app:trace:precision:threads:s:i:t:anomalies:work:overhead:idle:ntasks_total:ntasks_explicit:edges_max:edges_reported:elapsed"

for appname in lulesh-omp-t-clang++ lulesh-omp-t-mpc_cxx ; do
#for appname in lulesh-omp-t-mpc_cxx ; do
#for appname in lulesh-omp-t-clang++ ; do
    app=$appdir/$appname
    #for precision in us ns ; do
    for precision in ns ; do
        export POT_TIME=$precision
        for threads in {1..32..1} ; do
            if test $threads -le 16 ; then
                places=$(python -c "print(','.join(['{' + str(2*i) + '}' for i in range($threads)]))")
            else
                places=$(python -c "print(','.join(['{' + str(2*i) + '}' for i in range(16)]) + ',' + ','.join(['{' + str(2*i+1) + '}' for i in range($threads-16)]))")
            fi

#            for s in 10 60 100 ; do
            for s in 80 ; do
                for i in 128 ; do
                    for t in 1 {8..256..8} ; do
                        dir=$dst/$appname/$precision-$threads-$s-$i-$t
                        export POT_DIR=$dir/pot
                        if ! test -f $dir/trace-out || ! test -f $dir/out || ! test -s $dir/trace-out || ! grep -q "work, overhead, idle" $dir/trace-out ; then
                            echo "Running on $dir"
                            rm -rf $dir
                            mkdir -p $POT_DIR
                            echo "OMP_NUM_THREADS=$threads OMP_PLACES=\"$places\" $app -s $s -i $i -p -tel $t -tnl $t > $dir/out 2> $dir/err" > $dir/cmd
                            OMP_NUM_THREADS=$threads OMP_PLACES="$places" $app -s $s -i $i -p -tel $t -tnl $t > $dir/out 2> $dir/err
                        fi
                        if ! test -f $dir/trace-out ; then
                            echo "Post processing on $dir"
                            OMP_TOOL_LIBRARIES= $postprocess -i $POT_DIR -p $passes > $dir/trace-out 2> $dir/trace-err
                            if grep "ANOMALY" $dir/trace-out > /dev/null 2> /dev/null ; then
                                echo "DETECTED ANOMALY !"
                            fi
                            rm -rf $POT_DIR
                        else
                            content=$(cat $dir/trace-out)
                            anomalies=$(echo "$content" | grep "ANOMALY" | wc -l)
                            breakdown=$(echo "$content" | grep "time breakdown pass" | cut -d "=" -f 2 | tr -d " " | tr "," ":" | tr -d "(" | tr -d ")")
                            tasks=$(echo "$content" | grep "tasks - (total, explicit)" | cut -d "=" -f 2 | tr -d " " | tr "," ":" | tr -d "(" | tr -d ")")
                            edges=$(echo "$content" | grep "edges - (maximum, reported)" | cut -d "=" -f 2 | tr -d " " | tr "," ":" | tr -d "(" | tr -d ")")
                            time=$(cat $dir/out | grep Elapsed | cut -d ":" -f 3 | xargs)
                            echo "$appname:on:$precision:$threads:$s:$i:$t:$anomalies:$breakdown:$tasks:$edges:$time"
                        fi # trace-out exists
                    done # t
                done # i
            done # s
        done # $precision
    done # $threads
done # $appname
