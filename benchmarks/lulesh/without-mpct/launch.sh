#!/bin/bash

# CONF
dst=/home/rpereira/repo/pot/benchmarks/lulesh/without-pot/results
appdir=/home/rpereira/repo/LULESH/

# LAUNCH
echo "app:trace:precision:threads:s:i:t:anomalies:work:overhead:idle:ntasks_total:ntasks_explicit:edges_max:edges_reported:elapsed"

for appname in lulesh-omp-t-clang++ lulesh-omp-t-mpc_cxx ; do
#for appname in lulesh-omp-t-mpc_cxx ; do
#for appname in lulesh-omp-t-clang++ ; do
    app=$appdir/$appname
    #for precision in us ns ; do
    for precision in ns ; do
        export POT_TIME=$precision
        for threads in {1..32..1} ; do
            if test $threads -le 16 ; then
                places=$(python -c "print(','.join(['{' + str(2*i) + '}' for i in range($threads)]))")
            else
                places=$(python -c "print(','.join(['{' + str(2*i) + '}' for i in range(16)]) + ',' + ','.join(['{' + str(2*i+1) + '}' for i in range($threads-16)]))")
            fi

#            for s in 10 60 100 ; do
            for s in 80 ; do
                for i in 128 ; do
                    for t in 1 {8..256..8} ; do
                        dir=$dst/$appname/$precision-$threads-$s-$i-$t
                        export POT_DIR=$dir/pot
                        if ! test -f $dir/out || ! grep -q "Elapsed" $dir/out ; then
                            echo "Running on $dir"
                            rm -rf $dir
                            mkdir -p $POT_DIR
                            echo "OMP_NUM_THREADS=$threads OMP_PLACES=\"$places\" $app -s $s -i $i -p -tel $t -tnl $t > $dir/out 2> $dir/err" > $dir/cmd
                            OMP_NUM_THREADS=$threads OMP_PLACES="$places" $app -s $s -i $i -p -tel $t -tnl $t > $dir/out 2> $dir/err
                        else
                            breakdown="0:0:0"
                            tasks="0:0"
                            edges="0:0"
                            time=$(cat $dir/out | grep "Elapsed" | cut -d ":" -f 3 | xargs)
                            echo "$appname:off:$precision:$threads:$s:$i:$t:$anomalies:$breakdown:$tasks:$edges:$time"
                        fi # file exists
                    done # t
                done # i
            done # s
        done # $precision
    done # $threads
done # $appname
