#!/bin/bash

export OMPI_MCA_btl_tcp_rndv_eager_limit=128
export OMPI_MCA_btl_tcp_eager_limit=128

#for mode in ISEND ISEND_MULTIPLE PARTITIONNED ; do
for mode in PARTITIONNED ISEND_MULTIPLE ; do
    for nbytes in 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 8388608 16777216 33554432 67108864 134217728 268435456 536870912 1073741824 ; do

            for i in {1..10} ; do

            #OMPI - same node
            #mpirun -machinefile $OAR_NODEFILE -bind-to numa -np 2 --mca orte_base_help_aggregate 0 --mca btl self,vader,tcp ./launch.sh ./main

            #OMPI - distinct nodes
            mpirun -machinefile $OAR_NODEFILE -bind-to socket -npersocket 1 --mca orte_base_help_aggregate 0 --mca btl self,vader,tcp ./run.sh $mode $nbytes

            #MPICH
            #mpirun -machinefile $OAR_NODEFILE -np 2 ./launch.sh ./main

            if test $? -ne 0 ; then
                echo "Error: failed to launch one"
                exit 1
            fi
        done

    done
done
