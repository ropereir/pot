# define _GNU_SOURCE
# include <sched.h>
# include <unistd.h>

# ifdef USE_POT
#  include <pot/records.h>
# endif /* USE_POT */

# include <assert.h>
# include <omp.h>
# include <mpi.h>
# include <stdatomic.h>
# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <stdint.h>
# include <time.h>

# include "progress-thread.h"

# define SENDER_RANK        (0)
# define RECEIVER_RANK      (1)
# define TAG                (0)
# define ITERATIONS         (1)
# define WORK_FACTOR        (1000)
# define PRINT_SENDER       (0)
# define PARTS_PER_THREAD   (32)

typedef enum    bench_mode_e
{
    BENCH_MODE_ISEND            = 0,
    BENCH_MODE_ISEND_MULTIPLE   = 1,
    BENCH_MODE_PARTITIONNED     = 2,
    BENCH_MODE_MAX              = 3,
}               bench_mode_t;

static const char * mode_str[] = {
    "ISEND",
    "ISEND_MULTIPLE",
    "PARTITIONNED",
    "MAX",
};

static uint64_t
get_nanotime(void)
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (uint64_t)(ts.tv_sec * 1000000000) + (uint64_t) ts.tv_nsec;
}

/**
 *  Represents a workload to fill the i-th part of 'buffer' of total size 'nbytes'
 */
static inline void
work(char * buffer, const size_t nbytes, const int i, const size_t nparts)
{
    const size_t bs     = nbytes / nparts;
    const size_t from   = i * bs;
    const size_t length = bs;
    for (int i = WORK_FACTOR ; i >= 0 ; --i)
        memset(buffer + from, i, length);
}

static inline void
check_error(const char * label, int err)
{
    if (err)
    {
        char s[MPI_MAX_ERROR_STRING];
        int len;
        MPI_Error_string(err, s, &len);
        printf("%s failed with errcode=`%s`\n", label, s);
        fflush(stdout);
    }
}

static void
usage(char ** argv)
{
    fprintf(stderr, "usage: %s [nbytes] [mode]\n", argv[0]);
    fprintf(stderr, "  with [mode] in ");
    for (int i = 0 ; i < BENCH_MODE_MAX ; ++i)
    {
        fprintf(stderr, "%s", mode_str[i]);
        if (i != BENCH_MODE_MAX - 1)
            fprintf(stderr, ",");
    }
    fprintf(stderr, "\n");
}

int
main(int argc, char ** argv)
{
    if (argc != 3)
    {
        usage(argv);
        return 1;
    }
    const size_t nbytes = (size_t) atoi(argv[1]);
    const char * mode_s = argv[2];
    printf("Running with nbytes=%lu and mode=%s\n", nbytes, mode_s);

    int mode = -1;
    for (int i = 0 ; i < BENCH_MODE_MAX ; ++i)
    {
        if (strcmp(mode_str[i], mode_s) == 0)
        {
            mode = i;
            break ;
        }
    }
    if (mode == -1 || nbytes <= 0)
    {
        usage(argv);
        return 1;
    }

    const int required = MPI_THREAD_MULTIPLE;
    int provided;
    MPI_Init_thread(&argc, &argv, required, &provided);
    if (required != provided)
    {
        printf("MPI does not support `MPI_THREAD_MULTIPLE`\n"); fflush(stdout);
        exit(1);
    }

    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    if (size != 2)
    {
        printf("You should run with exactly 2 MPI processes\n");
        exit(1);
    }

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    # if USE_POT
    if (rank != SENDER_RANK)
        pot_pause();
    # endif

    # if 0
    if (rank == SENDER_RANK)
    {
        printf("nthreads:nbytes:nparts:iteration:mode:twork:tsend:trecv\n");
        fflush(stdout);
    }
    # endif

    MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);

    if (rank == SENDER_RANK)
        progress_thread_start();

    char hostname[512];
    gethostname(hostname, sizeof(hostname));

    MPI_Barrier(MPI_COMM_WORLD);
    # pragma omp parallel
    {
        # pragma omp masked
        {
            MPI_Barrier(MPI_COMM_WORLD);
            const size_t nparts = PARTS_PER_THREAD * omp_get_num_threads();
            // for (unsigned int i = 0 ; i < sizeof(MODES_TO_USE) / sizeof(bench_mode_t) ; ++i)
            {
                // bench_mode_t mode = MODES_TO_USE[i];
                // for (size_t n = 0 ; n < sizeof(nbytes_values) / sizeof(size_t) ; ++n)
                {
                    // const size_t nbytes = nbytes_values[n];
                    // if (nbytes < nparts)
                    //    continue ;
                    if (nbytes % nparts || nbytes < nparts)
                    {
                        fprintf(stderr, "error nbytes=%lu nparts=%lu\n", nbytes, nparts);
                        exit(0);
                    }

                    char * buffer = (char *) malloc(nbytes);
                    assert(buffer);

                    const size_t bs = nbytes / nparts;

                    for (int iter = 0 ; iter <= ITERATIONS ; ++iter)
                    {
                        MPI_Barrier(MPI_COMM_WORLD);

                        # if USE_POT
                        if      (iter == 0) pot_pause();
                        else if (iter == 1) pot_resume();
                        # endif /* USE_POT */

                        if (rank == SENDER_RANK)
                        {
                            progress_thread_reset();

                            uint64_t t0, t1, t2;
                            (void) t0;
                            (void) t1;
                            (void) t2;

                            # if PRINT_SENDER
                            uint64_t tinit = get_nanotime();
                            # endif

                            switch (mode)
                            {
                                case (BENCH_MODE_PARTITIONNED):
                                {
                                    MPI_Request req;

                                    # pragma omp taskgroup
                                    {
                                        # pragma omp task depend(out: req) shared(req, t0, t1)
                                        {
                                            t0 = t1 = get_nanotime();
                                            int err = MPI_Psend_init(buffer, nparts, bs, MPI_CHAR, RECEIVER_RANK, TAG, MPI_COMM_WORLD, MPI_INFO_NULL, &req);
                                            check_error("MPI_Psend_init", err);

                                            err = MPI_Start(&req);
                                            check_error("MPI_Start", err);
                                        }

                                        omp_event_handle_t hdl;
                                        # pragma omp task depend(in: req) shared(req) detach(hdl)
                                            progress_thread_detach(&req, hdl);

                                        for (size_t part = 0 ; part < nparts ; ++part)
                                        {
                                            # pragma omp task depend(in: req) depend(inoutset: buffer) default(none) shared(req) firstprivate(buffer, nbytes, part, nparts)
                                            {
                                                work(buffer, nbytes, part, nparts);
                                                int err = MPI_Pready(part, req);
                                                check_error("MPI_Pready", err);
                                            }
                                        }
                                    }

                                    break ;
                                }

                                case (BENCH_MODE_ISEND):
                                {
                                    MPI_Request req;

                                    # pragma omp taskgroup
                                    {
                                        t0 = get_nanotime();

                                        for (size_t part = 0 ; part < nparts ; ++part)
                                        {
                                            # pragma omp task depend(inoutset: buffer) firstprivate(part, nbytes, buffer, nparts)
                                                work(buffer, nbytes, part, nparts);
                                        }

                                        omp_event_handle_t hdl;
                                        # pragma omp task depend(in: buffer) shared(t1, req) detach(hdl)
                                        {
                                            t1 = get_nanotime();
                                            int err = MPI_Isend(buffer, nbytes, MPI_CHAR, RECEIVER_RANK, TAG, MPI_COMM_WORLD, &req);
                                            check_error("MPI_Isend", err);
                                            progress_thread_detach(&req, hdl);
                                        }
                                    }

                                    break ;
                                }

                                case (BENCH_MODE_ISEND_MULTIPLE):
                                {
                                    MPI_Request reqs[nparts];
                                    # pragma omp taskgroup
                                    {
                                        t0 = t1 = get_nanotime();
                                        for (size_t part = 0 ; part < nparts ; ++part)
                                        {
                                            omp_event_handle_t hdl;
                                            # pragma omp task detach(hdl) shared(reqs) firstprivate(part, bs)
                                            {
                                                work(buffer, nbytes, part, nparts);

                                                MPI_Request * req = reqs + part;
                                                int err = MPI_Send_init(buffer + part*bs, bs, MPI_CHAR, RECEIVER_RANK, TAG + part, MPI_COMM_WORLD, req);
                                                check_error("MPI_Send_init", err);

                                                MPI_Start(req);
                                                check_error("MPI_Start", err);

                                                progress_thread_detach(req, hdl);
                                            }
                                        }
                                    }

                                    for (size_t part = 0 ; part < nparts ; ++part)
                                        MPI_Request_free(reqs + part);

                                    break ;
                                }

                                default:
                                    abort();

                            }

                            # if PRINT_SENDER
                            t2 = get_nanotime();

                            /* reduction to get receive completion local time */
                            double recv_time;
                            MPI_Status status;
                            int err = MPI_Recv(&recv_time, 1, MPI_DOUBLE, RECEIVER_RANK, TAG, MPI_COMM_WORLD, &status);
                            check_error("MPI_Recv", err);

                            /* show results for iter > 0 */
                            if (iter != 0)
                            {
                                printf("%d:%10lu:%lu:%2d:%16s:%lf:%lf:%lf:%lf\n", omp_get_num_threads(), nbytes, nparts, iter, mode_str[mode], (t1 - t0) / 1e9, (t2 - t1) / 1e9, recv_time, (t2-tinit) / 1e9);
                                fflush(stdout);
                            }
                            # endif
                        }
                        else /* rank == RECEIVER_RANK */
                        {
                            assert(rank == RECEIVER_RANK);
                            uint64_t t3 = get_nanotime();

                            MPI_Status status;
                            int err;

                            switch (mode)
                            {
                                case (BENCH_MODE_PARTITIONNED):
                                {
                                    MPI_Request req;

                                    err = MPI_Precv_init(buffer, 1, nbytes, MPI_CHAR, SENDER_RANK, TAG, MPI_COMM_WORLD, MPI_INFO_NULL, &req);
                                    check_error("MPI_Precv_init", err);

                                    MPI_Start(&req);
                                    check_error("MPI_Start", err);

                                    err = MPI_Wait(&req, &status);
                                    check_error("MPI_Wait", err);

                                    break ;
                                }

                                case (BENCH_MODE_ISEND):
                                {
                                    err = MPI_Recv(buffer, nbytes, MPI_CHAR, SENDER_RANK, TAG, MPI_COMM_WORLD, &status);
                                    check_error("MPI_Recv", err);
                                    break ;
                                }

                                case (BENCH_MODE_ISEND_MULTIPLE):
                                {
                                    MPI_Request reqs[nparts];
                                    for (size_t part = 0 ; part < nparts ; ++part)
                                    {
                                        err = MPI_Irecv(buffer+part*bs, bs, MPI_CHAR, SENDER_RANK, TAG + part, MPI_COMM_WORLD, reqs + part);
                                        check_error("MPI_Recv", err);
                                    }

                                    MPI_Waitall(nparts, reqs, MPI_STATUSES_IGNORE);
                                    break ;
                                }

                                default:
                                    abort();

                            } /* switch */

                            uint64_t t4 = get_nanotime();
                            double dt = (t4 - t3) / 1e9;

                            # if PRINT_SENDER
                            MPI_Send(&dt, 1, MPI_DOUBLE, SENDER_RANK, TAG, MPI_COMM_WORLD);
                            check_error("MPI_Send", err);
                            # else
                            if (iter != 0)
                            {
                                printf("%d:%10lu:%lu:%2d:%16s:%lf:%lf:%lf:%lf\n", omp_get_num_threads(), nbytes, nparts, iter, mode_str[mode], -1.0, -1.0, dt, 0.0);
                                fflush(stdout);
                            }
                            # endif

                            for (size_t i = 0 ; i < nbytes ; ++i)
                                assert(buffer[i] == 0);

                        } /* SENDER_RANK | RECEIVER_RANK */
                    } /* ITERATIONS */
                    free(buffer);
                } /* each buffer size */
            } /* mode */
        } /* single */
    } /* parallel */

    if (rank == SENDER_RANK)
        progress_thread_join();

    MPI_Finalize();

    return 0;
}
