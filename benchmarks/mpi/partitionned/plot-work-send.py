import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import argparse
import numpy as np

# Set up argument parsing
parser = argparse.ArgumentParser(description='Plot median of twork and tsend against nbytes for ISEND mode with error bars (min, max)')
parser.add_argument('csv_file', type=str, help='Path to the input CSV file')
args = parser.parse_args()

# Load the CSV file into a DataFrame with ':' as the separator
df = pd.read_csv(args.csv_file, sep=':')

# Strip any leading/trailing whitespace from column names
df.columns = df.columns.str.strip()

# Strip any leading/trailing whitespace from the 'mode' column values
df['mode'] = df['mode'].str.strip()

# Ensure the required columns are present
required_columns = ['nthreads', 'nbytes', 'nparts', 'iteration', 'mode', 'twork', 'tsend', 'trecv']
missing_columns = [col for col in required_columns if col not in df.columns]
if missing_columns:
    raise ValueError(f"Missing columns in the CSV file: {', '.join(missing_columns)}")

# Filter the data to only include 'ISEND' mode
df_isend = df[df['mode'] == 'ISEND']

# Group by 'nbytes' to calculate the median, min, and max of 'twork' and 'tsend'
grouped_isend = df_isend.groupby(['nbytes'])[['twork', 'tsend']].agg(['median', 'min', 'max']).reset_index()

# Plot median of twork and tsend against nbytes for 'ISEND' mode with error bars (min, max)
f = 0.6
w = 6
h = w * f
plt.figure(figsize=(w, h))

# Define marker styles for 'twork' and 'tsend'
line_styles = {
    'twork': {'marker': 'o', 'color': 'black', 'label': 'Work', 'linestyle': '-'},
    'tsend': {'marker': 'x', 'color': 'black', 'label': 'Communication', 'linestyle': '--'}
}

# Plot for 'twork' and 'tsend' with error bars for 'ISEND' mode
for col in ['twork', 'tsend']:
    yerr = [grouped_isend[(col, 'median')] - grouped_isend[(col, 'min')],
            grouped_isend[(col, 'max')] - grouped_isend[(col, 'median')]]

    # Get the style for the current column
    style = line_styles[col]

    # Plot with error bars
    plt.errorbar(grouped_isend['nbytes'], grouped_isend[(col, 'median')], yerr=yerr,
                 fmt=style['marker'], color=style['color'], label=style['label'],
                 linestyle=style['linestyle'])

# Set logarithmic scale for x-axis with base 2
plt.xscale('log', base=2)

# Set y-axis logarithmic scale with base 2, but modify the tick formatting
plt.yscale('log', base=10)
plt.ylim(4*10**(-6), 2*10**0)

## Format the y-ticks as plain numbers instead of powers of 2
#def format_plain(y, pos):
#    return round(y, 5)
#
## Apply the format to y-ticks
#plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(format_plain))

# Get the unique nbytes values from the filtered dataset
x_ticks = np.unique(grouped_isend['nbytes'])

# Select every 2nd tick
x_ticks_filtered = x_ticks[::3]

# Format the x-ticks to be human-readable bytes (KB, MB, GB, etc.)
def format_bytes(x, pos):
    # Define byte units
    units = ['B', 'KB', 'MB', 'GB', 'TB']
    for i, unit in enumerate(units):
        if x < 1024.0:
            return f'{x:.0f}{unit}'
        x /= 1024.0
    return f'{x:.0f}{units[-1]}'

# Apply the x-ticks and format them
plt.xticks(x_ticks_filtered, [format_bytes(x, None) for x in x_ticks_filtered])

# Add labels and title
plt.xlabel('Number of bytes (S)')
plt.ylabel('Time (in s)')
#plt.title('Median of twork and tsend vs nbytes for ISEND mode with error bars (min, max)')
plt.legend()
plt.grid(True, which="both", ls="--")

# Show the plot
plt.show()

