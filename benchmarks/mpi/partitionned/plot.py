import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import argparse
import numpy as np

# Set up argument parsing
parser = argparse.ArgumentParser(description='Plot median of trecv against nbytes for each mode with error bars (min, max)')
parser.add_argument('csv_file', type=str, help='Path to the input CSV file')
args = parser.parse_args()

# Load the CSV file into a DataFrame with ':' as the separator
df = pd.read_csv(args.csv_file, sep=':')

# Strip any leading/trailing whitespace from column names
df.columns = df.columns.str.strip()

# Strip any leading/trailing whitespace from the 'mode' column values
df['mode'] = df['mode'].str.strip()

# Ensure the required columns are present
required_columns = ['nthreads', 'nbytes', 'nparts', 'iteration', 'mode', 'twork', 'tsend', 'trecv']
missing_columns = [col for col in required_columns if col not in df.columns]
if missing_columns:
    raise ValueError(f"Missing columns in the CSV file: {', '.join(missing_columns)}")

# Group by 'nbytes' and 'mode' to calculate the median, min, and max of 'trecv'
grouped = df.groupby(['nbytes', 'mode'])['trecv'].agg(['median', 'min', 'max']).reset_index()

# Filter out points where nbytes is below 1MB (1024*1024 bytes) and above 512MB (512*1024*1024 bytes)
# grouped_filtered = grouped[(grouped['nbytes'] >= 512 * 1024) & (grouped['nbytes'] <= 512 * 1024 * 1024)]
grouped_filtered = grouped

# Plot median of trecv against nbytes for each mode with error bars (min, max)
f=0.6
w=6
h=w*f
plt.figure(figsize=(w, h))

# Define markers, colors, and labels for each mode
mode_styles = {
    'ISEND': {'marker': 'o', 'color': 'blue', 'label': 'Fork-join', 'linestyle': '-'},
    'ISEND_MULTIPLE': {'marker': 's', 'color': 'green', 'label': 'Non-partitioned', 'linestyle': '--'},
    'PARTITIONNED': {'marker': 'x', 'color': 'red', 'label': 'Partitioned', 'linestyle': '-.'}
}

# Plot for each unique 'mode' with distinct styles
for mode, group_data in grouped_filtered.groupby('mode'):
    # Error bars: yerr is the difference between max, median and median, min
    yerr = [group_data['median'] - group_data['min'], group_data['max'] - group_data['median']]

    # Get the style for the current mode
    style = mode_styles.get(mode, {'marker': 'o', 'color': 'black', 'label': mode, 'linestyle': '-'})

    # Plot with error bars
    plt.errorbar(group_data['nbytes'], group_data['median'], yerr=yerr,
                 fmt=style['marker'], color=style['color'], label=style['label'],
                 linestyle=style['linestyle'])

# Set logarithmic scale for x-axis with base 2
plt.xscale('log', base=2)

# Set y-axis logarithmic scale with base 2, but modify the tick formatting
plt.yscale('log', base=10)
plt.ylim(4*10**(-6), 2*10**0)

## Format the y-ticks as plain numbers instead of powers of 2
#def format_plain(y, pos):
#    return round(y, 4)
#
## Apply the format to y-ticks
#plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(format_plain))

# Get the unique nbytes values from the filtered dataset
x_ticks = np.unique(grouped_filtered['nbytes'])

# Select every 2nd tick
x_ticks_filtered = x_ticks[::3]

# Format the x-ticks to be human-readable bytes (KB, MB, GB, etc.)
def format_bytes(x, pos):
    # Define byte units
    units = ['B', 'KB', 'MB', 'GB', 'TB']
    for i, unit in enumerate(units):
        if x < 1024.0:
            return f'{x:.0f}{unit}'
        x /= 1024.0
    return f'{x:.0f}{units[-1]}'

# Apply the x-ticks and format them
plt.xticks(x_ticks_filtered, [format_bytes(x, None) for x in x_ticks_filtered])

# Add labels and title
plt.xlabel('Number of bytes (S)')
plt.ylabel('Time to receive (in s)')
#plt.title('Median of trecv vs nbytes for each mode with error bars (min, max)')
plt.legend()
plt.grid(True, which="both", ls="--")

# Show the plot
plt.show()
