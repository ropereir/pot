# include <pot/spinlock.h>

# include <assert.h>
# include <omp.h>
# include <mpi.h>
# include <pthread.h>
# include <stdlib.h>
# include <unistd.h>

typedef struct  progress_request_s
{
    MPI_Request * req;
    omp_event_handle_t hdl;
    struct progress_request_s * next;
}               progress_request_t;

# define PROGRESS_THREAD_USLEEP (10)
# define MAX_REQUESTS (1024)
static pthread_t progress_thread;
static progress_request_t progress_requests_buffer[MAX_REQUESTS];
static volatile int progress_request_idx = 0;
static spinlock_t progress_requests_list_lock = 0;
static volatile progress_request_t * progress_requests_list = NULL;
static volatile int progress_thread_stop = 0;

void
progress_thread_detach(MPI_Request * req, omp_event_handle_t hdl)
{
    assert(req);

    int completed;
    MPI_Test(req, &completed, MPI_STATUS_IGNORE);
    if (completed)
    {
        omp_fulfill_event(hdl);
        return ;
    }

    assert(progress_request_idx <= MAX_REQUESTS);
    progress_request_t * p = NULL;
    SPINLOCK_LOCK(progress_requests_list_lock);
    {
        p = progress_requests_buffer + progress_request_idx++;
        p->next = progress_requests_list;
        progress_requests_list = p;
    }
    SPINLOCK_UNLOCK(progress_requests_list_lock);
    assert(p);

    p->req = req;
    p->hdl = hdl;
}

void
progress_thread_reset(void)
{
    SPINLOCK_LOCK(progress_requests_list_lock);
    {
        assert(progress_requests_list == NULL);
        // progress_requests_list = NULL;
        progress_request_idx = 0;
    }
    SPINLOCK_UNLOCK(progress_requests_list_lock);
}

static inline void *
progress_thread_main(void * args)
{
    while (!progress_thread_stop)
    {
        SPINLOCK_LOCK(progress_requests_list_lock);
        {
            progress_request_t *    p = progress_requests_list;
            progress_request_t * pred = NULL;
            while (p)
            {
                int completed;
                MPI_Test(p->req, &completed, MPI_STATUS_IGNORE);
                if (completed)
                {
                    if (pred)
                        pred->next = p->next;
                    else
                        progress_requests_list = p->next;
                    writemem_barrier();
                    omp_fulfill_event(p->hdl);
                }
                else
                {
                    pred = p;
                }
                p = p->next;
            }
        }
        SPINLOCK_UNLOCK(progress_requests_list_lock);

        usleep(PROGRESS_THREAD_USLEEP);
    }
    return args;
}

void
progress_thread_start(void)
{
    pthread_t progress_thread;
    pthread_create(&progress_thread, NULL, progress_thread_main, NULL);
}

void
progress_thread_join(void)
{
    progress_thread_stop = 1;
    pthread_join(progress_thread, NULL);
}
