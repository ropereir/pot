#ifndef __PROGRESS_THREAD_H__
# define __PROGRESS_THREAD_H__

# include <omp.h>
# include <mpi.h>

void progress_thread_detach(MPI_Request * req, omp_event_handle_t hdl);
void progress_thread_start(void);
void progress_thread_reset(void);
void progress_thread_join(void);

#endif /* __PROGRESS_THREAD_H__ */
