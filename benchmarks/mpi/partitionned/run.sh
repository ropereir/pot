#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <mode> <nbytes>"
    exit 1
fi

# Assign arguments to variables
mode=$1
nbytes=$2

# Check if nbytes is a positive integer
if ! [[ "$nbytes" =~ ^[0-9]+$ ]]; then
    echo "Invalid nbytes. nbytes should be a positive integer."
    exit 3
fi

export MPI_RANK=$OMPI_COMM_WORLD_RANK
export SENDER_RANK=0

# TODO : be sure it uses the correct interconnect and cpus

# on Grenoble
#if test "$[MPI_RANK % 2]" -eq "0" ; then
#    export OMP_PLACES="{0},{2},{4},{6},{8},{10},{12},{14},{16},{18},{20},{22},{24},{26},{28},{30}"
#else
#    export OMP_PLACES="{1},{3},{5},{7},{9},{11},{13},{15},{17},{19},{21},{23},{25},{27},{29},{31}"
#fi
#export OMP_NUM_THREADS=12

## on Lyon neowise
#if test "$[MPI_RANK % 2]" -eq "0" ; then
#    export OMP_PLACES="{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15}"
#else
#    export OMP_PLACES="{16},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31}"
#fi
#export OMP_NUM_THREADS=16

export OMP_NUM_THREADS=32
export OMP_PLACES="{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47}"
# ",".join(["{" + str(i) + "}" for i in range(48)])

# echo "$(hostname) - $OMP_PLACES"
export OMP_PROC_BIND=true

module load mpi/openmpi/5.0.6
module load llvm/19.x

export USE_POT=1

if test "$MPI_RANK" -eq "$SENDER_RANK" -a "$USE_POT" -eq "1"; then
    export OMP_TOOL_LIBRARIES=$POT/lib/libpot_ompt.so
    export POT_TIME=ns
    export LD_PRELOAD=$POT/lib/libpot_pthread.so:$POT/lib/libpot_pmpi.so:$LD_PRELOAD
    export POT_DIR=pot-trace-$nbytes-$mode
    rm -rf $POT_DIR
fi

r=$(./main $nbytes $mode)
#xterm -e bash -c "gdb --args ./main $nbytes $mode"

if test "$USE_POT" -eq "0" ; then
    echo "$r" | grep "$mode:"
fi

if test "$MPI_RANK" -eq "$SENDER_RANK" -a "$USE_POT" -eq "1"; then
    unset OMP_TOOL_LIBRARIES
    unset POT_TIME
    unset LD_PRELOAD
    r=$(pots -i $POT_DIR -p mpi-omp-task-overlap,time-breakdown)
    echo -n "$mode:$nbytes:"
    echo "$r" | grep "(overlaped, non_overlaped, ratio)" | cut -d "=" -f 3 | tr -d "(" | tr -d ")" | tr -d " " | tr "," ":"
fi
