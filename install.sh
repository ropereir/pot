#!/bin/bash

# src=/root/tools/pot
# dst=/root/.software/pot-0.0.0

src=/home/$USER/repos/pot
dst=/home/$USER/install/pot

# src=/home/$USER/repo/pot
# dst=/home/$USER/install/pot

mkdir -p $dst

# pot lib
rm -rf $src/pot/build
mkdir -p $src/pot/build
cd $src/pot/build
CC=clang CXX=clang cmake ../ -DCMAKE_INSTALL_PREFIX=$dst -DCMAKE_BUILD_TYPE=Release
make install -j

# ompt plugin
rm -rf $src/ompt/build
mkdir -p $src/ompt/build
cd $src/ompt/build
CC=clang CXX=clang cmake ../ -DCMAKE_INSTALL_PREFIX=$dst -DPOT_INSTALL=$dst -DCMAKE_BUILD_TYPE=Release
make install -j

# pthread plugin
rm -rf $src/pthread/build
mkdir -p $src/pthread/build
cd $src/pthread/build
CC=clang CXX=clang cmake ../ -DCMAKE_INSTALL_PREFIX=$dst -DPOT_INSTALL=$dst -DCMAKE_BUILD_TYPE=Release
make install -j

# pmpi plugin
#rm -rf $src/pmpi/build
#mkdir -p $src/pmpi/build
#cd $src/pmpi/build
#CC=clang CXX=clang cmake ../ -DCMAKE_INSTALL_PREFIX=$dst -DPOT_INSTALL=$dst -DCMAKE_BUILD_TYPE=Release
#make install -j

# post process
rm -rf $src/pots/build
mkdir -p $src/pots/build
cd $src/pots/build
CC=clang CXX=clang cmake ../ -DCMAKE_INSTALL_PREFIX=$dst -DPOT_INSTALL=$dst -DCMAKE_BUILD_TYPE=Release
make install -j
