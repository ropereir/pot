# include <omp.h>
# include <omp-tools.h>

# include <string.h>

# include <pot/todo.h>

typedef struct  thread_s
{
    int initialized;
    uint32_t tid;
    pot_omp_thread_type_t type;
    char * next_task_label;
    int next_task_color;
    uint32_t next_tlid;
}               thread_t;

static _Thread_local thread_t TLS;
static volatile atomic_uint THREAD_ID = 0;

static inline thread_t *
__get_thread(void)
{
    if (TLS.initialized == 0)
    {
        TLS.initialized = 1;
        TLS.tid = THREAD_ID++;
        TLS.next_task_label = NULL;
        TLS.next_task_color = 0;
        TLS.next_tlid = 0;
    }
    return &TLS;
}

void
ompt_set_task_name(const char * name)
{
    thread_t * thread = __get_thread();
    thread->next_task_label = (char *) name;
}

void
ompt_set_task_color(int color)
{
    thread_t * thread = __get_thread();
    thread->next_task_color = color;
}

void on_ompt_callback_thread_begin(ompt_thread_t thread_type, ompt_data_t *thread_data) {
    //POT_DEBUG("ompt_callback_thread_begin");

    thread_t * thread = __get_thread();
    switch (thread_type)
    {
        case (ompt_thread_initial):
        {
            thread->type = POT_OMP_THREAD_TYPE_INITIAL;
            break ;
        }

        case (ompt_thread_worker):
        {
            thread->type = POT_OMP_THREAD_TYPE_WORKER;
            break ;
        }

        default:
        {
            thread->type = POT_OMP_THREAD_TYPE_UNKNOWN;
            break ;
        }
    }

    POT_RECORD(omp_thread_begin, thread->type, thread->tid);
    thread_data->value = (uint64_t) thread;
}

void on_ompt_callback_thread_end(ompt_data_t *thread_data) {
    //POT_DEBUG("ompt_callback_thread_end");
    thread_t * thread = __get_thread();
    POT_RECORD(omp_thread_end, thread->tid);
    pot_thread_flush();
}

# if 0
void on_ompt_callback_parallel_begin(ompt_data_t *encountering_task_data, const ompt_frame_t *encountering_task_frame, ompt_data_t *parallel_data, unsigned int requested_parallelism, int flags, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_parallel_begin");
}

void on_ompt_callback_parallel_end(ompt_data_t *parallel_data, ompt_data_t *encountering_task_data, int flags, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_parallel_end");
}

void on_ompt_callback_work(ompt_work_t work_type, ompt_scope_endpoint_t endpoint, ompt_data_t *parallel_data, ompt_data_t *task_data, uint64_t count, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_work");
    // TODO : convert parallel for to if(0) implicit tasks
}

void on_ompt_callback_dispatch(ompt_data_t *parallel_data, ompt_data_t *task_data, ompt_dispatch_t kind, ompt_data_t instance) {
    //POT_DEBUG("ompt_callback_dispatch");
}

#endif

static inline pot_omp_task_uid_t
__next_task_uid(thread_t * thread)
{
    pot_omp_task_uid_t uid = {
        .tid = thread->tid,
        .tlid = ++thread->next_tlid
    };
    return uid;
}

void on_ompt_callback_task_create(ompt_data_t *encountering_task_data, const ompt_frame_t *encountering_task_frame, ompt_data_t *new_task_data, int flags, int has_dependences, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_task_create");
    thread_t * thread = __get_thread();
    pot_omp_task_uid_t uid = __next_task_uid(thread);
    pot_omp_task_flag_t pot_flags = {
        .implicit = 0, // TODO
        .tied = 0,
    };
    int32_t color = thread->next_task_color;    // not supported by ompt
    int32_t priority = 0;                       // not supported by ompt

    new_task_data->value = uid.value;

    POT_RECORD(omp_task_create, uid.tlid, pot_flags, color, priority, has_dependences);

    if (thread->next_task_label)
    {
        POT_RECORD(omp_task_label, uid.tlid, thread->next_task_label);
        thread->next_task_label = NULL;
    }

    // POT_RECORD(linux_rusage);
}

void on_ompt_callback_dependences(ompt_data_t *task_data, const ompt_dependence_t * deps, int ndeps) {
    //POT_DEBUG("ompt_callback_dependences");

    thread_t * thread = __get_thread();
    pot_omp_task_uid_t uid = (pot_omp_task_uid_t) task_data->value;

    pot_depend_t * potdeps = (pot_depend_t *) deps;
    pot_omp_task_depend_type_t type;
    for (int i = 0 ; i < ndeps ; ++i)
    {
        const ompt_dependence_t * dep = deps + i;
        switch (dep->dependence_type)
        {
            case ompt_dependence_type_in:
            {
                type = POT_DEPEND_TYPE_IN;
                break ;
            }

            // mutexinoutset is implement as 'out' in practice (2023)
            case ompt_dependence_type_out:
            case ompt_dependence_type_inout:
            case ompt_dependence_type_mutexinoutset:
            {
                type = POT_DEPEND_TYPE_OUT;
                break ;
            }

            case ompt_dependence_type_inoutset:
            {
                type = POT_DEPEND_TYPE_OUTSET;
                break ;
            }

            case ompt_dependence_type_source:
            case ompt_dependence_type_sink:
            case ompt_dependence_type_out_all_memory:
            case ompt_dependence_type_inout_all_memory:
            default:
            {
                POT_ERROR("Dependence type not supported %d\n", dep->dependence_type);
                exit(1);
                return ;
            }
        }
        potdeps[i].type = type;
    }
    POT_RECORD(omp_task_depend, uid.tlid, potdeps, ndeps);
}

void on_ompt_callback_task_dependence(ompt_data_t *src_task_data, ompt_data_t *sink_task_data) {
    //POT_DEBUG("ompt_callback_task_dependence");
    thread_t * thread = __get_thread();
    pot_omp_task_uid_t pred = { .value = src_task_data->value  };
    pot_omp_task_uid_t succ = { .value = sink_task_data->value };
    POT_RECORD(omp_task_dependency, pred, succ);
}

void on_ompt_callback_task_schedule(ompt_data_t *prior_task_data, ompt_task_status_t prior_task_status, ompt_data_t *next_task_data) {
    //POT_DEBUG("ompt_callback_task_schedule");
    thread_t * thread = __get_thread();

    # pragma message(TODO "Only trace OMPT states, and do the logic in the simulator")

    pot_omp_task_uid_t pred = { .value = prior_task_data->value };
    pot_omp_task_uid_t next;
    if (next_task_data == NULL)
    {
        // coming from an omp_fulfill_event
        next.value = POT_TASK_UID_NULL.value;
    }
    else
        next.value = next_task_data->value;
    POT_RECORD(omp_task_schedule, pred, prior_task_status, next);
}

void on_ompt_callback_implicit_task(ompt_scope_endpoint_t endpoint, ompt_data_t *parallel_data, ompt_data_t *task_data, unsigned int actual_parallelism, unsigned int index, int flags) {
    //POT_DEBUG("ompt_callback_implicit_task");
    thread_t * thread = __get_thread();

    pot_omp_task_uid_t uid;
    if (endpoint == ompt_scope_begin)
    {
        uid = __next_task_uid(thread);
        task_data->value = uid.value;
    }
    else
        uid.value = task_data->value;

    POT_RECORD(omp_task_implicit, uid.tlid, endpoint);
}

# if 0
void on_ompt_callback_masked(ompt_scope_endpoint_t endpoint, ompt_data_t *parallel_data, ompt_data_t *task_data, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_masked");
}

void on_ompt_callback_master(ompt_scope_endpoint_t endpoint, ompt_data_t *parallel_data, ompt_data_t *task_data, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_master");
}
# endif

void on_ompt_callback_sync_region(ompt_sync_region_t kind, ompt_scope_endpoint_t endpoint, ompt_data_t *parallel_data, ompt_data_t *task_data, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_sync_region");
    thread_t * thread = __get_thread();

    if (kind == ompt_sync_region_taskgroup)
    {
        switch (endpoint)
        {
            case (ompt_scope_begin):
            {
                POT_RECORD(omp_group_begin);
                break ;
            }

            case (ompt_scope_end):
            {
                POT_RECORD(omp_group_end);
                break ;
            }

            case (ompt_scope_beginend):
            default:
            {
                // nothing to do
                break ;
            }
        }
    }
    else
    {
        // TODO : any interesting stuff to forward to pot here ?
    }
}

void on_ompt_callback_sync_region_wait(ompt_sync_region_t kind, ompt_scope_endpoint_t endpoint, ompt_data_t *parallel_data, ompt_data_t *task_data, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_sync_region_wait");

    thread_t * thread = __get_thread();

    switch (endpoint)
    {
        case (ompt_scope_begin):
        {
            POT_RECORD(omp_sync_begin);
            break ;
        }

        case (ompt_scope_end):
        {
            POT_RECORD(omp_sync_end);
            break ;
        }

        case (ompt_scope_beginend):
        {
            // TODO : optimize pot with only 1 record
            POT_RECORD(omp_sync_begin);
            POT_RECORD(omp_sync_end);
            break ;
        }

        default:
        {
            break ;
        }
    }
}

# if 0

void on_ompt_callback_reduction(ompt_sync_region_t kind, ompt_scope_endpoint_t endpoint, ompt_data_t *parallel_data, ompt_data_t *task_data, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_reduction");
}

void on_ompt_callback_mutex_acquire(ompt_mutex_t kind, unsigned int hint, unsigned int impl, ompt_wait_id_t wait_id, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_mutex_acquire");
}

void on_ompt_callback_mutex_acquired(ompt_mutex_t kind, ompt_wait_id_t wait_id, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_mutex");
}

void on_ompt_callback_lock_init(ompt_mutex_t kind, unsigned int hint, unsigned int impl, ompt_wait_id_t wait_id, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_lock_init");
}

void on_ompt_callback_lock_destroy(ompt_mutex_t kind, ompt_wait_id_t wait_id, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_lock_destroy");
}

void on_ompt_callback_mutex_released(ompt_mutex_t kind, ompt_wait_id_t wait_id, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_released");
}

void on_ompt_callback_nest_lock(ompt_scope_endpoint_t endpoint, ompt_wait_id_t wait_id, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_nest_lock");
}

void on_ompt_callback_flush(ompt_data_t *thread_data, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_flush");
}
#endif

void on_ompt_callback_cancel(ompt_data_t *task_data, int flags, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_cancel");

    thread_t * thread = __get_thread();

    pot_omp_task_uid_t uid = { .value = task_data->value  };
    if (flags & ompt_cancel_discarded_task)
    {
        // TODO : a task transition (cancelled) -> (detached),(completed)
        POT_RECORD(omp_task_discarded, uid);
    }
    else if (flags & ompt_cancel_taskgroup)
    {
        POT_RECORD(omp_group_cancel, uid);
    }
}

#if 0
void on_ompt_callback_device_initialize(int device_num, const char *type, ompt_device_t *device, ompt_function_lookup_t lookup, const char *documentation) {
    //POT_DEBUG("ompt_callback_device_initialize");
}

void on_ompt_callback_device_finalize(int device_num) {
    //POT_DEBUG("ompt_callback_device_finalize");
}

void on_ompt_callback_device_load(int device_num, const char *filename, int64_t offset_in_file, void *vma_in_file, size_t bytes, void *host_addr, void *device_addr, uint64_t module_id) {
    //POT_DEBUG("ompt_callback_device_load");
}

void on_ompt_callback_device_unload(int device_num, uint64_t module_id) {
    //POT_DEBUG("ompt_callback_device_unload");
}

void on_ompt_callback_target_data_op_emi(ompt_scope_endpoint_t endpoint, ompt_data_t *target_task_data, ompt_data_t *target_data, ompt_id_t *host_op_id, ompt_target_data_op_t optype, void *src_addr, int src_device_num, void *dest_addr, int dest_device_num, size_t bytes, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_target_data_op_emi");
}

void on_ompt_callback_target_data_op(ompt_id_t target_id, ompt_id_t host_op_id, ompt_target_data_op_t optype, void *src_addr, int src_device_num, void *dest_addr, int dest_device_num, size_t bytes, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_target_data_op");
}

void on_ompt_callback_target_emi(ompt_target_t kind, ompt_scope_endpoint_t endpoint, int device_num, ompt_data_t *task_data, ompt_data_t *target_task_data, ompt_data_t *target_data, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_target_emi");
}

void on_ompt_callback_target(ompt_target_t kind, ompt_scope_endpoint_t endpoint, int device_num, ompt_data_t *task_data, ompt_id_t target_id, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_target");
}

void on_ompt_callback_target_map_emi(ompt_data_t *target_data, unsigned int nitems, void **host_addr, void **device_addr, size_t *bytes, unsigned int *mapping_flags, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_target_map_emi");
}

void on_ompt_callback_target_map(ompt_id_t target_id, unsigned int nitems, void **host_addr, void **device_addr, size_t *bytes, unsigned int *mapping_flags, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_target_map");
}

void on_ompt_callback_target_submit_emi(ompt_scope_endpoint_t endpoint, ompt_data_t *target_data, ompt_id_t *host_op_id, unsigned int requested_num_teams) {
    //POT_DEBUG("ompt_callback_target_submit_emi");
}

void on_ompt_callback_target_submit(ompt_id_t target_id, ompt_id_t host_op_id, unsigned int requested_num_teams) {
    //POT_DEBUG("ompt_callback_target_submit");
}

int on_ompt_callback_control_tool(uint64_t command, uint64_t modifier, void *arg, const void *codeptr_ra) {
    //POT_DEBUG("ompt_callback_control_tool");
    return omp_control_tool_success;
}
#endif

void on_ompt_callback_error(ompt_severity_t severity, const char *message, size_t length, const void *codeptr_ra) {
    fprintf(stderr, "Could not load MPC-TRACE OMPT\n");
    exit(1);
}
