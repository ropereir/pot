# include <pot/logger.h>
# include <pot/records.h>

# include <assert.h>
# include <omp.h>
# include <ompt.h>
# include <stdio.h>
# include <stdatomic.h>

# define TOOL_NAME "OMPT Plugin"

# define REGISTER_CALLBACK_T(name, type)                                            \
    do {                                                                            \
        type f_##name = &on_##name;                                                 \
        if (ompt_set_callback(name, (ompt_callback_t)f_##name) == ompt_set_never)   \
            POT_INFO("Could not register callback '" #name "' (ompt_set_never)");  \
    } while(0)
# define REGISTER_CALLBACK(name) REGISTER_CALLBACK_T(name, name##_t)

// interfaces to be lookup
static ompt_get_thread_data_t   ompt_get_thread_data;

# include "callback-impl.h"

void
ompt_finalize(ompt_data_t * tool_data)
{
    (void)tool_data;
//    POT_DEBUG("Tool finalize");
}

int ompt_initialize(
    ompt_function_lookup_t lookup,
    int initial_device_num,
    ompt_data_t * tool_data)
{
//    POT_DEBUG("Tool initialize");
    pot_init();

    (void) initial_device_num;
    (void) tool_data;
    ompt_set_callback_t ompt_set_callback = (ompt_set_callback_t) lookup("ompt_set_callback");
    if (ompt_set_callback == NULL)
    {
        POT_ERROR("Could not retrieve the 'ompt_set_callback' routine. Disabling the instrumentation.");
        return 0;
    }
    # include "callback-register.h"

    ompt_get_thread_data = (ompt_get_thread_data_t) lookup("ompt_get_thread_data");

    return 1;
}

ompt_start_tool_result_t *
ompt_start_tool(
    unsigned int omp_version,
    const char * runtime_version)
{
    static ompt_start_tool_result_t data = {&ompt_initialize, &ompt_finalize, (ompt_data_t) NULL};
    POT_INFO("%s: OpenMP version %d and runtime %s", TOOL_NAME, omp_version, runtime_version);
    return &data;
}
