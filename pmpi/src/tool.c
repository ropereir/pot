# include <pot/logger.h>
# include <pot/todo.h>
# include <pot/records.h>

# include <assert.h>
# include <pthread.h>
# include <mpi.h>

# define USE_OMPI 1

// convert from MPI impl to POT - this had been achieved with OpenMPI in mind, may not work with other MPI impl
static inline pot_mpi_communicator_t
communicator_get_uid(MPI_Comm comm)
{
    static_assert(sizeof(MPI_Comm) == sizeof(void *), "Assuming an MPI_Comm is a pointer ...");
    pot_mpi_communicator_t comm_id = (pot_mpi_communicator_t) comm;
    return comm_id;
}

static inline pot_mpi_datatype_t
datatype_get_uid(MPI_Datatype datatype)
{
    static_assert(sizeof(MPI_Datatype) == sizeof(void *), "Assuming an MPI_Datatype is a pointer ...");
    pot_mpi_datatype_t datatype_id = (pot_mpi_datatype_t) datatype;
    return datatype_id;
}

static inline pot_mpi_request_t
mpi_request_to_uid(MPI_Request request)
{
    static_assert(sizeof(MPI_Request) == sizeof(void *), "Assuming an MPI_Request is a pointer ...");
    pot_mpi_request_t uid = (pot_mpi_request_t) request;
    return uid;
}

// Routines override

int
MPI_Isend(const void * buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request * request)
{
    int r = PMPI_Isend(buf, count, datatype, dest, tag, comm, request);

    pot_mpi_request_t uid = mpi_request_to_uid(*request);
    POT_RECORD(mpi_req_isend, uid, buf, count, datatype_get_uid(datatype), dest, tag, communicator_get_uid(comm));

    return r;
}

int
MPI_Send_init(const void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request * request)
{
    int r = PMPI_Send_init(buf, count, datatype, dest, tag, comm, request);

    pot_mpi_request_t uid = mpi_request_to_uid(*request);
    POT_RECORD(mpi_req_send_init, uid, buf, count, datatype_get_uid(datatype), dest, tag, communicator_get_uid(comm));

    return r;
}

int
MPI_Irecv(void * buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request * request)
{
    int r = PMPI_Irecv(buf, count, datatype, source, tag, comm, request);

    pot_mpi_request_t uid = mpi_request_to_uid(*request);
    POT_RECORD(mpi_req_irecv, uid, buf, count, datatype_get_uid(datatype), source, tag, communicator_get_uid(comm));

    return r;
}

int
MPI_Test(MPI_Request * request, int * flag, MPI_Status * status)
{
    pot_mpi_request_t uid = mpi_request_to_uid(*request);
    int r = PMPI_Test(request, flag, status);
    if (*flag)
        POT_RECORD(mpi_req_test_success, uid);

    return r;
}

# if 0
int
MPI_Init_thread(int * argc, char *** argv, int required, int * provided)
{
    POT_FATAL("impl me");
    return PMPI_Init_thread(argc, argv, required, provided);
}
# endif

int
MPI_Psend_init(
    const void * buf, int partitions, MPI_Count count,
    MPI_Datatype datatype, int dest, int tag, MPI_Comm comm,
    MPI_Info info, MPI_Request * request
) {
    int r = PMPI_Psend_init(buf, partitions, count, datatype, dest, tag, comm, info, request);

    pot_mpi_request_t uid = mpi_request_to_uid(*request);
    POT_RECORD(mpi_req_psend_init, uid, buf, partitions, count, datatype_get_uid(datatype), dest, tag, communicator_get_uid(comm));

    return r;
}

int
MPI_Precv_init(
    void * buf, int partitions, MPI_Count count,
    MPI_Datatype datatype, int dest, int tag, MPI_Comm comm,
    MPI_Info info, MPI_Request * request
) {
    POT_FATAL("impl me");
    return PMPI_Precv_init(buf, partitions, count, datatype, dest, tag, comm, info, request);
}

int
MPI_Start(MPI_Request * request)
{
    pot_mpi_request_t uid = mpi_request_to_uid(*request);
    POT_RECORD(mpi_req_start, uid);

    int r = PMPI_Start(request);

    return r;
}

int
MPI_Pready(int partition, MPI_Request request)
{
    pot_mpi_request_t uid = mpi_request_to_uid(request);
    POT_RECORD(mpi_req_pready, partition, uid);

    int r = PMPI_Pready(partition, request);

    return r;
}

# if USE_OMPI
extern struct ompi_request_t ompi_request_empty;
# endif /* USE_OMPI */

static void
__MPI_Init_thread(int required, int provided)
{
    int rank, size;
    PMPI_Comm_rank(MPI_COMM_WORLD, &rank);
    PMPI_Comm_size(MPI_COMM_WORLD, &size);

    int len;
    char version[MPI_MAX_LIBRARY_VERSION_STRING];
    MPI_Get_library_version(version, &len);

  # if USE_OMPI
    pot_mpi_communicator_t comm_world = communicator_get_uid(MPI_COMM_WORLD);
    pot_mpi_request_t request_null = mpi_request_to_uid((MPI_Request) &ompi_request_null);
    pot_mpi_request_t request_empty = mpi_request_to_uid((MPI_Request) &ompi_request_empty);

  # else
    pot_mpi_communicator_t comm_world = 0;
    pot_mpi_request_t request_null = 0;
    pot_mpi_request_t request_empty = 0;
  # endif

    POT_RECORD(mpi_init, required, provided, comm_world, rank, size, version, request_null, request_empty);
}

int
MPI_Init(int * argc, char *** argv)
{
    int r = PMPI_Init(argc, argv);
    __MPI_Init_thread(MPI_THREAD_SINGLE, MPI_THREAD_SINGLE);
    return r;
}

int
MPI_Init_thread(int * argc, char *** argv, int required, int * provided)
{
    int r = PMPI_Init_thread(argc, argv, required, provided);
    __MPI_Init_thread(required, *provided);
    return r;
}

int
MPI_Finalize(void)
{
    int r = PMPI_Finalize();
    pot_thread_flush();
    return r;
}

int
MPI_Wait(MPI_Request * request, MPI_Status * status)
{
    pot_mpi_request_t uid = mpi_request_to_uid(*request);
    POT_RECORD(mpi_req_wait, uid, POT_MPI_REQ_PHASE_BEGIN);
    int r = PMPI_Wait(request, status);
    POT_RECORD(mpi_req_wait, uid, POT_MPI_REQ_PHASE_END);
    return r;
}

int
MPI_Waitall(int count, MPI_Request array_of_requests[], MPI_Status * array_of_statuses)
{
    POT_WARN("impl me");
    return PMPI_Waitall(count, array_of_requests, array_of_statuses);
}

# if 0
int
MPI_Request_free(MPI_Request * request)
{
    POT_FATAL("impl me");
    return PMPI_Request_free(request);
}
# endif
