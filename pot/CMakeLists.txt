# CMake Minimum version
cmake_minimum_required(VERSION 3.10)

# Project name
project(pot)

# Lib name
add_library(pot SHARED src/records.c src/logger.c)

# json-c
add_subdirectory(json-c)
target_link_libraries(pot json-c)

# Include directory
include_directories(include)

# Installation
INSTALL(TARGETS pot LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)
INSTALL(DIRECTORY ${CMAKE_SOURCE_DIR}/include/ DESTINATION include FILES_MATCHING PATTERN "*.h*")
