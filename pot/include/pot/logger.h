#ifndef __LOGGER_H__
# define __LOGGER_H__

#ifdef __cplusplus
extern "C" {
#endif

# ifndef _GNU_SOURCE
#  define _GNU_SOURCE
# endif /* _GNU_SOURCE */
# include <unistd.h>
# include <sys/types.h>
# include <time.h>
# include <stdio.h>
# include <stdlib.h>
# include <stdint.h>

# include "spinlock.h"
extern spinlock_t POT_PRINT_MTX;

# define POT_PRINT_FATAL_ID      0
# define POT_PRINT_ERROR_ID      1
# define POT_PRINT_WARN_ID       2
# define POT_PRINT_INFO_ID       3
# define POT_PRINT_IMPL_ID       4
# define POT_PRINT_DEBUG_ID      5

extern const char * POT_PRINT_COLORS[6];
extern const char * POT_PRINT_HEADERS[6];

extern int POT_VERBOSE;

extern volatile double   POT_TIME_ELAPSED;
extern volatile uint64_t POT_LAST_TIME;

# define POT_PRINT_LINE() \
    fprintf(stderr, "%s:%d (%s)\n", __FILE__, __LINE__, __func__);

# define POT_PRINT(LVL, ...)                                                    \
    do {                                                                        \
        if (LVL <= POT_VERBOSE)                                                 \
        {                                                                       \
            SPINLOCK_LOCK(POT_PRINT_MTX);                                       \
            struct timespec _ts;                                                \
            clock_gettime(CLOCK_MONOTONIC, &_ts);                               \
            uint64_t t = (uint64_t)(_ts.tv_sec * 1000000000) +                  \
                            (uint64_t) _ts.tv_nsec;                             \
            if (POT_LAST_TIME != 0)                                             \
                POT_TIME_ELAPSED += (double) (t - POT_LAST_TIME) / 1e9;         \
            POT_LAST_TIME = t;                                                  \
            if (isatty(STDOUT_FILENO))                                          \
                fprintf(stdout, "[%8lf] "                                       \
                                "[TID=%d] "                                     \
                                "[\033[1;37mPOT\033[0m] "                       \
                                "[%s%s\033[0m] ",                               \
                                POT_TIME_ELAPSED,                               \
                                gettid(),                                       \
                                POT_PRINT_COLORS[LVL],                          \
                                POT_PRINT_HEADERS[LVL]);                        \
            else                                                                \
                fprintf(stdout, "[%8lf]"                                        \
                                "[TID=%d] "                                     \
                                "[POT] "                                        \
                                "[%s] ",                                        \
                                POT_TIME_ELAPSED,                               \
                                gettid(),                                       \
                                POT_PRINT_HEADERS[LVL]);                        \
            fprintf(stdout, __VA_ARGS__);                                       \
            fprintf(stdout, "\n");                                              \
            fflush(stdout);                                                     \
            SPINLOCK_UNLOCK(POT_PRINT_MTX);                                     \
        }                                                                       \
        if (LVL == POT_PRINT_FATAL_ID)                                          \
        {                                                                       \
            POT_PRINT_LINE();                                                   \
            fflush(stderr);                                                     \
            abort();                                                            \
        }                                                                       \
    } while (0)

# define POT_NOT_IMPLEMENTED() POT_NOT_IMPLEMENTED_WARN("")

# define POT_NOT_IMPLEMENTED_WARN(S)                                 \
    POT_IMPL("'%s' at %s:%d in %s()",                                \
            S, __FILE__, __LINE__, __func__);

# define POT_INFO(...)  POT_PRINT(POT_PRINT_INFO_ID,  __VA_ARGS__)
# define POT_WARN(...)  POT_PRINT(POT_PRINT_WARN_ID,  __VA_ARGS__)
# define POT_ERROR(...) POT_PRINT(POT_PRINT_ERROR_ID, __VA_ARGS__)
# define POT_IMPL(...)  POT_PRINT(POT_PRINT_IMPL_ID,  __VA_ARGS__)
# ifndef NDEBUG
#  define POT_DEBUG(...) POT_PRINT(POT_PRINT_DEBUG_ID, __VA_ARGS__)
# else
#  define POT_DEBUG(...)
# endif
# define POT_FATAL(...) POT_PRINT(POT_PRINT_FATAL_ID, __VA_ARGS__)

void pot_logger_set_verbosity(const int verbosity);

#ifdef __cplusplus
};
#endif

#endif /* __LOGGER_H__*/
