#ifndef __RECORDS_H__
# define __RECORDS_H__

#ifdef __cplusplus
extern "C" {
#endif

# include <sys/time.h>
# include <sys/resource.h>

# include <stdatomic.h>
# include <stdint.h>
# include <stdio.h>

extern int POT_PAUSED;
# define POT_RECORD(EVENT, ...)         \
    do {                                \
        if (!POT_PAUSED)                \
            pot_##EVENT(__VA_ARGS__);   \
    } while (0)

// pause / resume event recording
void pot_pause(void);
void pot_resume(void);

// Must be called and returned once before doing any other calls
void pot_init(void);

//////////
// File //
//////////

/* Coucou beau-gosse */
# define POT_FILE_MAGIC   0xCCB6
# define POT_FILE_VERSION 2

# define POT_FLUSH_BUFFER_SIZE    8192

/* A trace file writer (1 per thread) */
typedef struct  pot_writer_s
{
    /* the file */
    FILE * file;
}               pot_writer_t;

/* time precision enum */
typedef enum    pot_time_spec_e
{
    POT_TIME_SPEC_UNKNOWN,
    POT_TIME_SPEC_US,
    POT_TIME_SPEC_NS,
}               pot_time_spec_t;

/* A trace file header */
typedef struct  pot_file_header_s
{
    /* magic */
    int magic;

    /* trace file version */
    int version;

    /* Kernel Process ID */
    uint32_t pid;

    /* Kernel Thread TID */
    uint32_t tid;

    /* Time precision */
    pot_time_spec_t timespec;

}               pot_file_header_t;

/////////////
// Records //
/////////////
typedef enum    pot_record_type_e
{
    // OpenMP
    POT_RECORD_OMP_THREAD_BEGIN = 0,
    POT_RECORD_OMP_THREAD_END,
    POT_RECORD_OMP_TASK_IMPLICIT,
    POT_RECORD_OMP_TASK_CREATE,
    POT_RECORD_OMP_TASK_LABEL,
    POT_RECORD_OMP_TASK_SCHEDULE,
    POT_RECORD_OMP_TASK_DELETE,
    POT_RECORD_OMP_TASK_DEPEND,
    POT_RECORD_OMP_TASK_DEPENDENCY,
    POT_RECORD_OMP_TASK_DISCARDED,
    POT_RECORD_OMP_SYNC_BEGIN,
    POT_RECORD_OMP_SYNC_END,
    POT_RECORD_OMP_GROUP_BEGIN,
    POT_RECORD_OMP_GROUP_CANCEL,
    POT_RECORD_OMP_GROUP_END,

    // MPI
    POT_RECORD_MPI_INIT,
    POT_RECORD_MPI_RANK,
    POT_RECORD_MPI_REQ_START,
    POT_RECORD_MPI_REQ_SEND,
    POT_RECORD_MPI_REQ_RECV,
    POT_RECORD_MPI_REQ_ISEND,
    POT_RECORD_MPI_REQ_SEND_INIT,
    POT_RECORD_MPI_REQ_IRECV,
    POT_RECORD_MPI_REQ_PSEND_INIT,
    POT_RECORD_MPI_REQ_PRECV_INIT,
    POT_RECORD_MPI_REQ_WAIT,
    POT_RECORD_MPI_REQ_WAITALL,
    POT_RECORD_MPI_REQ_TEST_SUCCESS,
    POT_RECORD_MPI_REQ_PREADY,

    // Linux
    POT_RECORD_LINUX_RUSAGE,

    POT_RECORD_TYPE_COUNT

}               pot_record_type_t;

const char * record_type_to_str(pot_record_type_t type);

// return the size of a record type in bytes
uint32_t pot_record_sizeof_type(pot_record_type_t type);

typedef enum    pot_record_endpoint_e
{
    POT_RECORD_ENDPOINT_BEGIN,
    POT_RECORD_ENDPOINT_END,
}               pot_record_endpoint_t;

// Record header
typedef struct  pot_record_s
{
    /* event time */
    uint64_t time;

    /* the record type */
    pot_record_type_t type;

    /* record data allocated straight after the header */
}               pot_record_t;

// return the sizeof of a record in byte
uint32_t pot_record_sizeof(pot_record_t * record);

// dump a record on stdout
void pot_record_dump(pot_record_t * record, uint32_t tid);

// Maximum number of threads that can exist
# define POT_MAX_THREADS      (512)

// Maximum preallocated memory usage to store records (256 Mo)
# define POT_RECORD_POOL_CAPACITY (256*1024*1024)

// flush records
void pot_thread_flush(void);

/////////////
//  POT VM //
/////////////

// thread state
typedef enum    pot_thread_state_e
{
    pot_thread_state_not_running,  // 0
    pot_thread_state_running,      // 1
}               pot_thread_state_t;

////////////
// OpenMP //
////////////

# include <ompt.h>

typedef enum    pot_omp_thread_type_e
{
    POT_OMP_THREAD_TYPE_INITIAL,
    POT_OMP_THREAD_TYPE_WORKER,
    POT_OMP_THREAD_TYPE_UNKNOWN,
}               pot_omp_thread_type_t;

// a thread starts / ends
void pot_omp_thread_begin(pot_omp_thread_type_t type, uint32_t tid);
void pot_omp_thread_end(uint32_t tid);

typedef struct  pot_omp_task_flag_s
{
    // 0 if explicit, 1 if implicit
    uint8_t implicit : 1;

    // 0 if tied, 1 if untied
    uint8_t tied : 1;

}           pot_omp_task_flag_t;

typedef union   pot_omp_task_uid_u
{
    uint64_t value;
    struct {
        uint32_t tid;
        uint32_t tlid;
    };
}               pot_omp_task_uid_t;

extern pot_omp_task_uid_t POT_TASK_UID_NULL;
# define POT_TASK_UID_IS_NULL(U) (U.value == POT_TASK_UID_NULL.value)

// see Figure 3.2: OpenMP Task Transition System in the MPC-OMP runtime
// of R. PEREIRA Manuscript
typedef enum    pot_omp_task_state_e
{
    pot_omp_task_state_uninitialized,     // 0
    pot_omp_task_state_initialized,       // 1
    pot_omp_task_state_ready,             // 2
    pot_omp_task_state_running,           // 3
    pot_omp_task_state_cancelled,         // 4
    pot_omp_task_state_suspended,         // 5
    pot_omp_task_state_executed,          // 6
    pot_omp_task_state_detached,          // 7
    pot_omp_task_state_completed,         // 8
    pot_omp_task_state_deleted,           // 9
}               pot_omp_task_state_t;

// task implicit
void pot_omp_task_implicit(uint32_t tlid, ompt_scope_endpoint_t endpoint);

// a task is created
void pot_omp_task_create(uint32_t tlid, pot_omp_task_flag_t flags, int32_t color, int32_t priority, int32_t has_dependences);

// set a task label
void pot_omp_task_label(uint32_t tlid, char * label);

// thread switches from 'prev' to 'next'
void pot_omp_task_schedule(pot_omp_task_uid_t prev_uid, ompt_task_status_t prev_status, pot_omp_task_uid_t next_uid);

// 'task' is deleted by the runtime
void pot_omp_task_delete(pot_omp_task_uid_t uid);

// out <=> inout <=> mutexinoutset ; inoutset <=> outset
typedef enum    pot_omp_task_depend_type_e
{
    POT_DEPEND_TYPE_IN,
    POT_DEPEND_TYPE_OUT,
    POT_DEPEND_TYPE_OUTSET,
}               pot_omp_task_depend_type_t;

typedef struct  pot_depend_t
{
    uint64_t addr;
    pot_omp_task_depend_type_t type;
}               pot_depend_t;

// task depend(type: *addr)
void pot_omp_task_depend(uint32_t tlid, pot_depend_t * deps, uint64_t ndeps);

// 'pred' -> 'succ' and 'pred' completed
void pot_omp_task_dependency(pot_omp_task_uid_t pred, pot_omp_task_uid_t succ);

// discarded after a cancellation
void pot_omp_task_discarded(pot_omp_task_uid_t uid);

// sync on thread
void pot_omp_sync_begin(void);
void pot_omp_sync_end(void);

// task groups
void pot_omp_group_begin(void);
void pot_omp_group_cancel(pot_omp_task_uid_t uid);
void pot_omp_group_end(void);

// return the number of ('us', 'ns', ...) in a 's'
double pot_get_timespec_precision(pot_time_spec_t timespec);

// OMP RECORDS
typedef struct  pot_record_omp_thread_begin_s
{
    pot_record_t parent;
    pot_omp_thread_type_t type;
    uint32_t tid;
}               pot_record_omp_thread_begin_t;

typedef struct  pot_record_omp_thread_end_s
{
    pot_record_t parent;
    uint32_t tid;
}               pot_record_omp_thread_end_t;

typedef struct  pot_record_omp_task_implicit_s
{
    pot_record_t parent;
    uint32_t tlid;
    ompt_scope_endpoint_t endpoint;
}               pot_record_omp_task_implicit_t;

typedef struct  pot_record_omp_task_create_s
{
    pot_record_t parent;
    uint32_t tlid;
    pot_omp_task_flag_t flags;
    int32_t color;
    int32_t priority;
    int32_t has_dependences;
}               pot_record_omp_task_create_t;

typedef struct  pot_record_omp_task_label_s
{
    pot_record_t parent;
    uint32_t tlid;
    uint16_t len;
    /* 'len' chars follows the record */
}               pot_record_omp_task_label_t;

typedef struct  pot_record_omp_task_schedule_s
{
    pot_record_t parent;
    pot_omp_task_uid_t prev_uid;
    ompt_task_status_t prev_status;
    pot_omp_task_uid_t next_uid;
}               pot_record_omp_task_schedule_t;

typedef struct  pot_record_omp_task_delete_s
{
    pot_record_t parent;
    pot_omp_task_uid_t uid;
}               pot_record_omp_task_delete_t;

typedef struct  pot_record_omp_task_depend_s
{
    pot_record_t parent;
    uint32_t tlid;
    uint32_t ndeps;

    /**
     * A serie of 'ndeps' couple follows the structure
     *
     *   pot_omp_task_depend_type_t type;
     *   void * addr;
     */
}               pot_record_omp_task_depend_t;

typedef struct  pot_record_omp_task_dependency_s
{
    pot_record_t parent;
    pot_omp_task_uid_t pred;
    pot_omp_task_uid_t succ;
}               pot_record_omp_task_dependency_t;

typedef struct  pot_record_omp_task_discarded_s
{
    pot_record_t parent;
    pot_omp_task_uid_t uid;
}               pot_record_omp_task_discarded_t;

typedef struct  pot_record_omp_sync_begin_s
{
    pot_record_t parent;
}               pot_record_omp_sync_begin_t;

typedef struct  pot_record_omp_sync_end_s
{
    pot_record_t parent;
}               pot_record_omp_sync_end_t;

typedef struct  pot_record_omp_group_begin_s
{
    pot_record_t parent;
}               pot_record_omp_group_begin_t;

typedef struct  pot_record_omp_group_cancel_s
{
    pot_record_t parent;
    pot_omp_task_uid_t uid;
}               pot_record_omp_group_cancel_t;

typedef struct  pot_record_omp_group_end_s
{
    pot_record_t parent;
}               pot_record_omp_group_end_t;

/////////
// MPI //
/////////

// uid representing an mpi request
typedef uintptr_t   pot_mpi_communicator_t;
typedef uintptr_t   pot_mpi_datatype_t;
typedef uintptr_t   pot_mpi_request_t;

// see Figure 3.1: MPI Requests as a transition system
// of R. PEREIRA Manuscript
typedef enum    pot_mpi_req_status_e
{
    pot_mpi_req_status_uninitialized,  // 0
    pot_mpi_req_status_initialized,    // 1
    pot_mpi_req_status_started,        // 2
    pot_mpi_req_status_consumed,       // 3
}               pot_mpi_req_status_t;

typedef enum    pot_mpi_req_synchronousity_e
{
    POT_MPI_REQ_SYNCHRONOUS,
    POT_MPI_REQ_ASYNCHRONOUS,
    POT_MPI_REQ_SYNC_UNKNOWN
}               pot_mpi_req_synchronousity_t;

typedef enum    pot_mpi_req_type_e
{
    POT_MPI_REQ_RECV,
    POT_MPI_REQ_SEND,
    POT_MPI_REQ_ALLGATHER,
    POT_MPI_REQ_ALLREDUCE,
    POT_MPI_REQ_GATHER,
    POT_MPI_REQ_REDUCE
    // [...]
}               pot_mpi_req_type_t;

typedef enum    pot_mpi_req_phase_e
{
    POT_MPI_REQ_PHASE_BEGIN,
    POT_MPI_REQ_PHASE_END
}               pot_mpi_req_phase_t;

// TODO : add MPI_Status to these

// records register
void pot_mpi_init(int required, int provided, pot_mpi_communicator_t world_comm, int world_rank, int world_size, char * version, pot_mpi_request_t request_null, pot_mpi_request_t request_empty);
void pot_mpi_rank(pot_mpi_communicator_t communicator, int rank);
void pot_mpi_req_send(const void * buf, int count, pot_mpi_datatype_t datatype, int dest, int tag, pot_mpi_communicator_t communicator);
void pot_mpi_req_recv(void * buf, int count, pot_mpi_datatype_t datatype, int src, int tag, pot_mpi_communicator_t communicator);
void pot_mpi_req_isend(pot_mpi_request_t uid, const void * buf, int count, pot_mpi_datatype_t datatype, int dest, int tag, pot_mpi_communicator_t communicator);
void pot_mpi_req_send_init(pot_mpi_request_t uid, const void * buf, int count, pot_mpi_datatype_t datatype, int dest, int tag, pot_mpi_communicator_t communicator);
void pot_mpi_req_irecv(pot_mpi_request_t uid, void * buf, int count, pot_mpi_datatype_t datatype, int src, int tag, pot_mpi_communicator_t communicator);
void pot_mpi_req_start(pot_mpi_request_t uid);
void pot_mpi_req_psend_init(pot_mpi_request_t uid, const void * buf, int npartites, int count_per_partite, pot_mpi_datatype_t datatype, int dest, int tag, pot_mpi_communicator_t communicator);
void pot_mpi_req_precv_init(pot_mpi_request_t uid, void * buf, int npartites, int count_per_partite, pot_mpi_datatype_t datatype, int src, int tag, pot_mpi_communicator_t communicator);
void pot_mpi_req_wait(pot_mpi_request_t uid, pot_mpi_req_phase_t phase);
void pot_mpi_req_waitall(int count, pot_mpi_request_t array_of_requests[], pot_mpi_req_phase_t phase);
void pot_mpi_req_test_success(pot_mpi_request_t uid);
void pot_mpi_req_pready(int partite, pot_mpi_request_t uid);

// records
typedef struct  pot_record_mpi_init_s
{
    pot_record_t parent;
    int required;
    int provided;
    struct {
        pot_mpi_communicator_t comm;
        int rank;
        int size;
    } world;
    struct {
        pot_mpi_request_t null;
        pot_mpi_request_t empty;
    } requests;
    int version_len;
}               pot_record_mpi_init_t;

typedef struct  pot_record_mpi_rank_s
{
    pot_record_t parent;
    pot_mpi_communicator_t communicator;
    int rank;
}               pot_record_mpi_rank_t;

typedef struct  pot_record_mpi_req_p2p_s
{
    pot_record_t parent;
    const void * buf;
    int count;
    pot_mpi_datatype_t datatype;
    union {
        int dest;
        int src;
        int rank;
    };
    int tag;
    pot_mpi_communicator_t communicator;
}               pot_record_mpi_req_p2p_t;

typedef pot_record_mpi_req_p2p_t pot_record_mpi_req_send_t;
typedef pot_record_mpi_req_p2p_t pot_record_mpi_req_recv_t;

typedef struct  pot_record_mpi_req_ip2p_s
{
    pot_record_mpi_req_p2p_t parent;
    pot_mpi_request_t uid;
}               pot_record_mpi_req_ip2p_t;

typedef pot_record_mpi_req_ip2p_t pot_record_mpi_req_isend_t;
typedef pot_record_mpi_req_ip2p_t pot_record_mpi_req_irecv_t;

typedef pot_record_mpi_req_ip2p_t pot_record_mpi_req_send_init_t;

typedef struct  pot_record_mpi_req_start_s
{
    pot_record_t parent;
    pot_mpi_request_t uid;
}               pot_record_mpi_req_start_t;

typedef struct  pot_record_mpi_req_partitionned_init_s
{
    pot_record_t parent;
    pot_mpi_request_t uid;
    const void * buf;
    int npartites;
    int count_per_partite;
    pot_mpi_datatype_t datatype;
    union {
        int dest;
        int src;
        int rank;
    };
    int tag;
    pot_mpi_communicator_t communicator;
}               pot_record_mpi_req_partitionned_init_t;

typedef pot_record_mpi_req_partitionned_init_t pot_record_mpi_req_psend_init_t;
typedef pot_record_mpi_req_partitionned_init_t pot_record_mpi_req_precv_init_t;

typedef struct  pot_record_mpi_req_wait_s
{
    pot_record_t parent;
    pot_mpi_request_t uid;
    pot_mpi_req_phase_t phase;
}               pot_record_mpi_req_wait_t;

typedef struct  pot_record_mpi_req_waitall_s
{
    pot_record_t parent;
    uint32_t nreq;
    pot_mpi_req_phase_t phase;

    /* A serie of 'nreq' pot_mpi_request_t follows */
}               pot_record_mpi_req_waitall_t;

typedef struct  pot_record_mpi_req_test_success_s
{
    pot_record_t parent;
    pot_mpi_request_t uid;
}               pot_record_mpi_req_test_success_t;

typedef struct  pot_record_mpi_req_pready_s
{
    pot_record_t parent;
    int partite;
    pot_mpi_request_t uid;
}               pot_record_mpi_req_pready_t;

//////////////
//  Linux   //
//////////////

typedef struct  pot_record_linux_rusage_s
{
    pot_record_t parent;
    struct rusage rusage;
}               pot_record_linux_rusage_t;

void pot_linux_rusage(void);

#ifdef __cplusplus
};
#endif

#endif /* __RECORDS_H__ */
