# include <stdint.h>
# include "pot/logger.h"
# include "pot/spinlock.h"
volatile spinlock_t POT_PRINT_MTX;

volatile double     POT_TIME_ELAPSED = 0.0;
volatile uint64_t   POT_LAST_TIME    = 0;

# define NLVL 6

char const * POT_PRINT_COLORS[NLVL] = {
    "\033[1;31m",
    "\033[1;31m",
    "\033[1;33m",
    "\033[1;32m",
    "\033[1;35m",
    "\033[1;36m",
};

char const * POT_PRINT_HEADERS[NLVL] = {
    "FATAL",
    "ERROR",
    "WARN",
    "INFO",
    "IMPL",
    "DEBUG",
};

int POT_VERBOSE = POT_PRINT_IMPL_ID;

void
pot_logger_set_verbosity(const int verbosity)
{
    POT_VERBOSE = verbosity;
}
