# include "pot/logger.h"
# include "pot/records.h"
# include "pot/spinlock.h"

# include <assert.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/time.h>
# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>

typedef struct  pot_tls_s
{
    // trace writer
    pot_writer_t writer;

    // file id
    uint32_t fid;

    // thread id
    uint32_t tid;

    // records
    uint8_t * records;

    // memory used (< capacity)
    uint64_t used;

    // capacity
    uint64_t capacity;

}               pot_tls_t;

static volatile atomic_int pot_next_tid;
static _Thread_local pot_tls_t pot_tls;

// the null task uid
pot_omp_task_uid_t POT_TASK_UID_NULL = {.value = 0};

// Utilities
static inline char *
pot_omp_task_depend_type_str(pot_omp_task_depend_type_t type)
{
    switch (type)
    {
        case (POT_DEPEND_TYPE_IN):
            return "in";

        case (POT_DEPEND_TYPE_OUT):
            return "out";

        case (POT_DEPEND_TYPE_OUTSET):
            return "outset";
    }
    return "unknown";
}

const char *
record_type_to_str(pot_record_type_t type)
{
    switch (type)
    {
        case (POT_RECORD_OMP_THREAD_BEGIN):         return "OMP_THREAD_BEGIN";
        case (POT_RECORD_OMP_THREAD_END):           return "OMP_THREAD_END";
        case (POT_RECORD_OMP_TASK_IMPLICIT):        return "OMP_TASK_IMPLICIT";
        case (POT_RECORD_OMP_TASK_CREATE):          return "OMP_TASK_CREATE";
        case (POT_RECORD_OMP_TASK_LABEL):           return "OMP_TASK_LABEL";
        case (POT_RECORD_OMP_TASK_SCHEDULE):        return "OMP_TASK_SCHEDULE";
        case (POT_RECORD_OMP_TASK_DELETE):          return "OMP_TASK_DELETE";
        case (POT_RECORD_OMP_TASK_DEPEND):          return "OMP_TASK_DEPEND";
        case (POT_RECORD_OMP_TASK_DEPENDENCY):      return "OMP_TASK_DEPENDENCY";
        case (POT_RECORD_OMP_TASK_DISCARDED):       return "OMP_TASK_DISCARDED";
        case (POT_RECORD_OMP_SYNC_BEGIN):           return "OMP_SYNC_BEGIN";
        case (POT_RECORD_OMP_SYNC_END):             return "OMP_SYNC_END";
        case (POT_RECORD_OMP_GROUP_BEGIN):          return "OMP_GROUP_BEGIN";
        case (POT_RECORD_OMP_GROUP_CANCEL):         return "OMP_GROUP_CANCEL";
        case (POT_RECORD_OMP_GROUP_END):            return "OMP_GROUP_END";
        case (POT_RECORD_MPI_INIT):                 return "MPI_INIT";
        case (POT_RECORD_MPI_RANK):                 return "MPI_RANK";
        case (POT_RECORD_MPI_REQ_START):            return "MPI_REQ_START";
        case (POT_RECORD_MPI_REQ_SEND):             return "MPI_REQ_SEND";
        case (POT_RECORD_MPI_REQ_RECV):             return "MPI_REQ_RECV";
        case (POT_RECORD_MPI_REQ_ISEND):            return "MPI_REQ_ISEND";
        case (POT_RECORD_MPI_REQ_SEND_INIT):        return "MPI_REQ_SEND_INIT";
        case (POT_RECORD_MPI_REQ_IRECV):            return "MPI_REQ_IRECV";
        case (POT_RECORD_MPI_REQ_PSEND_INIT):       return "MPI_REQ_PSEND_INIT";
        case (POT_RECORD_MPI_REQ_PRECV_INIT):       return "MPI_REQ_PRECV_INIT";
        case (POT_RECORD_MPI_REQ_WAIT):             return "MPI_REQ_WAIT";
        case (POT_RECORD_MPI_REQ_WAITALL):          return "MPI_REQ_WAITALL";
        case (POT_RECORD_MPI_REQ_TEST_SUCCESS):     return "MPI_REQ_TEST_SUCCESS";
        case (POT_RECORD_MPI_REQ_PREADY):           return "MPI_REQ_PREADY";
        case (POT_RECORD_LINUX_RUSAGE):             return "LINUX_RUSAGE";
        default:
        case (POT_RECORD_TYPE_COUNT):               return "UNKNOWN";
    }
}


void
pot_record_dump(pot_record_t * record, uint32_t tid)
{
    const char * name = record_type_to_str(record->type);

    char params1[64];
    snprintf(params1, sizeof(params1), "type=%d, time=%lu", record->type, record->time);

    char params2[512];
    params2[0] = 0;

    switch (record->type)
    {
        case (POT_RECORD_OMP_THREAD_BEGIN):
        {
            pot_record_omp_thread_begin_t * r = (pot_record_omp_thread_begin_t *) record;
            snprintf(params2, sizeof(params2), "type=%d, tid=%u", r->type, r->tid);
            break ;
        }

        case (POT_RECORD_OMP_THREAD_END):
        {
            pot_record_omp_thread_end_t * r = (pot_record_omp_thread_end_t *) record;
            snprintf(params2, sizeof(params2), "tid=%u", r->tid);
            break ;
        }

        case (POT_RECORD_OMP_TASK_IMPLICIT):
        {
            pot_record_omp_task_implicit_t * r = (pot_record_omp_task_implicit_t *) record;
            snprintf(params2, sizeof(params2), "uid=(%u, %u), endpoint=%d", tid, r->tlid, (int) r->endpoint);
            break ;
        }

        case (POT_RECORD_OMP_TASK_CREATE):
        {
            pot_record_omp_task_create_t * r = (pot_record_omp_task_create_t *) record;
            snprintf(params2, sizeof(params2), "uid=(%u, %u), flags=%d, color=%d, priority=%d", tid, r->tlid, *((int *) &r->flags), r->color, r->priority);
            break ;
        }

        case (POT_RECORD_OMP_TASK_LABEL):
        {
            // TODO
            return ;
        }

        case (POT_RECORD_OMP_TASK_DELETE):
        {
            // TODO
            return ;
        }

        case (POT_RECORD_OMP_TASK_SCHEDULE):
        {
            pot_record_omp_task_schedule_t * r = (pot_record_omp_task_schedule_t *) record;
            snprintf(params2, sizeof(params2), "prev_state=%d, prev_uid=(%u, %u), next_uid=(%u, %u)", r->prev_status, r->prev_uid.tid, r->prev_uid.tlid, r->next_uid.tid, r->next_uid.tlid);
            break ;
        }

        case (POT_RECORD_OMP_TASK_DEPEND):
        {
            pot_record_omp_task_depend_t * r = (pot_record_omp_task_depend_t *) record;
            snprintf(params2, sizeof(params2), "task_lid=%u, ndeps=%u", r->tlid, r->ndeps);
            // pot_omp_task_depend_type_str(r->type), r->addr // TODO : add to output
            break ;
        }

        case (POT_RECORD_OMP_TASK_DEPENDENCY):
        {
            pot_record_omp_task_dependency_t * r = (pot_record_omp_task_dependency_t *) record;
            snprintf(params2, sizeof(params2), "pred_uid=(%u, %u), succ_uid=(%u, %u)", r->pred.tid, r->pred.tlid, r->succ.tid, r->succ.tlid);
            break ;
        }

        case (POT_RECORD_OMP_TASK_DISCARDED):
        {
            pot_record_omp_task_discarded_t * r = (pot_record_omp_task_discarded_t *) record;
            snprintf(params2, sizeof(params2), "uid=(%u, %u)", r->uid.tid, r->uid.tlid);
            break ;
        }

        case (POT_RECORD_OMP_SYNC_BEGIN):
        {
            break ;
        }

        case (POT_RECORD_OMP_SYNC_END):
        {
            break ;
        }

        case (POT_RECORD_OMP_GROUP_BEGIN):
        {
            break ;
        }

        case (POT_RECORD_OMP_GROUP_CANCEL):
        {
            pot_record_omp_group_cancel_t * r = (pot_record_omp_group_cancel_t *) record;
            snprintf(params2, sizeof(params2), "uid=(%u, %u)", r->uid.tid, r->uid.tlid);
            break ;
        }

        case (POT_RECORD_OMP_GROUP_END):
        {
            break ;
        }

        // MPI

        case (POT_RECORD_MPI_INIT):
        case (POT_RECORD_MPI_RANK):
        {
            break ;
        }

        case (POT_RECORD_MPI_REQ_START):
        {
            pot_record_mpi_req_start_t * r = (pot_record_mpi_req_start_t *) record;
            snprintf(params2, sizeof(params2), "uid=%lu", r->uid);
            break ;
        }

        case (POT_RECORD_MPI_REQ_PSEND_INIT):
        case (POT_RECORD_MPI_REQ_PRECV_INIT):
        {
            break ;
        }

        case (POT_RECORD_MPI_REQ_ISEND):
        case (POT_RECORD_MPI_REQ_SEND_INIT):
        case (POT_RECORD_MPI_REQ_IRECV):
        {
            pot_record_mpi_req_ip2p_t * r = (pot_record_mpi_req_ip2p_t *) record;
            snprintf(params2, sizeof(params2), "uid=%lu", r->uid);
            break ;
        }

        case (POT_RECORD_MPI_REQ_SEND):
        case (POT_RECORD_MPI_REQ_RECV):
        case (POT_RECORD_MPI_REQ_WAIT):
        case (POT_RECORD_MPI_REQ_WAITALL):
        {
            break ;
        }

        case (POT_RECORD_MPI_REQ_TEST_SUCCESS):
        {
            pot_record_mpi_req_test_success_t * r = (pot_record_mpi_req_test_success_t *) record;
            snprintf(params2, sizeof(params2), "uid=%lu", r->uid);
            break ;
        }
        case (POT_RECORD_MPI_REQ_PREADY):

        // LINUX
        case (POT_RECORD_LINUX_RUSAGE):

        case (POT_RECORD_TYPE_COUNT):
        default:
        {
            break ;
        }
    }

    if (params2[0] == 0)
        POT_INFO("[TID=%u] %s(%s)", tid, name, params1);
    else
        POT_INFO("[TID=%u] %s(%s, %s)", tid, name, params1, params2);
}

uint32_t
pot_record_sizeof_type(pot_record_type_t type)
{
    switch (type)
    {
        // OMP
        case (POT_RECORD_OMP_THREAD_BEGIN):
            return sizeof(pot_record_omp_thread_begin_t);

        case (POT_RECORD_OMP_THREAD_END):
            return sizeof(pot_record_omp_thread_end_t);

        case (POT_RECORD_OMP_TASK_IMPLICIT):
            return sizeof(pot_record_omp_task_implicit_t);

        case (POT_RECORD_OMP_TASK_CREATE):
            return sizeof(pot_record_omp_task_create_t);

        case (POT_RECORD_OMP_TASK_LABEL):
            return sizeof(pot_record_omp_task_label_t);

        case (POT_RECORD_OMP_TASK_DELETE):
            return sizeof(pot_record_omp_task_delete_t);

        case (POT_RECORD_OMP_TASK_SCHEDULE):
            return sizeof(pot_record_omp_task_schedule_t);

        case (POT_RECORD_OMP_TASK_DEPEND):
            return sizeof(pot_record_omp_task_depend_t);

        case (POT_RECORD_OMP_TASK_DEPENDENCY):
            return sizeof(pot_record_omp_task_dependency_t);

        case (POT_RECORD_OMP_TASK_DISCARDED):
            return sizeof(pot_record_omp_task_discarded_t);

        case (POT_RECORD_OMP_SYNC_BEGIN):
            return sizeof(pot_record_omp_sync_begin_t);

        case (POT_RECORD_OMP_SYNC_END):
            return sizeof(pot_record_omp_sync_end_t);

        case (POT_RECORD_OMP_GROUP_BEGIN):
            return sizeof(pot_record_omp_group_begin_t);

        case (POT_RECORD_OMP_GROUP_CANCEL):
            return sizeof(pot_record_omp_group_cancel_t);

        case (POT_RECORD_OMP_GROUP_END):
            return sizeof(pot_record_omp_group_end_t);

        // MPI
        case (POT_RECORD_MPI_INIT):
            return sizeof(pot_record_mpi_init_t);

        case (POT_RECORD_MPI_RANK):
            return sizeof(pot_record_mpi_rank_t);

        case (POT_RECORD_MPI_REQ_START):
            return sizeof(pot_record_mpi_req_start_t);

        case (POT_RECORD_MPI_REQ_PSEND_INIT):
            return sizeof(pot_record_mpi_req_psend_init_t);

        case (POT_RECORD_MPI_REQ_PRECV_INIT):
            return sizeof(pot_record_mpi_req_precv_init_t);

        case (POT_RECORD_MPI_REQ_ISEND):
            return sizeof(pot_record_mpi_req_isend_t);

        case (POT_RECORD_MPI_REQ_SEND_INIT):
            return sizeof(pot_record_mpi_req_isend_t);

        case (POT_RECORD_MPI_REQ_IRECV):
            return sizeof(pot_record_mpi_req_irecv_t);

        case (POT_RECORD_MPI_REQ_SEND):
            return sizeof(pot_record_mpi_req_send_t);

        case (POT_RECORD_MPI_REQ_RECV):
            return sizeof(pot_record_mpi_req_recv_t);

        case (POT_RECORD_MPI_REQ_WAIT):
            return sizeof(pot_record_mpi_req_wait_t);

        case (POT_RECORD_MPI_REQ_WAITALL):
            return sizeof(pot_record_mpi_req_waitall_t);

        case (POT_RECORD_MPI_REQ_TEST_SUCCESS):
            return sizeof(pot_record_mpi_req_test_success_t);

        case (POT_RECORD_MPI_REQ_PREADY):
            return sizeof(pot_record_mpi_req_pready_t);

        // LINUX
        case (POT_RECORD_LINUX_RUSAGE):
            return sizeof(pot_record_linux_rusage_t);

        case (POT_RECORD_TYPE_COUNT):
        default:
        {
            POT_FATAL("Unknown record type=%d", type);
            return (uint32_t) -1;
        }
    }
}

uint32_t
pot_record_sizeof(pot_record_t * record)
{
    if (record->type == POT_RECORD_OMP_TASK_LABEL)
        return sizeof(pot_record_omp_task_label_t) + ((pot_record_omp_task_label_t *)record)->len * sizeof(char);
    if (record->type == POT_RECORD_OMP_TASK_DEPEND)
        return sizeof(pot_record_omp_task_depend_t) + ((pot_record_omp_task_depend_t *)record)->ndeps * sizeof(pot_depend_t);
    if (record->type == POT_RECORD_MPI_INIT)
        return sizeof(pot_record_mpi_init_t) + ((pot_record_mpi_init_t *)record)->version_len * sizeof(char);
    return pot_record_sizeof_type(record->type);
}

static inline pot_tls_t *
pot_tls_get(void)
{
    if (pot_tls.records == NULL)
    {
        uint64_t capacity = POT_RECORD_POOL_CAPACITY;
        char * memory = getenv("POT_MEMORY");
        if (memory)
            capacity = strtoull(memory, NULL, 10);

        pot_tls.records = (uint8_t *) malloc(capacity);
        if (!pot_tls.records)
            POT_ERROR("Could not alloc record buffer ('POT_MEMORY' too big ? Current value is '%lu')", capacity);
        assert(pot_tls.records);

        char * memory_touch = getenv("POT_MEMORY_TOUCH");
        int touch = memory_touch ? (strncmp(memory_touch, "0", 1) ? 1 : 0) : 1;
        if (touch)
        {
            // memset(pot_tls.records, 0, capacity);
            for (int i = 0; i < capacity ; i += sysconf(_SC_PAGESIZE))
                pot_tls.records[i] = 0;
        }

        pot_tls.used = 0;
        pot_tls.fid = 0;
        pot_tls.capacity = capacity;
        pot_tls.tid = pot_next_tid++;
    }

    return &pot_tls;
}

// This function will be called when the library is loaded
__attribute__((constructor))
void pre_load_callback()
{
    pot_tls_t * tls = pot_tls_get();
    assert(tls->records);
}

// File writer
static inline int
__thread_flush_create_file_directory(char * tracedir)
{
    char buffer[1024];
    strncpy(buffer, tracedir, sizeof(buffer));

    struct stat sb;
    char * tmp = buffer;
    while (*tmp)
    {
        if (*tmp == '/' && *tmp != *tracedir)
        {
            *tmp = 0;
            if (mkdir(buffer, S_IRWXU) == -1)
            {
                if (stat(buffer, &sb) == -1 || !S_ISDIR(sb.st_mode))
                    return -1;
            }
            *tmp = '/';
        }
        ++tmp;
    }

    if (mkdir(buffer, S_IRWXU) == -1)
        if (stat(buffer, &sb) == -1 || !S_ISDIR(sb.st_mode))
            return -1;

    return 0;
}

static inline void
__thread_flush_create_file(pot_tls_t * tls, uint32_t fid, uint32_t pid)
{
    // Create file
    char hostname[128];
    gethostname(hostname, sizeof(hostname));

    // allocate for full filepath
    char * filepath;
    char auto_tracedir[128];
    char * tracedir = getenv("POT_DIR");
    if (tracedir == NULL)
    {
        snprintf(auto_tracedir, sizeof(auto_tracedir), "pot-%ld", (unsigned long) time(NULL));
        tracedir = (char *) auto_tracedir;
    }

    const size_t extra = 512;
    const size_t size = sizeof(char) * (strlen(tracedir) + strlen(hostname) + extra);
    assert(3 * 10 + 5 < extra);
    filepath = (char *) malloc(size);

    // append directory to filepath
    snprintf(filepath, size, "%s", tracedir);
    POT_DEBUG("Generating trace to %s", filepath);

    // create directory
    if (__thread_flush_create_file_directory(filepath) < 0)
    {
        POT_ERROR("Could not access directory");
        perror(filepath);
        exit(1);
    }

    // append filename to filepath
    int len = strlen(filepath);
    snprintf(filepath + len, sizeof(filepath) - len - 1, "/%s-%u-%u-%u.dat", hostname, pid, tls->tid, fid);
    if ((tls->writer.file = fopen(filepath, "wb")) == NULL)
    {
        perror(filepath);
        POT_ERROR("Could create data file");
        exit(1);
    }
    assert(tls->writer.file);
}

static inline pot_time_spec_t
__get_timespec(void)
{
    static pot_time_spec_t timespec = POT_TIME_SPEC_UNKNOWN;
    if (timespec == POT_TIME_SPEC_UNKNOWN)
    {
        const char * var = "POT_TIME";
        char * s = getenv(var);
        if (s == NULL || !s[0])
            timespec = POT_TIME_SPEC_NS;
        else if (strcmp(s, "ns") == 0)
            timespec = POT_TIME_SPEC_NS;
        else if (strcmp(s, "us") == 0)
            timespec = POT_TIME_SPEC_US;
        else
        {
            timespec = POT_TIME_SPEC_NS;
            POT_ERROR("Wrong value for '%s'. Available values are ['us', 'ns']", var);
        }
    }
    return timespec;
}

void
pot_thread_flush(void)
{
    pot_tls_t * tls = pot_tls_get();
    if (tls->used == 0)
        return ;

    const uint32_t pid = (uint32_t) getpid();
    const uint32_t tid = tls->tid;
    const uint32_t fid = tls->fid;

    POT_DEBUG("Thread %u flushing (fid=%u)", tid, fid);

    __thread_flush_create_file(tls, fid, pid);

    // write header
    pot_file_header_t header;
    header.magic    = POT_FILE_MAGIC;
    header.version  = POT_FILE_VERSION;
    header.pid      = pid;
    header.tid      = tid;
    header.timespec = __get_timespec();
    fwrite(&header, sizeof(pot_file_header_t), 1, tls->writer.file);

    // write content
    fwrite(tls->records, tls->used, 1, tls->writer.file);

    // Release file
    fclose(tls->writer.file);

    POT_DEBUG("Thread %u flushed (fid=%u)", tid, tls->fid);

    // Release records
    tls->used = 0;
    ++tls->fid;
}

double
pot_get_timespec_precision(pot_time_spec_t timespec)
{
    switch (timespec)
    {
        case (POT_TIME_SPEC_US):
            return 1e6;
        case (POT_TIME_SPEC_NS):
            return 1e9;
        default:
            assert(0);
    }
    return 1;
}

static inline pot_record_t *
__record_new_variable_length(pot_record_type_t type, uint64_t extra_bytes)
{
    pot_tls_t * tls = pot_tls_get();

    // timestamp
    pot_time_spec_t timespec = __get_timespec();
    uint64_t time;
    if (timespec == POT_TIME_SPEC_US)
    {
        struct timeval tp;
        gettimeofday(&tp, NULL);
        time = (uint64_t)(tp.tv_sec * 1000000) + (uint64_t) tp.tv_usec;
    }
    else if (timespec == POT_TIME_SPEC_NS)
    {
        struct timespec ts;
        // clock_gettime(CLOCK_MONOTONIC, &ts);
        clock_gettime(CLOCK_BOOTTIME, &ts);
        time = (uint64_t)(ts.tv_sec * 1000000000) + (uint64_t) ts.tv_nsec;
    }
    else
    {
        POT_ERROR("Wrong timespec '%d'", timespec);
        return NULL;
    }

    // allocate
    size_t size = pot_record_sizeof_type(type);
    if (size == (size_t) -1)
        return NULL;
    size += extra_bytes;

    if (tls->used + size >= tls->capacity)
    {
        POT_ERROR("Out of records memory, please increase 'POT_MEMORY' (current value is '%lu')", tls->capacity);
        exit(1);
    }

    pot_record_t * record = (pot_record_t *) (tls->records + tls->used);
    record->type = type;
    record->time = time;

    tls->used += size;

    return record;

}

static inline pot_record_t *
__record_new(pot_record_type_t type)
{
    return __record_new_variable_length(type, 0);
}

// Records

/////////
// OMP //
/////////

void
pot_omp_thread_begin(pot_omp_thread_type_t type, uint32_t tid)
{
    pot_record_omp_thread_begin_t * record = (pot_record_omp_thread_begin_t *) __record_new(POT_RECORD_OMP_THREAD_BEGIN);
    record->type = type;
    record->tid = tid;
}

void
pot_omp_thread_end(uint32_t tid)
{
    pot_record_omp_thread_end_t * record = (pot_record_omp_thread_end_t *) __record_new(POT_RECORD_OMP_THREAD_END);
    record->tid = tid;
}

void
pot_omp_task_create(uint32_t tlid, pot_omp_task_flag_t flags, int32_t color, int32_t priority, int32_t has_dependences)
{
    pot_record_omp_task_create_t * record = (pot_record_omp_task_create_t *) __record_new(POT_RECORD_OMP_TASK_CREATE);
    record->tlid = tlid;
    record->flags = flags;
    record->color = color;
    record->priority = priority;
    record->has_dependences = has_dependences;
}

void
pot_omp_task_implicit(uint32_t tlid, ompt_scope_endpoint_t endpoint)
{
    pot_record_omp_task_implicit_t * record = (pot_record_omp_task_implicit_t *) __record_new(POT_RECORD_OMP_TASK_IMPLICIT);
    record->tlid = tlid;
    record->endpoint = endpoint;
}

void
pot_omp_task_label(uint32_t tlid, char * label)
{
    uint64_t len = strlen(label) + 1;
    pot_record_omp_task_label_t * record = (pot_record_omp_task_label_t *) __record_new_variable_length(POT_RECORD_OMP_TASK_LABEL, len);
    record->tlid = tlid;
    record->len = len;

    char * dst = (char *) (record + 1);
    memcpy(dst, label, len);
    dst[len] = 0;
}

void
pot_omp_task_schedule(pot_omp_task_uid_t prev_uid, ompt_task_status_t prev_status, pot_omp_task_uid_t next_uid)
{
    pot_record_omp_task_schedule_t * record = (pot_record_omp_task_schedule_t *) __record_new(POT_RECORD_OMP_TASK_SCHEDULE);
    record->prev_uid = prev_uid;
    record->prev_status = prev_status;
    record->next_uid = next_uid;
}

void
pot_omp_task_delete(pot_omp_task_uid_t uid)
{
    // TODO: not supported by ompt yet
    pot_record_omp_task_delete_t * record = (pot_record_omp_task_delete_t *) __record_new(POT_RECORD_OMP_TASK_DELETE);
    record->uid = uid;
}

void
pot_omp_task_depend(uint32_t tlid, pot_depend_t * deps, uint64_t ndeps)
{
    uint64_t size = ndeps * sizeof(pot_depend_t);
    pot_record_omp_task_depend_t * record = (pot_record_omp_task_depend_t *) __record_new_variable_length(POT_RECORD_OMP_TASK_DEPEND, size);
    record->tlid = tlid;
    record->ndeps = ndeps;
    memcpy(record + 1, deps, ndeps * sizeof(pot_depend_t));
}

void
pot_omp_task_dependency(pot_omp_task_uid_t pred, pot_omp_task_uid_t succ)
{
    pot_record_omp_task_dependency_t * record = (pot_record_omp_task_dependency_t *) __record_new(POT_RECORD_OMP_TASK_DEPENDENCY);
    record->pred = pred;
    record->succ = succ;
}

void
pot_omp_task_discarded(pot_omp_task_uid_t uid)
{
    pot_record_omp_task_discarded_t * record = (pot_record_omp_task_discarded_t *) __record_new(POT_RECORD_OMP_TASK_DISCARDED);
    record->uid = uid;
}

void
pot_omp_sync_begin(void)
{
    __record_new(POT_RECORD_OMP_SYNC_BEGIN);
}

void
pot_omp_sync_end(void)
{
    __record_new(POT_RECORD_OMP_SYNC_END);
}

void
pot_omp_group_begin(void)
{
    __record_new(POT_RECORD_OMP_GROUP_BEGIN);
}

void
pot_omp_group_cancel(pot_omp_task_uid_t uid)
{
    pot_record_omp_group_cancel_t * record = (pot_record_omp_group_cancel_t *) __record_new(POT_RECORD_OMP_GROUP_CANCEL);
    record->uid = uid;
}


void
pot_omp_group_end(void)
{
    __record_new(POT_RECORD_OMP_GROUP_END);
}

/////////
// MPI //
/////////
void
pot_mpi_rank(pot_mpi_communicator_t communicator, int rank)
{
    POT_FATAL("impl me");
}

static inline void
pot_mpi_req_p2p_set(const void * buf, int count, pot_mpi_datatype_t datatype, int rank, int tag, pot_mpi_communicator_t communicator, pot_record_mpi_req_p2p_t * record)
{
    assert(record);
    record->buf = buf;
    record->count = count;
    record->datatype = datatype;
    record->rank = rank;
    record->tag = tag;
    record->communicator = communicator;
}

void
pot_mpi_req_send(const void * buf, int count, pot_mpi_datatype_t datatype, int dest, int tag, pot_mpi_communicator_t communicator)
{
    pot_record_mpi_req_send_t * record = (pot_record_mpi_req_send_t *) __record_new(POT_RECORD_MPI_REQ_SEND);
    pot_mpi_req_p2p_set(buf, count, datatype, dest, tag, communicator, record);
}

void
pot_mpi_req_recv(void * buf, int count, pot_mpi_datatype_t datatype, int src, int tag, pot_mpi_communicator_t communicator)
{
    pot_record_mpi_req_recv_t * record = (pot_record_mpi_req_recv_t *) __record_new(POT_RECORD_MPI_REQ_RECV);
    pot_mpi_req_p2p_set(buf, count, datatype, src, tag, communicator, record);
}

static inline void
pot_mpi_req_ip2p_set(pot_mpi_request_t uid, const void * buf, int count, pot_mpi_datatype_t datatype, int rank, int tag, pot_mpi_communicator_t communicator, pot_record_mpi_req_ip2p_t * record)
{
    assert(record);
    record->uid = uid;
    pot_mpi_req_p2p_set(buf, count, datatype, rank, tag, communicator, &(record->parent));
}

void
pot_mpi_req_isend(pot_mpi_request_t uid, const void * buf, int count, pot_mpi_datatype_t datatype, int dest, int tag, pot_mpi_communicator_t communicator)
{
    pot_record_mpi_req_isend_t * record = (pot_record_mpi_req_isend_t *) __record_new(POT_RECORD_MPI_REQ_ISEND);
    pot_mpi_req_ip2p_set(uid, buf, count, datatype, dest, tag, communicator, record);
}

void
pot_mpi_req_send_init(pot_mpi_request_t uid, const void * buf, int count, pot_mpi_datatype_t datatype, int dest, int tag, pot_mpi_communicator_t communicator)
{
    pot_record_mpi_req_send_init_t * record = (pot_record_mpi_req_send_init_t *) __record_new(POT_RECORD_MPI_REQ_SEND_INIT);
    pot_mpi_req_ip2p_set(uid, buf, count, datatype, dest, tag, communicator, record);
}

void
pot_mpi_req_irecv(pot_mpi_request_t uid, void * buf, int count, pot_mpi_datatype_t datatype, int src, int tag, pot_mpi_communicator_t communicator)
{
    pot_record_mpi_req_irecv_t * record = (pot_record_mpi_req_irecv_t *) __record_new(POT_RECORD_MPI_REQ_IRECV);
    pot_mpi_req_ip2p_set(uid, buf, count, datatype, src, tag, communicator, record);
}

void
pot_mpi_req_start(pot_mpi_request_t uid)
{
    pot_record_mpi_req_start_t * record = (pot_record_mpi_req_start_t *) __record_new(POT_RECORD_MPI_REQ_START);
    record->uid = uid;
}

static inline void
pot_mpi_req_partitionned_set(pot_mpi_request_t uid, const void * buf, int npartites, int count, pot_mpi_datatype_t datatype, int rank, int tag, pot_mpi_communicator_t communicator, pot_record_mpi_req_partitionned_init_t * record)
{
    assert(record);
    record->uid = uid;
    record->buf = buf;
    record->npartites = npartites;
    record->count_per_partite = count;
    record->datatype = datatype;
    record->rank = rank;
    record->tag = tag;
    record->communicator = communicator;
}

void
pot_mpi_req_psend_init(pot_mpi_request_t uid, const void * buf, int npartites, int count_per_partite, pot_mpi_datatype_t datatype, int dest, int tag, pot_mpi_communicator_t communicator)
{
    pot_record_mpi_req_psend_init_t * record = (pot_record_mpi_req_psend_init_t *) __record_new(POT_RECORD_MPI_REQ_PSEND_INIT);
    pot_mpi_req_partitionned_set(uid, buf, npartites, count_per_partite, datatype, dest, tag, communicator, record);
}

void
pot_mpi_req_precv_init(pot_mpi_request_t uid, void * buf, int npartites, int count_per_partite, pot_mpi_datatype_t datatype, int src, int tag, pot_mpi_communicator_t communicator)
{
    pot_record_mpi_req_precv_init_t * record = (pot_record_mpi_req_precv_init_t *) __record_new(POT_RECORD_MPI_REQ_PRECV_INIT);
    pot_mpi_req_partitionned_set(uid, buf, npartites, count_per_partite, datatype, src, tag, communicator, record);
}

void
pot_mpi_req_wait(pot_mpi_request_t uid, pot_mpi_req_phase_t phase)
{
    pot_record_mpi_req_wait_t * record = (pot_record_mpi_req_wait_t *) __record_new(POT_RECORD_MPI_REQ_WAIT);
    record->uid = uid;
    record->phase = phase;
}

void
pot_mpi_req_waitall(int count, pot_mpi_request_t array_of_requests[], pot_mpi_req_phase_t phase)
{
    POT_FATAL("impl me");
}

void
pot_mpi_req_test_success(pot_mpi_request_t uid)
{
    pot_record_mpi_req_test_success_t * record = (pot_record_mpi_req_test_success_t *) __record_new(POT_RECORD_MPI_REQ_TEST_SUCCESS);
    record->uid = uid;
}

void
pot_mpi_req_pready(int partite, pot_mpi_request_t uid)
{
    pot_record_mpi_req_pready_t * record = (pot_record_mpi_req_pready_t *) __record_new(POT_RECORD_MPI_REQ_PREADY);
    record->partite = partite;
    record->uid = uid;
}

void
pot_mpi_init(int required, int provided, pot_mpi_communicator_t world_comm, int world_rank, int world_size, char * version, pot_mpi_request_t request_null, pot_mpi_request_t request_empty)
{
    int len = strlen(version) + 1;

    pot_record_mpi_init_t * record = (pot_record_mpi_init_t *) __record_new_variable_length(POT_RECORD_MPI_INIT, len);
    record->required = required;
    record->provided = provided;
    record->world.comm = world_comm;
    record->world.rank = world_rank;
    record->world.size = world_size;
    record->requests.null = request_null;
    record->requests.empty = request_empty;
    record->version_len = len;

    char * dst = (char *) (record + 1);
    memcpy(dst, version, len);
    dst[len] = 0;
}

//////////////
//  Linux   //
//////////////
void
pot_linux_rusage(void)
{
    pot_record_linux_rusage_t * record = (pot_record_linux_rusage_t *) __record_new(POT_RECORD_LINUX_RUSAGE);
    getrusage(RUSAGE_THREAD, &record->rusage);
}

// Initialization, must be called at least once before any other call, and does
// not return until the POT environement is initialized
void
pot_init(void)
{
    static spinlock_t INIT_MTX = 0;
    static volatile int INITIALIZED = 0;

    SPINLOCK_LOCK(INIT_MTX);
    {
        if (INITIALIZED == 0)
        {
            # if 0
            for (int i = 0 ; i < POT_RECORD_TYPE_COUNT ; ++i)
            {
                uint32_t size = pot_record_sizeof_type((pot_record_type_t) i);
                POT_DEBUG("Record %d size is %u", i, size);
            }
            # endif
            if (getenv("POT_VERBOSITY"))
                pot_logger_set_verbosity(atoi(getenv("POT_VERBOSITY")));

            INITIALIZED = 1;
        }
    }
    SPINLOCK_UNLOCK(INIT_MTX);
}

int POT_PAUSED = 0;

void
pot_pause(void)
{
    POT_PAUSED = 1;
}

void
pot_resume(void)
{
    assert(POT_PAUSED == 1);
    POT_PAUSED = 0;
}
