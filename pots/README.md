# POT Simulator (POTS)

## Usage
```
> pots -h
Usage: pots [OPTION...]
This program replays the schedule from a given MPC Trace to perform analysis

  -?, --help                 Give this help list
  -d, --dump                 Dump records during the loading phase
  -i, --input=DIRECTORY      List of available passes:
                             dummy,time-breakdown,stats,gantt-chrome,dump,task-creation-time,Tikki,ready-over-time,average-parallelism,time-per-task,mpi,omp,mpi-omp-task-overlap
  -p, --passes=pass1,pass2,...
  -s, --separator=separator  CSV separator character
      --usage                Give a short usage message
  -V, --version              Print program version
  -x, --export=FILEPATH      Export configuration to FILEPATH

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

Report bugs to <romain.pereira@inria.fr>.
```

## Dev
To implement a new pass, hack by grepping `PASS_NONE` and copy/paste code
