#ifndef __ARRAY_H__
# define __ARRAY_H__

#include "stdint.h"

typedef struct  array_s
{
    union {
        uint8_t * objs;
    };

    // capacity
    uint32_t capacity;

    // number of objects set
    uint32_t n;

    // size of an object
    uint32_t objsize;
}               array_t;

# define ARRAY_INITIALIZE_STATIC {{NULL}, 0, 0}

void array_init(array_t * array, uint32_t default_capacity, uint32_t objsize);
void * array_push(array_t * array, void * obj);
void * array_last(array_t * array);
void * array_penultimate(array_t * array);
void * array_first(array_t * array);
int array_is_empty(array_t * array);
void array_clear(array_t * array);
void array_deinit(array_t * array);

# define ARRAY_FOREACH_BEGIN(A, T, X)                   \
    do {                                                \
        for (int X##i = 0 ; X##i < (A)->n ; ++X##i) {   \
            T X = ((T) (A)->objs) + X##i;

# define ARRAY_FOREACH_END(A, T, X)             \
        }                                       \
    } while (0);

#endif /* __ARRAY_H__ */
