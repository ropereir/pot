# include <pot/logger.h>

# include "clo.h"
# include "pass.h"

# include <argp.h>
# include <assert.h>
# include <string.h>

clo_t CLO = { 0 };

const char * argp_program_version = "POT Simulator 1.0";
const char * argp_program_bug_address = "<romain.pereira@inria.fr>";
static char doc[] = "This program replays events from a given POT Trace to perform analysis";
static char args_doc[] = "";
static struct argp_option options[] = {
    { NULL, 'h', 0, OPTION_HIDDEN, NULL, -1 },
    { .name="input", .key='i', .arg="DIRECTORY", .flags=0, .doc="The input trace directory", .group=0},
    { .name="passes", .key='p', .arg="pass1,pass2,...", .flags=0, .doc=NULL, .group=0},
    { .name="separator", .key='s', .arg="separator", .flags=0, .doc="CSV separator character", .group=0},
    { .name="dump", .key='d', .arg="FILEPATH", .flags=0, .doc="Dump the pass dependency graph to FILEPATH", .group=0},
    { .name="export", .key='x', .arg="FILEPATH", .flags=0, .doc="Export configuration to FILEPATH", .group=0},
    { 0 }
};

static error_t
parse_opt(int key, char * arg, struct argp_state * state)
{
	/* Get the input argument from argp_parse, which we
	know is a pointer to our clo structure. */
	clo_t * clo = (clo_t *) state->input;

	switch (key)
	{
		case 'i':
        {
			CLO.input = arg;
			break;
        }

        case 'p':
        {
            char * token = strtok(arg, ",");
            while (token)
            {
                int found = 0;
                FOREACH_PASS_BEGIN(pass);
                {
                    if (strcmp(pass->name, token) == 0)
                    {
                        found = 1;
                        pass_enable(pass);
                    }
                }
                FOREACH_PASS_END(pass);
                if (!found)
                {
                    POT_WARN("Pass '%s' does not exists", token);
                    POT_WARN(options[1].doc, token);
                    argp_usage(state);
                }
                token = strtok(NULL, ",");
            }
            break ;
        }

        case 'd':
        {
            CLO.export_pdg = arg;
            break ;
        }

        case 's':
        {
            POT_WARN("Option '-s' is deprecated, please use configuration files (please refer to README.md)");
            break ;
        }

        case 'x':
        {
            CLO.export_conf = arg;
            break ;
        }

        case 'h':
        {
            argp_state_help(state, state->out_stream, ARGP_HELP_STD_HELP);
            break;
        }

		case ARGP_KEY_ARG:
			argp_usage(state);
			break;

		case ARGP_KEY_END:
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

static int
cmp_pass_name(const void * x, const void * y)
{
    const int i = *((const int *) x);
    const int j = *((const int *) y);
    return strcmp(PASSES[i]->name, PASSES[j]->name);
}

void
clo_parse(int argc, char ** argv)
{
    static const char * default_input = "pot-trace";
    // set default
    CLO.input = (char *) default_input;
    CLO.export_conf = NULL;
    CLO.export_pdg = NULL;

    const char * header = "List of available passes: ";
    const size_t header_len = strlen(header);

    size_t passes_name_len = header_len;
    int pass_sorted_indices[__PASS_COUNT__];
    for (int i = 0 ; i < __PASS_COUNT__ ; ++i)
    {
        pass_t * pass = PASSES[i];
        pass_sorted_indices[i] = i;
        passes_name_len += strlen(pass->name) + 1;
    }
    qsort(pass_sorted_indices, __PASS_COUNT__, sizeof(int), cmp_pass_name);

    char * passes_name = (char *) malloc(passes_name_len + 1);
    assert(passes_name);

    strcpy(passes_name, header);
    size_t index = header_len;

    for (int i = 0 ; i < __PASS_COUNT__ ; ++i)
    {
        pass_t * pass = PASSES[pass_sorted_indices[i]];

        int pass_name_len = strlen(pass->name) + 1;
        int r = snprintf(passes_name + index, pass_name_len + 1, "%s,", pass->name);
        index += pass_name_len;
        assert(r == pass_name_len);
    }
    assert(index == passes_name_len);
    passes_name[index - 1] = 0;
    options[1].doc = passes_name;

    // parse
    argp_parse(&argp, argc, argv, 0, 0, &CLO);
}
