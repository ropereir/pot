#ifndef __CLO_H__
# define __CLO_H__

typedef struct  clo_s
{
    char * input;
    char * export_conf;
    char * export_pdg;
}               clo_t;

void clo_parse(int argc, char ** argv);

extern clo_t CLO;

#endif /* __CLO_H__ */
