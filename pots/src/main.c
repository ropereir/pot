# include <pot/logger.h>

# include "clo.h"
# include "simulator.h"

# if defined(_OPENMP)
#  include <omp.h>
# endif

# include <time.h>

int
main(int argc, char ** argv)
{
    # pragma omp parallel
    {
        # pragma omp single
        {
            # if defined(_OPENMP)
            int nthreads = omp_get_num_threads();
            # else
            int nthreads = 1;
            # endif
            POT_INFO("Running post-process on %u threads", nthreads);

            struct timespec t0;
            clock_gettime(CLOCK_MONOTONIC, &t0);

            // parse command line options
            passes_init();
            clo_parse(argc, argv);
            passes_conf();

            // start simulator
            simulate();

            struct timespec tf;
            clock_gettime(CLOCK_MONOTONIC, &tf);
            double dt = (double) (tf.tv_sec - t0.tv_sec) + ((tf.tv_nsec - t0.tv_nsec) / 1000000000.0);
            POT_INFO("Post-process in %lf s.", dt);
        }
    }
    return 0;
}
