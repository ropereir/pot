# include <pot/logger.h>

# include "clo.h"
# include "pass.h"

# include <assert.h>
# include <errno.h>
# include <stdlib.h>
# include <string.h>

///////////////////////////////
// MODIFY HERE TO ADD A PASS //
///////////////////////////////

// passes
extern pass_t pass_none;
extern pass_t pass_timebreakdown;
extern pass_t pass_stats;
extern pass_t pass_ganttchrome;
extern pass_t pass_dump;
extern pass_t pass_taskcreationtime;
extern pass_t pass_tikki;
extern pass_t pass_readyovertime;
extern pass_t pass_averageparallelism;
extern pass_t pass_timepertask;
extern pass_t pass_mpi;
extern pass_t pass_omp;
extern pass_t pass_mpi_omp_task_overlap;
extern pass_t pass_mpi_request_started_over_time;
extern pass_t pass_count;

// passes must be added to the array so that `PASSES[id]->id == id`
pass_t * PASSES[__PASS_COUNT__] = {
    &pass_none,
    &pass_timebreakdown,
    &pass_stats,
    &pass_ganttchrome,
    &pass_dump,
    &pass_taskcreationtime,
    &pass_tikki,
    &pass_readyovertime,
    &pass_averageparallelism,
    &pass_timepertask,
    &pass_mpi,
    &pass_omp,
    &pass_mpi_omp_task_overlap,
    &pass_mpi_request_started_over_time,
    &pass_count
};

//////////////////////////
// DO NOT MODIFY BELLOW //
//////////////////////////

pass_t * PASSES_SORTED[__PASS_COUNT__];

static int
_pass_cmp_rank(const void * x, const void * y)
{
    const pass_t * p1 = *((const pass_t **) x);
    const pass_t * p2 = *((const pass_t **) y);
    return p1->rank - p2->rank;
}

// TODO : move this to POT kernel ?

static json_object * conf;

void
passes_init(void)
{
    // check that passes are inserted correctly in the array
    FOREACH_PASS_BEGIN(pass);
    {
        if (pass == NULL)
            POT_FATAL("The `PASSES` array is wrong! The pass `%d` is NULL", _i);
        else if (_i != pass->id)
            POT_FATAL("The `PASSES` array is wrong! The pass `%s` is inserted at the `%d-th` instead of the `%d-ith` index", pass->name, _i, pass->id);
    }
    FOREACH_PASS_END(pass);
}

static void
passes_compute_rank(void)
{
    int changed = 1;
    while (changed)
    {
        changed = 0;
        FOREACH_PASS_BEGIN(pass);
        {
            FOREACH_PASS_DEPENDENCES_BEGIN(pass, dep, when)
            {
                pass_t * pred = (when == BEFORE) ? pass : dep;
                pass_t * succ = (when == BEFORE) ? dep  : pass;

                if (succ->rank <= pred->rank)
                {
                    succ->rank = pred->rank + 1;
                    if (succ->rank >= __PASS_COUNT__ + 1)
                        POT_FATAL("Cycle detected in passes dependency graph detected while processing pass `%s`", succ->name);
                    changed = 1;
                }
            }
            FOREACH_PASS_DEPENDENCES_END(pass, dep, when);
        }
        FOREACH_PASS_END(pass);
    }
}

void
passes_conf(void)
{
    // load default conf
    conf = json_object_new_object();
    FOREACH_PASS_BEGIN(pass)
    {
        if ((pass->conf.init && !pass->conf.load) || (!pass->conf.init && pass->conf.load))
        {
            POT_ERROR("Pass '%s' must define whether both or none of 'conf.load' and 'conf.init'", pass->name);
            exit(1);
        }

        if (!pass->conf.init)
            continue ;

        json_object * pconf = json_object_new_object();
        if (pass->conf.init)
            pass->conf.init(pconf);

        json_object_object_add(conf, pass->name, pconf);
    }
    FOREACH_PASS_END(pass);

    // read user conf
    json_object * user_conf = NULL;
    char * user_conf_path = getenv("POT_CONF");
    if (user_conf_path)
    {
        POT_INFO("Reading user configuration '%s'", user_conf_path);
        user_conf = json_object_from_file(user_conf_path);
        if (user_conf == NULL)
            POT_WARN("Could not load conf file '%s'", user_conf_path);
    }

    // merge user conf into default
    if (user_conf)
        json_object_merge(conf, user_conf);

    assert(conf);

    if (CLO.export_conf)
    {
        POT_INFO("Saving configuration to '%s' ...", CLO.export_conf);
        int r = json_object_to_file_ext(CLO.export_conf, conf, JSON_C_TO_STRING_PRETTY);
        if (r)
        {
            POT_ERROR("Could not export configuration (err=%d)", r);
            perror(CLO.export_conf);
        }
    }

    // signal passes with final conf
    FOREACH_PASS_BEGIN(pass)
    {
        if (!pass->conf.load)
            continue ;

        if (pass->conf.load)
        {
            json_object * pconf = json_object_object_get(conf, pass->name);
            pass->conf.load(pconf);
        }
    }
    FOREACH_PASS_END(pass);

    // compute rank of each pass
    passes_compute_rank();

    // sort passes by rank
    memcpy(PASSES_SORTED, PASSES, sizeof(PASSES));
    qsort(PASSES_SORTED, sizeof(PASSES_SORTED) / sizeof(pass_t *), sizeof(pass_t *), _pass_cmp_rank);

    POT_INFO("Passes will execute in the following order:");
    FOREACH_ENABLED_PASS_SORTED_BEGIN(pass)
    {
        POT_INFO("Enabled pass `%s` of rank %d", pass->name, pass->rank);
    }
    FOREACH_ENABLED_PASS_SORTED_END(pass);

    // export the pass dependency graph
    const char * pdg = CLO.export_pdg;
    if (pdg)
    {
        POT_INFO("Exporting the pass dependency graph to `%s`", pdg);
        FILE * file = fopen(pdg, "w");
        if (file)
        {
            int r = 0;

            // digraph begin
            r = (r < 0) ? r : fprintf(file, "digraph G {\n");

            // nodes
            FOREACH_PASS_BEGIN(pass)
            {
                r = (r < 0) ? r : fprintf(file, "    N%u [label=\"%s\\n%s\"]\n", pass->id, pass->name, pass->desc);
            }
            FOREACH_PASS_END(pass);

            // edges
            FOREACH_PASS_BEGIN(pass)
            {
                FOREACH_PASS_DEPENDENCES_BEGIN(pass, dep, when)
                {
                    int pred = (when == BEFORE) ? pass->id : dep->id;
                    int succ = (when == BEFORE) ? dep->id  : pass->id;
                    r = (r < 0) ? r : fprintf(file, "    N%u -> N%u\n", pred, succ);
                }
                FOREACH_PASS_DEPENDENCES_END(pass, dep, when);
            }
            FOREACH_PASS_END(pass);

            // digraph end
            r = (r < 0) ? r : fprintf(file, "}\n");

            if (r < 0)
                POT_ERROR("Error writing file `%s` : %s", pdg, strerror(errno));

            if (fclose(file) != 0)
                POT_ERROR("Error closing file `%s` : %s", pdg, strerror(errno));
        }
        else
            POT_ERROR("Error opening file `%s` : %s", pdg, strerror(errno));
    }
}

// enable a pass and its dependences
void
pass_enable(pass_t * pass)
{
    if (pass->enabled)
        return ;

    POT_INFO("Enabling pass '%s' - %s", pass->name, pass->desc);
    pass->enabled = 1;
    FOREACH_PASS_DEPENDENCES_BEGIN(pass, dep, when)
    {
        pass_enable(dep);
    }
    FOREACH_PASS_DEPENDENCES_END(pass, dep, when);
}
