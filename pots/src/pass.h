#ifndef __PASS_H__
# define __PASS_H__

# include <pot/records.h>
# include <json-c/json.h>

// should be large enough, increase if you need to
# define PASS_MAX_DEPS 8

// Passes must be sorted with respect of their dependences
typedef enum    pass_enum_e
{
    PASS_NONE = 0,
    PASS_TIMEBREAKDOWN,
    PASS_STATS,
    PASS_GANTTCHROME,
    PASS_DUMP,
    PASS_TASKCREATIONTIME,
    PASS_TIKKI,
    PASS_READYOVERTIME,
    PASS_AVERAGEPARALLELISM,
    PASS_TIMEPERTASK,
    PASS_MPI,
    PASS_OMP,
    PASS_MPI_OMP_TASK_OVERLAP,
    PASS_MPI_REQUEST_STARTED_OVER_TIME,
    PASS_COUNT,

    // add your pass here

    __PASS_COUNT__

}               pass_enum_t;

struct simulator_s;
struct process_s;

typedef struct  pass_callback_s
{
    /* call once before inorder simulation begins */
    void (*start_callback)(struct simulator_s *, struct process_s *);

    /* call once after inorder simulation finishes */
    void (*fini_callback)(struct simulator_s *, struct process_s *);

    /* Callbacks for inorder analysis, raised before simulator update */
    void (*record_callback[POT_RECORD_TYPE_COUNT])(struct simulator_s *, struct process_s *);

}               pass_callback_t;

typedef struct  pass_conf_s
{
    void (*init)(json_object * conf);
    void (*load)(json_object * conf);
}               pass_conf_t;

typedef enum    pass_dependence_order_e
{
    BEFORE,
    AFTER,
    IRRELEVANT
}               pass_dependence_order_t;

typedef struct  pass_dependence_s
{
    pass_dependence_order_t order;
    pass_enum_t pass;
}               pass_dependence_t;

typedef struct  pass_s
{
    /* pass id */
    pass_enum_t id;

    /* Pass name */
    const char * name;

    /* Pass description */
    const char * desc;

    /* Pass dependencies */
    pass_dependence_t dependences[PASS_MAX_DEPS];

    /* inorder callbacks */
    pass_callback_t inorder;

    /* pass config */
    pass_conf_t conf;

    /* Whether the pass is enabled or not */
    int enabled;

    /* Rank (= priority with respect to dependencies */
    int rank;
}               pass_t;

// all passes (ordered by id)
extern pass_t * PASSES[__PASS_COUNT__];

// all passes (ordered by rank) */
extern pass_t * PASSES_SORTED[__PASS_COUNT__];

void passes_init(void);

// must be called at program launch to initialize passes conf
void passes_conf(void);

// enable a pass and its dependences
void pass_enable(pass_t * pass);

// for each pass
# define FOREACH_PASS_BEGIN(NAME)                               \
    do {                                                        \
        for (int _i = 0 ; _i < __PASS_COUNT__ ; ++_i )          \
        {                                                       \
            pass_t * NAME = PASSES[_i];
# define FOREACH_PASS_END(NAME)                                 \
        }                                                       \
    } while(0)

// for each pass sorted
# define FOREACH_PASS_SORTED_BEGIN(NAME)                        \
    do {                                                        \
        for (int _i = 0 ; _i < __PASS_COUNT__ ; ++_i )          \
        {                                                       \
            pass_t * NAME = PASSES_SORTED[_i];
# define FOREACH_PASS_SORTED_END(NAME)                          \
        }                                                       \
    } while(0)

// foreach dependences 'D' of the pass 'P'
# define FOREACH_PASS_DEPENDENCES_BEGIN(P, D, W)                                                \
    do {                                                                                        \
        for (int _i = 0 ; _i < PASS_MAX_DEPS && (P)->dependences[_i].pass != PASS_NONE ; ++_i)  \
        {                                                                                       \
            pass_dependence_order_t W = (P)->dependences[_i].order;                             \
            pass_t * D = PASSES[(P)->dependences[_i].pass];
# define FOREACH_PASS_DEPENDENCES_END(P, D, W)                                                  \
        }                                                                                       \
    } while(0)

// for each pass enabled
# define FOREACH_ENABLED_PASS_BEGIN(NAME)                       \
    do {                                                        \
        for (int _i = 0 ; _i < __PASS_COUNT__ ; ++_i )          \
        {                                                       \
            pass_t * NAME = PASSES[_i];                         \
            if (NAME->enabled)                                  \
            {
# define FOREACH_ENABLED_PASS_END(NAME)                         \
            }                                                   \
        }                                                       \
    } while(0)

// for each pass enabled and sorted by rank
# define FOREACH_ENABLED_PASS_SORTED_BEGIN(NAME)                \
    do {                                                        \
        for (int _i = 0 ; _i < __PASS_COUNT__ ; ++_i )          \
        {                                                       \
            pass_t * NAME = PASSES_SORTED[_i];                  \
            if (NAME->enabled)                                  \
            {
# define FOREACH_ENABLED_PASS_SORTED_END(NAME)                  \
            }                                                   \
        }                                                       \
    } while(0)

#endif /* __PASS_H__ */
