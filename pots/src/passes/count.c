# include <pot/logger.h>

# include "pass.h"
# include "simulator.h"

static int records_count[POT_RECORD_TYPE_COUNT];

static void
fini(simulator_t * simulator, process_t * process)
{
    POT_INFO("Records count");
    for (pot_record_type_t t = 0 ; t < POT_RECORD_TYPE_COUNT ; ++t)
        POT_INFO("  `%s` : %d", record_type_to_str(t), records_count[t]);
}

static void
count(simulator_t * simulator, process_t * process)
{
    pot_record_t * record = process->current_thread->current_record;
    ++records_count[record->type];
}

pass_t pass_count = {
    .id   = PASS_COUNT,
    .name = "count",
    .desc = "A pass that count records",
    .dependences = {},
    .inorder = {
        .start_callback = NULL,
        .fini_callback = fini,
        .record_callback = {
            count,           // POT_RECORD_OMP_THREAD_BEGIN,
            count,           // POT_RECORD_OMP_THREAD_END,
            count,           // POT_RECORD_OMP_TASK_IMPLICIT,
            count,           // POT_RECORD_OMP_TASK_CREATE,
            count,           // POT_RECORD_OMP_TASK_LABEL,
            count,           // POT_RECORD_OMP_TASK_SCHEDULE,
            count,           // POT_RECORD_OMP_TASK_DELETE,
            count,           // POT_RECORD_OMP_TASK_DEPEND,
            count,           // POT_RECORD_OMP_TASK_DEPENDENCY,
            count,           // POT_RECORD_OMP_TASK_DISCARDED,
            count,           // POT_RECORD_OMP_SYNC_BEGIN,
            count,           // POT_RECORD_OMP_SYNC_END,
            count,           // POT_RECORD_OMP_GROUP_BEGIN,
            count,           // POT_RECORD_OMP_GROUP_CANCEL,
            count,           // POT_RECORD_OMP_GROUP_END,

            count,           // POT_RECORD_MPI_INIT,
            count,           // POT_RECORD_MPI_RANK,
            count,           // POT_RECORD_MPI_REQ_START,
            count,           // POT_RECORD_MPI_REQ_SEND,
            count,           // POT_RECORD_MPI_REQ_RECV,
            count,           // POT_RECORD_MPI_REQ_ISEND,
            count,           // POT_RECORD_MPI_REQ_SEND_INIT,
            count,           // POT_RECORD_MPI_REQ_IRECV,
            count,           // POT_RECORD_MPI_REQ_PSEND_INIT,
            count,           // POT_RECORD_MPI_REQ_PRECV_INIT,
            count,           // POT_RECORD_MPI_REQ_WAIT,
            count,           // POT_RECORD_MPI_REQ_WAITALL,
            count,           // POT_RECORD_MPI_REQ_TEST_SUCCESS,
            count,           // POT_RECORD_MPI_REQ_PREADY,

            count,           // POT_RECORD_LINUX_RUSAGE,
        },
    },
};
