# include "pass.h"
# include "simulator.h"

static void
dump(simulator_t * simulator, process_t * process)
{
    pot_record_t * record = process->current_thread->current_record;
    pot_record_dump(record, process->current_thread->tid);
    fflush(stdout);
}

pass_t pass_dump = {
    .id   = PASS_DUMP,
    .name = "dump",
    .desc = "A pass that dump records",
    .dependences = {},
    .inorder = {
        .start_callback = NULL,
        .fini_callback = NULL,
        .record_callback = {
            dump,           // POT_RECORD_OMP_THREAD_BEGIN,
            dump,           // POT_RECORD_OMP_THREAD_END,
            dump,           // POT_RECORD_OMP_TASK_IMPLICIT,
            dump,           // POT_RECORD_OMP_TASK_CREATE,
            dump,           // POT_RECORD_OMP_TASK_LABEL,
            dump,           // POT_RECORD_OMP_TASK_SCHEDULE,
            dump,           // POT_RECORD_OMP_TASK_DELETE,
            dump,           // POT_RECORD_OMP_TASK_DEPEND,
            dump,           // POT_RECORD_OMP_TASK_DEPENDENCY,
            dump,           // POT_RECORD_OMP_TASK_DISCARDED,
            dump,           // POT_RECORD_OMP_SYNC_BEGIN,
            dump,           // POT_RECORD_OMP_SYNC_END,
            dump,           // POT_RECORD_OMP_GROUP_BEGIN,
            dump,           // POT_RECORD_OMP_GROUP_CANCEL,
            dump,           // POT_RECORD_OMP_GROUP_END,

            dump,           // POT_RECORD_MPI_INIT,
            dump,           // POT_RECORD_MPI_RANK,
            dump,           // POT_RECORD_MPI_REQ_START,
            dump,           // POT_RECORD_MPI_REQ_SEND,
            dump,           // POT_RECORD_MPI_REQ_RECV,
            dump,           // POT_RECORD_MPI_REQ_ISEND,
            dump,           // POT_RECORD_MPI_REQ_SEND_INIT,
            dump,           // POT_RECORD_MPI_REQ_IRECV,
            dump,           // POT_RECORD_MPI_REQ_PSEND_INIT,
            dump,           // POT_RECORD_MPI_REQ_PRECV_INIT,
            dump,           // POT_RECORD_MPI_REQ_WAIT,
            dump,           // POT_RECORD_MPI_REQ_WAITALL,
            dump,           // POT_RECORD_MPI_REQ_TEST_SUCCESS,
            dump,           // POT_RECORD_MPI_REQ_PREADY,

            dump,           // POT_RECORD_LINUX_RUSAGE,
        },
    },
};
