# include <pot/logger.h>
# include <json-c/json.h>

# include <assert.h>
# include <string.h>

# include "clo.h"
# include "pass.h"
# include "simulator.h"
# include "uthash.h"
# include "passes/omp/omp.h"

// TODO : add support for multi-process

static int TIME_FACTOR = 1;
# define FORMAT_TIME(T) ((uint64_t) (T)/TIME_FACTOR)

static json_object * CONF;
static json_object * ENABLE;

static void
conf_init(json_object * conf)
{
    json_object_object_add(conf, "export", json_object_new_string("gantt-chrome.json"));
    json_object_object_add(conf, "round_to_us", json_object_new_boolean(1));

    json_object * ENABLE = json_object_new_object();
    json_object_object_add(ENABLE, "creation",              json_object_new_boolean(1));
    json_object_object_add(ENABLE, "dependences_all",       json_object_new_boolean(1));
    json_object_object_add(ENABLE, "dependences_reported",  json_object_new_boolean(1));
    json_object_object_add(ENABLE, "schedules",             json_object_new_boolean(1));
    json_object_object_add(ENABLE, "colors",                json_object_new_boolean(1));
    json_object_object_add(ENABLE, "rusage",                json_object_new_boolean(1));

    json_object_object_add(conf, "enable", ENABLE);
}

static void
conf_load(json_object * conf)
{
    CONF = conf;
    ENABLE = json_object_object_get(CONF, "enable");

    POT_WARN("Gantt chrome export : Chrome only supports time in us.");
    if (json_object_get_boolean(json_object_object_get(conf, "round_to_us")))
    {
        TIME_FACTOR = 1000;
        POT_WARN("'round_to_us' set to true - rounding times to us. for Catapult - you may see tasks overlaping or missing dependences if your task grain are ~1us.");
    }
    else
    {
        POT_WARN("'round_to_us' set to false - times will be in nanosecond - Catapult interpret them as microseconds, so multiply visualized times by 1e3");
    }
}

static const char *
gencolor(int color)
{
    static const char * colors[] = {
        "thread_state_uninterruptible",
        "thread_state_iowait",
        "thread_state_running",
        "thread_state_runnable",
        "thread_state_unknown",
        "background_memory_dump",
        "light_memory_dump",
        "detailed_memory_dump",
        "vsync_highlight_color",
        "generic_work",
        "good",
        "bad",
        "terrible",
        "black",
        "grey",
        "white",
        "yellow",
        "olive",
        "rail_response",
        "rail_animation",
        "rail_idle",
        "rail_load",
        "startup",
        "heap_dump_stack_frame",
        "heap_dump_child_node_arrow",
        "cq_build_running",
        "cq_build_passed",
        "cq_build_failed",
        "cq_build_abandoned",
        // "cq_build_attempt_running" // some versions of chrome have a buggy name 'cq_build_attempt_runnig'
        "cq_build_attempt_passed",
        "cq_build_attempt_failed"
    };
    return colors[(color * color * 83) % (sizeof(colors) / sizeof(char *))];
}

/**
 * Events should be :
 * {
 *      'name': STRING,
 *      'ph':   STRING,
 *      'pid':  INT,
 *      'tid':  INT,
 *      'args': {
 *          'name': STRING
 *      }
 */
static json_object * cte, * events;

static void
start(simulator_t * simulator, process_t * process)
{
    cte = json_object_new_object();
    events = json_object_new_array();
    json_object_object_add(cte, "traceEvents", events);
    // json_object_object_add(cte, "displayTimeUnit", json_object_new_string("ms"));

    json_object * other_data = json_object_new_object();
    char s[256];
    snprintf(s, sizeof(s), "POT (trace file version %d)", POT_FILE_VERSION);
    json_object_object_add(other_data, "Version", json_object_new_string(s));
    json_object_object_add(cte, "otherData", other_data);
}

static void
finish(simulator_t * simulator, process_t * process)
{
    // TODO : do the export during the record pass instead of at finish,
    // so we can keep track of task states

    // add every threads
    FOREACH_THREAD_BEGIN(process, thread);
    {
        json_object * th_obj = json_object_new_object();
        json_object_object_add(th_obj, "name",  json_object_new_string("thread_name"));
        json_object_object_add(th_obj, "ph",    json_object_new_string("M"));
        json_object_object_add(th_obj, "pid",   json_object_new_uint64(process->pid));
        json_object_object_add(th_obj, "tid",   json_object_new_uint64(thread->tid));

        json_object * args = json_object_new_object();
        char thread_name[128];
        snprintf(thread_name, sizeof(thread_name), "omp thread %d", thread->tid);
        json_object_object_add(args, "name", json_object_new_string(thread_name));
        json_object_object_add(th_obj, "args",  args);

        json_object_array_add(events, th_obj);
    }
    FOREACH_THREAD_END(simulator, thread);

    // add every explicit tasks
    int schedules = json_object_get_boolean(json_object_object_get(ENABLE, "schedules"));
    int colors = json_object_get_boolean(json_object_object_get(ENABLE, "colors"));
    int create = json_object_get_boolean(json_object_object_get(ENABLE, "creation"));
    if (schedules || create)
    {
        FOREACH_OMP_TASK_BEGIN(process, task);
        {
            if (task->flags.implicit)
                continue ;

            // schedule event
            if (schedules)
            {
                ARRAY_FOREACH_BEGIN(&task->schedules, omp_task_schedule_t *, schedule)
                {
                    json_object * sched_obj = json_object_new_object();

                    if (!schedule->begin || !schedule->end)
                    {
                        POT_WARN("Invalid schedule for task %lu, ignoring it", task->uid.value);
                        continue ;
                    }

                    json_object_object_add(sched_obj, "name", json_object_new_string(task->label ? task->label : "task"));
                    // json_object_object_add(sched_obj, "name", json_object_new_string("task"));
                    json_object_object_add(sched_obj, "pid", json_object_new_uint64(process->pid));
                    json_object_object_add(sched_obj, "tid", json_object_new_uint64(schedule->tid));
                    json_object_object_add(sched_obj, "cat", json_object_new_string("ts"));
                    json_object_object_add(sched_obj, "ph", json_object_new_string("X"));
                    json_object_object_add(sched_obj, "ts", json_object_new_uint64(FORMAT_TIME(schedule->begin->time)));
                    json_object_object_add(sched_obj, "dur", json_object_new_uint64(FORMAT_TIME(schedule->end->time - schedule->begin->time)));
                    if (colors)
                        json_object_object_add(sched_obj, "cname", json_object_new_string(gencolor(task->color)));


                    json_object * args = json_object_new_object();
                    json_object_object_add(args, "uid", json_object_new_uint64(task->uid.value));
                    json_object_object_add(args, "created", json_object_new_uint64(FORMAT_TIME(task->create_time)));
                    json_object_object_add(args, "priority", json_object_new_uint64(task->priority));
                    json_object_object_add(args, "color", json_object_new_uint64(task->color));
                    json_object_object_add(sched_obj, "args", args);

                    json_object_array_add(events, sched_obj);
                }
                ARRAY_FOREACH_END(&task->schedules, pot_record_omp_task_schedule_t **, schedule);
            }

            // creation event
            if (create)
            {
               json_object * create_obj = json_object_new_object();

               json_object_object_add(create_obj, "name", json_object_new_string("c"));
               json_object_object_add(create_obj, "pid", json_object_new_uint64(process->pid));
               json_object_object_add(create_obj, "tid", json_object_new_uint64(task->uid.tid));
               json_object_object_add(create_obj, "cat", json_object_new_string("tc"));
               json_object_object_add(create_obj, "ph", json_object_new_string("X"));
               json_object_object_add(create_obj, "ts", json_object_new_uint64(FORMAT_TIME(task->create_time)));
               json_object_object_add(create_obj, "dur", json_object_new_uint64(1));

               json_object * args = json_object_new_object();
               json_object_object_add(args, "uid", json_object_new_uint64(task->uid.value));
               json_object_object_add(create_obj, "args", args);

               json_object_array_add(events, create_obj);
            }
        }
        FOREACH_OMP_TASK_END(process, task);
    }

    FOREACH_OMP_TASK_BEGIN(process, pred);
    {
        // add every expressed dependences
        if (json_object_get_boolean(json_object_object_get(ENABLE, "dependences_all")))
        {
            ARRAY_FOREACH_BEGIN(&pred->successors, omp_task_t **, succ_ptr)
            {
                omp_task_t * succ = *succ_ptr;

                omp_task_schedule_t * pred_sched = (omp_task_schedule_t *) array_last(&pred->schedules);
                omp_task_schedule_t * succ_sched = (omp_task_schedule_t *) array_first(&succ->schedules);

                assert(pred_sched);
                assert(succ_sched);

                char identifier[512];
                snprintf(identifier, sizeof(identifier), "da-%u-%lu-%lu", process->pid, pred->uid.value, succ->uid.value);

                uint32_t id_hash;
                HASH_VALUE(identifier, strlen(identifier), id_hash);

                json_object * pred_deps_obj = json_object_new_object();
                json_object_object_add(pred_deps_obj, "name", json_object_new_string(identifier));
                json_object_object_add(pred_deps_obj, "pid", json_object_new_int(process->pid));
                json_object_object_add(pred_deps_obj, "tid", json_object_new_int(pred_sched->tid));
                json_object_object_add(pred_deps_obj, "cat", json_object_new_string("deps(a)"));
                json_object_object_add(pred_deps_obj, "ph", json_object_new_string("s"));
                json_object_object_add(pred_deps_obj, "ts", json_object_new_uint64(FORMAT_TIME(pred_sched->end->time)));
                json_object_object_add(pred_deps_obj, "name", json_object_new_string(identifier));
                json_object_object_add(pred_deps_obj, "id", json_object_new_uint64(id_hash));
                json_object_array_add(events, pred_deps_obj);

                json_object * succ_deps_obj = json_object_new_object();
                json_object_object_add(succ_deps_obj, "name", json_object_new_string(identifier));
                json_object_object_add(succ_deps_obj, "pid", json_object_new_int(process->pid));
                json_object_object_add(succ_deps_obj, "tid", json_object_new_int(succ_sched->tid));
                json_object_object_add(succ_deps_obj, "cat", json_object_new_string("deps(a)"));
                json_object_object_add(succ_deps_obj, "ph", json_object_new_string("t"));
                json_object_object_add(succ_deps_obj, "ts", json_object_new_uint64(FORMAT_TIME(succ_sched->begin->time)));
                json_object_object_add(succ_deps_obj, "name", json_object_new_string(identifier));
                json_object_object_add(succ_deps_obj, "id", json_object_new_uint64(id_hash));
                json_object_array_add(events, succ_deps_obj);
            }
            ARRAY_FOREACH_END(&pred->successors, omp_task_t **, succ_ptr);
        }

        // add every unfulfilled dependencies
        if (json_object_get_boolean(json_object_object_get(ENABLE, "dependences_reported")))
        {
            ARRAY_FOREACH_BEGIN(&pred->reported_successors, omp_task_t **, succ_ptr)
            {
                omp_task_t * succ = *succ_ptr;

                omp_task_schedule_t * pred_sched = (omp_task_schedule_t *) array_last(&pred->schedules);
                omp_task_schedule_t * succ_sched = (omp_task_schedule_t *) array_first(&succ->schedules);

                char identifier[512];
                snprintf(identifier, sizeof(identifier), "du-%u-%lu-%lu", process->pid, pred->uid.value, succ->uid.value);

                uint32_t id_hash;
                HASH_VALUE(identifier, strlen(identifier), id_hash);

                json_object * pred_deps_obj = json_object_new_object();
                json_object_object_add(pred_deps_obj, "name", json_object_new_string(identifier));
                json_object_object_add(pred_deps_obj, "pid", json_object_new_int(process->pid));
                json_object_object_add(pred_deps_obj, "tid", json_object_new_int(pred_sched->tid));
                json_object_object_add(pred_deps_obj, "cat", json_object_new_string("deps(u)"));
                json_object_object_add(pred_deps_obj, "ph", json_object_new_string("s"));
                json_object_object_add(pred_deps_obj, "ts", json_object_new_uint64(FORMAT_TIME(pred_sched->end->time)));
                json_object_object_add(pred_deps_obj, "name", json_object_new_string(identifier));
                json_object_object_add(pred_deps_obj, "id", json_object_new_uint64(id_hash));
                json_object_array_add(events, pred_deps_obj);

                json_object * succ_deps_obj = json_object_new_object();
                json_object_object_add(succ_deps_obj, "name", json_object_new_string(identifier));
                json_object_object_add(succ_deps_obj, "pid", json_object_new_int(process->pid));
                json_object_object_add(succ_deps_obj, "tid", json_object_new_int(succ_sched->tid));
                json_object_object_add(succ_deps_obj, "cat", json_object_new_string("deps(u)"));
                json_object_object_add(succ_deps_obj, "ph", json_object_new_string("t"));
                json_object_object_add(succ_deps_obj, "ts", json_object_new_uint64(FORMAT_TIME(succ_sched->begin->time)));
                json_object_object_add(succ_deps_obj, "name", json_object_new_string(identifier));
                json_object_object_add(succ_deps_obj, "id", json_object_new_uint64(id_hash));
                json_object_array_add(events, succ_deps_obj);
            }
            ARRAY_FOREACH_END(&pred->reported_successors, omp_task_t **, succ_ptr);
        }
    }
    FOREACH_OMP_TASK_END(process, task);

    // print file
    char filename[1024];
    char * exportt = (char *) json_object_get_string(json_object_object_get(CONF, "export"));
    assert(exportt);
    if (*exportt != '/')
    {
        strncpy(filename, CLO.input, sizeof(filename) - strlen(exportt) - 2);
        strcat(filename, "/");
        strncat(filename, exportt, strlen(exportt));
        exportt = (char *) filename;
    }

    POT_INFO("Exporting Gantt chart to %s ...", exportt);
    json_object_to_file_ext(exportt, cte, JSON_C_TO_STRING_PRETTY);
    json_object_put(cte);
}

static void
rusage(simulator_t * simulator, process_t * process)
{
    int use = json_object_get_boolean(json_object_object_get(ENABLE, "rusage"));
    if (!use)
        return ;

    thread_t * thread = process->current_thread;
    pot_record_linux_rusage_t * record = (pot_record_linux_rusage_t *) thread->current_record;

    void * rusage = thread->pls[PASS_GANTTCHROME];
    if (rusage == NULL || memcmp(&(record->rusage.ru_maxrss), rusage, sizeof(long) * 14))
    {
        // add event
        json_object * ctxsw = json_object_new_object();

        json_object_object_add(ctxsw, "name", json_object_new_string("ru"));
        json_object_object_add(ctxsw, "pid", json_object_new_uint64(process->pid));
        json_object_object_add(ctxsw, "tid", json_object_new_uint64(thread->tid));
        json_object_object_add(ctxsw, "cat", json_object_new_string("ru"));
        json_object_object_add(ctxsw, "ph", json_object_new_string("X"));
        json_object_object_add(ctxsw, "ts", json_object_new_uint64(FORMAT_TIME(record->parent.time)));
        json_object_object_add(ctxsw, "dur", json_object_new_uint64(1));

        json_object * args = json_object_new_object();
        json_object_object_add(args, "ru_maxrss", json_object_new_uint64(record->rusage.ru_maxrss));
        json_object_object_add(args, "ru_ixrss", json_object_new_uint64(record->rusage.ru_ixrss));
        json_object_object_add(args, "ru_idrss", json_object_new_uint64(record->rusage.ru_idrss));
        json_object_object_add(args, "ru_isrss", json_object_new_uint64(record->rusage.ru_isrss));
        json_object_object_add(args, "ru_minflt", json_object_new_uint64(record->rusage.ru_minflt));
        json_object_object_add(args, "ru_majflt", json_object_new_uint64(record->rusage.ru_majflt));
        json_object_object_add(args, "ru_nswap", json_object_new_uint64(record->rusage.ru_nswap));
        json_object_object_add(args, "ru_inblock", json_object_new_uint64(record->rusage.ru_inblock));
        json_object_object_add(args, "ru_oublock", json_object_new_uint64(record->rusage.ru_oublock));
        json_object_object_add(args, "ru_msgsnd", json_object_new_uint64(record->rusage.ru_msgsnd));
        json_object_object_add(args, "ru_msgrcv", json_object_new_uint64(record->rusage.ru_msgrcv));
        json_object_object_add(args, "ru_nsignals", json_object_new_uint64(record->rusage.ru_nsignals));
        json_object_object_add(args, "ru_nvcsw", json_object_new_uint64(record->rusage.ru_nvcsw));
        json_object_object_add(args, "ru_nivcsw", json_object_new_uint64(record->rusage.ru_nivcsw));
        json_object_object_add(ctxsw, "args", args);

        json_object_array_add(events, ctxsw);

        // save counter
        thread->pls[PASS_GANTTCHROME] = (void *) &(record->rusage.ru_maxrss);
    }
}

// Register the pass
pass_t pass_ganttchrome = {
    .id   = PASS_GANTTCHROME,
    .name = "gantt-chrome",
    .desc = "Create a gantt chart to be imported in Chrome's about:tracing",
    .dependences = { {AFTER, PASS_OMP}, {AFTER, PASS_MPI} },
    .inorder = {
        .start_callback = start,
        .fini_callback = finish,
        .record_callback = {
            NULL,           // POT_RECORD_OMP_THREAD_BEGIN,
            NULL,           // POT_RECORD_OMP_THREAD_END,
            NULL,           // POT_RECORD_OMP_TASK_IMPLICIT,
            NULL,           // POT_RECORD_OMP_TASK_CREATE,
            NULL,           // POT_RECORD_OMP_TASK_LABEL,
            NULL,           // POT_RECORD_OMP_TASK_SCHEDULE,
            NULL,           // POT_RECORD_OMP_TASK_DELETE,
            NULL,           // POT_RECORD_OMP_TASK_DEPEND,
            NULL,           // POT_RECORD_OMP_TASK_DEPENDENCY,
            NULL,           // POT_RECORD_OMP_TASK_DISCARDED,
            NULL,           // POT_RECORD_OMP_SYNC_BEGIN,
            NULL,           // POT_RECORD_OMP_SYNC_END,
            NULL,           // POT_RECORD_OMP_GROUP_BEGIN,
            NULL,           // POT_RECORD_OMP_GROUP_CANCEL,
            NULL,           // POT_RECORD_OMP_GROUP_END,

            NULL,           // POT_RECORD_MPI_INIT,
            NULL,           // POT_RECORD_MPI_RANK,
            NULL,           // POT_RECORD_MPI_REQ_START,
            NULL,           // POT_RECORD_MPI_REQ_SEND,
            NULL,           // POT_RECORD_MPI_REQ_RECV,
            NULL,           // POT_RECORD_MPI_REQ_ISEND,
            NULL,           // POT_RECORD_MPI_REQ_SEND_INIT,
            NULL,           // POT_RECORD_MPI_REQ_IRECV,
            NULL,           // POT_RECORD_MPI_REQ_PSEND_INIT,
            NULL,           // POT_RECORD_MPI_REQ_PRECV_INIT,
            NULL,           // POT_RECORD_MPI_REQ_WAIT,
            NULL,           // POT_RECORD_MPI_REQ_WAITALL,
            NULL,           // POT_RECORD_MPI_REQ_TEST_SUCCESS,
            NULL,           // POT_RECORD_MPI_REQ_PREADY,

            rusage,         // POT_RECORD_LINUX_RUSAGE,
        },
    },
    .conf = {
        .init = conf_init,
        .load = conf_load,
    }
};
