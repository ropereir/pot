# include <pot/logger.h>

# include "pass.h"
# include "simulator.h"

# include <assert.h>

// Time step
static uint64_t now = 0;
static uint64_t then = 0;

// Metrics
static uint64_t non_overlaped = 0;
static uint64_t overlaped     = 0;

static void
conf_init(json_object * conf)
{

}

static void
conf_load(json_object * conf)
{

}

static void
update(simulator_t * simulator, process_t * process)
{
    pot_record_t * record = process->current_thread->current_record;
    omp_team_t * team = &(process->omp.team);

    now = record->time;
    if (then && then != now)
    {
        int n = process->mpi.counters.requests.started;
        if (n)
        {
            uint64_t dt = (now - then) * n;
            FOREACH_OMP_THREAD_RUNNING_BEGIN(team, thread);
            {
                if (thread->in_explicit_task)
                    overlaped += dt;
                else
                    non_overlaped += dt;
            }
            FOREACH_OMP_THREAD_RUNNING_END(team, thread);
        }
    }
    then = now;
}

static void
start(simulator_t * simulator, process_t * process)
{

}

static void
finish(simulator_t * simulator, process_t * process)
{
    POT_INFO("overlap pass (in s. and %%) - (overlaped, non_overlaped, ratio) = (%lu, %lu, %lf)", overlaped, non_overlaped, overlaped / (double) (overlaped + non_overlaped) * 100.0);
}

// Register the pass
pass_t pass_mpi_omp_task_overlap = {
    .id   = PASS_MPI_OMP_TASK_OVERLAP,
    .name = "mpi-omp-task-overlap",
    .desc = "Compute the overlap of MPI send requests with OpenMP tasks",
    .dependences = { {BEFORE, PASS_OMP}, {BEFORE, PASS_MPI} },
    .inorder = {
        .start_callback = start,
        .fini_callback = finish,
        .record_callback = {
            update,             // POT_RECORD_OMP_THREAD_BEGIN,
            update,             // POT_RECORD_OMP_THREAD_END,
            update,             // POT_RECORD_OMP_TASK_IMPLICIT,
            update,             // POT_RECORD_OMP_TASK_CREATE,
            update,             // POT_RECORD_OMP_TASK_LABEL,
            update,             // POT_RECORD_OMP_TASK_SCHEDULE,
            update,             // POT_RECORD_OMP_TASK_DELETE,
            update,             // POT_RECORD_OMP_TASK_DEPEND,
            update,             // POT_RECORD_OMP_TASK_DEPENDENCY,
            update,             // POT_RECORD_OMP_TASK_DISCARDED,
            update,             // POT_RECORD_OMP_SYNC_BEGIN,
            update,             // POT_RECORD_OMP_SYNC_END,
            update,             // POT_RECORD_OMP_GROUP_BEGIN,
            update,             // POT_RECORD_OMP_GROUP_CANCEL,
            update,             // POT_RECORD_OMP_GROUP_END,

            update,             // POT_RECORD_MPI_INIT,
            update,             // POT_RECORD_MPI_RANK,
            update,             // POT_RECORD_MPI_REQ_START,
            update,             // POT_RECORD_MPI_REQ_SEND,
            update,             // POT_RECORD_MPI_REQ_RECV,
            update,             // POT_RECORD_MPI_REQ_ISEND,
            update,             // POT_RECORD_MPI_REQ_SEND_INIT,
            update,             // POT_RECORD_MPI_REQ_IRECV,
            update,             // POT_RECORD_MPI_REQ_PSEND_INIT,
            update,             // POT_RECORD_MPI_REQ_PRECV_INIT,
            update,             // POT_RECORD_MPI_REQ_WAIT,
            update,             // POT_RECORD_MPI_REQ_WAITALL,
            update,             // POT_RECORD_MPI_REQ_TEST_SUCCESS,
            update,             // POT_RECORD_MPI_REQ_PREADY,

            NULL,               // POT_RECORD_LINUX_RUSAGE,
        },
    },
    .conf = {
        .init = conf_init,
        .load = conf_load,
    }

};
