# include <pot/logger.h>

# include "clo.h"
# include "pass.h"
# include "simulator.h"

# include <assert.h>
# include <string.h>

// config
static char * FILEPATH;
static char SEPARATOR;

// vars
static FILE * FD;
static uint32_t LAST_NSTARTED = -1;

static void
conf_init(json_object * conf)
{
    json_object_object_add(conf, "export", json_object_new_string("mpi-request-started-over-time.csv"));
    json_object_object_add(conf, "separator", json_object_new_string(","));
}

static void
conf_load(json_object * conf)
{
    const char * separator = json_object_get_string(json_object_object_get(conf, "separator"));
    if (separator && separator[0])
        SEPARATOR = separator[0];

    char * exportt = (char *) json_object_get_string(json_object_object_get(conf, "export"));
    assert(exportt);

    // absolute path
    if (*exportt == '/')
    {
        FILEPATH = exportt;
    }
    else
    {
        FILEPATH = (char *) malloc(strlen(CLO.input) + strlen(exportt) + 1);
        strcpy(FILEPATH, CLO.input);
        strcat(FILEPATH, exportt);
    }
}

static void
update(simulator_t * simulator, process_t * process)
{
    if (FD)
    {
        int nstarted = process->mpi.counters.requests.started;
        if (LAST_NSTARTED != nstarted)
        {
            pot_record_t * record = process->current_thread->current_record;
            fprintf(FD, "%lu%c%d\n", record->time, SEPARATOR, nstarted);
            LAST_NSTARTED = nstarted;
        }
    }
}

static void
start(simulator_t * simulator, process_t * process)
{
    FD = fopen(FILEPATH, "w");
    if (FD == NULL)
    {
        POT_ERROR("Failed to create csv file");
        perror(FILEPATH);
        return ;
    }
    fprintf(FD, "%s%c%s\n", "time", SEPARATOR, "n_started_requests");
    POT_INFO("Created file '%s'", FILEPATH);
}

static void
finish(simulator_t * simulator, process_t * process)
{
    fclose(FD);
    POT_INFO("Saved to '%s'", FILEPATH);
}

// Register the pass
pass_t pass_mpi_request_started_over_time = {
    .id   = PASS_MPI_REQUEST_STARTED_OVER_TIME,
    .name = "mpi-request-started-over-time",
    .desc = "Export a .csv file with the number of started MPI requests over time",
    .dependences = { {AFTER, PASS_MPI} },
    .inorder = {
        .start_callback = start,
        .fini_callback = finish,
        .record_callback = {
            NULL,               // POT_RECORD_OMP_THREAD_BEGIN,
            NULL,               // POT_RECORD_OMP_THREAD_END,
            NULL,               // POT_RECORD_OMP_TASK_IMPLICIT,
            NULL,               // POT_RECORD_OMP_TASK_CREATE,
            NULL,               // POT_RECORD_OMP_TASK_LABEL,
            NULL,               // POT_RECORD_OMP_TASK_SCHEDULE,
            NULL,               // POT_RECORD_OMP_TASK_DELETE,
            NULL,               // POT_RECORD_OMP_TASK_DEPEND,
            NULL,               // POT_RECORD_OMP_TASK_DEPENDENCY,
            NULL,               // POT_RECORD_OMP_TASK_DISCARDED,
            NULL,               // POT_RECORD_OMP_SYNC_BEGIN,
            NULL,               // POT_RECORD_OMP_SYNC_END,
            NULL,               // POT_RECORD_OMP_GROUP_BEGIN,
            NULL,               // POT_RECORD_OMP_GROUP_CANCEL,
            NULL,               // POT_RECORD_OMP_GROUP_END,

            NULL,               // POT_RECORD_MPI_INIT,
            NULL,               // POT_RECORD_MPI_RANK,
            update,             // POT_RECORD_MPI_REQ_START,
            update,             // POT_RECORD_MPI_REQ_SEND,
            update,             // POT_RECORD_MPI_REQ_RECV,
            update,             // POT_RECORD_MPI_REQ_ISEND,
            update,             // POT_RECORD_MPI_REQ_SEND_INIT,
            update,             // POT_RECORD_MPI_REQ_IRECV,
            update,             // POT_RECORD_MPI_REQ_PSEND_INIT,
            update,             // POT_RECORD_MPI_REQ_PRECV_INIT,
            update,             // POT_RECORD_MPI_REQ_WAIT,
            update,             // POT_RECORD_MPI_REQ_WAITALL,
            update,             // POT_RECORD_MPI_REQ_TEST_SUCCESS,
            update,             // POT_RECORD_MPI_REQ_PREADY,

            NULL,               // POT_RECORD_LINUX_RUSAGE,
        },
    },
    .conf = {
        .init = conf_init,
        .load = conf_load,
    }

};
