//  A few notes
//
//  Minor
//      - in a partitionned communication, the standard is using 'partition' to
//      name a subset to be transfered, which may be counter-intuitive. In this
//      simulator, we use the term 'partite' for subsets, and 'partition' for
//      the whole request
//
//
//  Major
//      - There is currently no way to detect that a partite had been received from the sender-side
//        It means we cannot trace data movement per-partite.
//        One idea could be to allow MPI_Parrived on the sender-side too, and update the PMPI plugin so it tests each partites and reports their arrival.
//        Maybe we would also simply want a callback on partite arival, similarly to various MPI_Detach proposals, but specifically on a partite
//        Currently, POT computes the overlap as soon as the first partite is marked ready.

# include <pot/logger.h>

# include "pass.h"
# include "simulator.h"
# include "mpi.h"

# include <assert.h>

// virtual runtime management

static mpi_session_t *
mpi_session_get(process_t * process)
{
    return &(process->mpi.session);
}

static mpi_datatype_t *
mpi_datatype_get(
    process_t * process,
    pot_mpi_datatype_t uid
) {
    unsigned hashv;
    HASH_VALUE(&uid, sizeof(pot_mpi_datatype_t), hashv);

    mpi_datatype_t * datatype;
    HASH_FIND_BYHASHVALUE(hh, process->mpi.datatypes, &uid, sizeof(pot_mpi_datatype_t), hashv, datatype);

    return datatype;
}

static mpi_communicator_t *
mpi_communicator_get(
    process_t * process,
    pot_mpi_communicator_t uid
) {
    mpi_session_t * session = mpi_session_get(process);
    assert(session);

    unsigned hashv;
    HASH_VALUE(&uid, sizeof(pot_mpi_communicator_t), hashv);

    mpi_communicator_t * communicator;
    HASH_FIND_BYHASHVALUE(hh, session->communicators, &uid, sizeof(pot_mpi_communicator_t), hashv, communicator);

    return communicator;
}

static mpi_request_t *
mpi_request_get(
    process_t * process,
    pot_mpi_request_t uid
) {
    mpi_session_t * session = mpi_session_get(process);
    assert(session);

    unsigned hashv;
    HASH_VALUE(&uid, sizeof(pot_mpi_request_t), hashv);

    mpi_request_t * req;
    HASH_FIND_BYHASHVALUE(hh, session->requests, &uid, sizeof(pot_mpi_request_t), hashv, req);

    return req;
}

static mpi_request_t *
mpi_request_create(
    process_t * process,
    pot_mpi_request_t req_uid,
    pot_mpi_communicator_t communicator_uid,
    mpi_request_type_t type,
    pot_mpi_datatype_t datatype,
    const void * buf,
    int npartites,
    int count_per_partite,
    int rank,
    int tag,
    mpi_request_persistency_t persistency,
    mpi_request_state_t state
) {
    if (req_uid == process->mpi.consts.requests.null ||
            req_uid == process->mpi.consts.requests.empty ||
            req_uid == 0)
        return NULL;

    mpi_request_t * req = mpi_request_get(process, req_uid);
    if (req)
        POT_FATAL("Tried to create an already existing MPI request (uid=%lu) in state %d", req->uid, req->state);
    else
        req = (mpi_request_t *) malloc(sizeof(mpi_request_t));

    assert(req);
    req->uid = req_uid;
    req->type = type;
    req->state = MPI_REQUEST_STATE_UNINITIALIZED;
    req->datatype = mpi_datatype_get(process, datatype);
    req->buf = buf;
    req->npartites = npartites;
    req->count_per_partite = count_per_partite;
    req->rank = rank;
    req->tag = tag;
    req->persistency = persistency;
    req->communicator = mpi_communicator_get(process, communicator_uid);
    req->state = state;
    req->parrived = 0;

    unsigned hashv;
    HASH_VALUE(&req_uid, sizeof(pot_mpi_request_t), hashv);

    mpi_session_t * session = mpi_session_get(process);
    HASH_ADD_KEYPTR_BYHASHVALUE(hh, session->requests, &req->uid, sizeof(pot_mpi_request_t), hashv, req);

    return req;
}

static void
mpi_request_consume(process_t * process, mpi_request_t * req)
{
    req->state = MPI_REQUEST_STATE_CONSUMED;

    mpi_session_t * session = mpi_session_get(process);
    assert(session);

    HASH_DEL(session->requests, req);
    free(req);
}

static void
mpi_request_recycle(process_t * process, mpi_request_t * req)
{
    req->state = MPI_REQUEST_STATE_INITIALIZED;
}

// pass managment routines
static void
start(simulator_t * simulator, process_t * process)
{
    // TODO : create default session
    // TODO : create default comm world
    // TODO : create default datatype
}

static void
fini(simulator_t * simulator, process_t * process)
{
    assert(process->mpi.counters.requests.started == 0);
}

// records callback

static void
mpi_init(simulator_t * simulator, process_t * process)
{
    pot_record_mpi_init_t * record = (pot_record_mpi_init_t *) process->current_thread->current_record;
    assert(record);

    char * version = (char *) (record + 1);
    POT_INFO("Trace is from MPI with version `%s`", version);
    assert(strlen(version) == record->version_len - 1);

    if (record->required != record->provided)
        POT_WARN("MPI thread support required was `%d` but `%d` was provided", record->required, record->provided);
    else
        POT_INFO("MPI was initialized with threading support `%d`", record->required);

    process->mpi.consts.requests.null = record->requests.null;
    process->mpi.consts.requests.empty = record->requests.empty;
}

static void
mpi_rank(simulator_t * simulator, process_t * process)
{
    pot_record_mpi_rank_t * record = (pot_record_mpi_rank_t *) process->current_thread->current_record;
    assert(record);

    POT_FATAL("impl me");
}

static void
mpi_req_start(simulator_t * simulator, process_t * process)
{
    pot_record_mpi_req_start_t * record = (pot_record_mpi_req_start_t *) process->current_thread->current_record;
    assert(record);

    mpi_request_t * req = mpi_request_get(process, record->uid);
    assert(req);

    assert(req->state == MPI_REQUEST_STATE_INITIALIZED);
    req->state = MPI_REQUEST_STATE_STARTED;
    req->pready = 0;

    ++process->mpi.counters.requests.started;
}

static void
mpi_req_isend(simulator_t * simulator, process_t * process)
{
    pot_record_mpi_req_isend_t * record = (pot_record_mpi_req_isend_t *) process->current_thread->current_record;
    assert(record);

    mpi_request_t * req = mpi_request_create(
        process,
        record->uid,
        record->parent.communicator,
        MPI_REQUEST_TYPE_SEND,
        record->parent.datatype,
        record->parent.buf,
        1,
        record->parent.count,
        record->parent.rank,
        record->parent.tag,
        MPI_REQUEST_NONPERSISTENT,
        MPI_REQUEST_STATE_STARTED
    );

    // if the req is null, it means the copy has been done directly, and OpenMPI returned an empty request
    if (req)
        ++process->mpi.counters.requests.started;
}

static void
mpi_req_send_init(simulator_t * simulator, process_t * process)
{
    pot_record_mpi_req_send_init_t * record = (pot_record_mpi_req_send_init_t *) process->current_thread->current_record;
    assert(record);

    mpi_request_t * req = mpi_request_create(
        process,
        record->uid,
        record->parent.communicator,
        MPI_REQUEST_TYPE_SEND,
        record->parent.datatype,
        record->parent.buf,
        1,
        record->parent.count,
        record->parent.rank,
        record->parent.tag,
        MPI_REQUEST_NONPERSISTENT,
        MPI_REQUEST_STATE_INITIALIZED
    );
    assert(req);
}

static void
mpi_req_irecv(simulator_t * simulator, process_t * process)
{
    pot_record_mpi_req_irecv_t * record = (pot_record_mpi_req_irecv_t *) process->current_thread->current_record;
    assert(record);

    POT_FATAL("impl me");
}

static void
mpi_req_psend_init(simulator_t * simulator, process_t * process)
{
    pot_record_mpi_req_psend_init_t * record = (pot_record_mpi_req_psend_init_t *) process->current_thread->current_record;
    assert(record);

    mpi_request_t * req = mpi_request_create(
        process,
        record->uid,
        record->communicator,
        MPI_REQUEST_TYPE_SEND_PARTITIONNED,
        record->datatype,
        record->buf,
        record->npartites,
        record->count_per_partite,
        record->rank,
        record->tag,
        MPI_REQUEST_PERSISTENT,
        MPI_REQUEST_STATE_INITIALIZED
    );
    assert(req);
}

static void
mpi_req_precv_init(simulator_t * simulator, process_t * process)
{
    pot_record_mpi_req_precv_init_t * record = (pot_record_mpi_req_precv_init_t *) process->current_thread->current_record;
    assert(record);

    POT_FATAL("impl me");
}

static void
mpi_req_send(simulator_t * simulator, process_t * process)
{
    POT_FATAL("impl me");
}

static void
mpi_req_recv(simulator_t * simulator, process_t * process)
{
    POT_FATAL("impl me");
}

static void
mpi_req_wait(simulator_t * simulator, process_t * process)
{
    POT_FATAL("impl me");
}

static void
mpi_req_waitall(simulator_t * simulator, process_t * process)
{
    POT_FATAL("impl me");
}

static void
mpi_req_test_success(simulator_t * simulator, process_t * process)
{
    pot_record_mpi_req_test_success_t * record = (pot_record_mpi_req_test_success_t *) process->current_thread->current_record;
    assert(record);

    mpi_request_t * req = mpi_request_get(process, record->uid);

    if (req)
    {
        assert(req->state == MPI_REQUEST_STATE_STARTED);
        if (req->persistency == MPI_REQUEST_NONPERSISTENT)
            mpi_request_consume(process, req);
        else
            mpi_request_recycle(process, req);

        --process->mpi.counters.requests.started;
    }
    else
    {
        // with openmpi, it means the task was consumed right on the 'isend' or 'start'
    }
}

static void
mpi_req_pready(simulator_t * simulator, process_t * process)
{
    pot_record_mpi_req_pready_t * record = (pot_record_mpi_req_pready_t *) process->current_thread->current_record;
    assert(record);

    mpi_request_t * req = mpi_request_get(process, record->uid);
    assert(req);
    assert(req->state == MPI_REQUEST_STATE_STARTED);
    assert(req->pready < req->npartites);
    assert(req->type == MPI_REQUEST_TYPE_SEND_PARTITIONNED);

    req->pready++;
}

// the pass
pass_t pass_mpi = {
    .id   = PASS_MPI,
    .name = "mpi",
    .desc = "Keep track of MPI objects",
    .dependences = { },
    .inorder = {
        .start_callback = start,
        .fini_callback = fini,
        .record_callback = {
            NULL,                       // POT_RECORD_OMP_THREAD_BEGIN,
            NULL,                       // POT_RECORD_OMP_THREAD_END,
            NULL,                       // POT_RECORD_OMP_TASK_IMPLICIT,
            NULL,                       // POT_RECORD_OMP_TASK_CREATE,
            NULL,                       // POT_RECORD_OMP_TASK_LABEL,
            NULL,                       // POT_RECORD_OMP_TASK_SCHEDULE,
            NULL,                       // POT_RECORD_OMP_TASK_DELETE,
            NULL,                       // POT_RECORD_OMP_TASK_DEPEND,
            NULL,                       // POT_RECORD_OMP_TASK_DEPENDENCY,
            NULL,                       // POT_RECORD_OMP_TASK_DISCARDED,
            NULL,                       // POT_RECORD_OMP_SYNC_BEGIN,
            NULL,                       // POT_RECORD_OMP_SYNC_END,
            NULL,                       // POT_RECORD_OMP_GROUP_BEGIN,
            NULL,                       // POT_RECORD_OMP_GROUP_CANCEL,
            NULL,                       // POT_RECORD_OMP_GROUP_END,

            mpi_init,                   // POT_RECORD_MPI_INIT,
            mpi_rank,                   // POT_RECORD_MPI_RANK,
            mpi_req_start,              // POT_RECORD_MPI_REQ_START,
            mpi_req_send,               // POT_RECORD_MPI_REQ_SEND,
            mpi_req_recv,               // POT_RECORD_MPI_REQ_RECV,
            mpi_req_isend,              // POT_RECORD_MPI_REQ_ISEND,
            mpi_req_send_init,          // POT_RECORD_MPI_REQ_SEND_INIT,
            mpi_req_irecv,              // POT_RECORD_MPI_REQ_IRECV,
            mpi_req_psend_init,         // POT_RECORD_MPI_REQ_PSEND_INIT,
            mpi_req_precv_init,         // POT_RECORD_MPI_REQ_PRECV_INIT,
            mpi_req_wait,               // POT_RECORD_MPI_REQ_WAIT,
            mpi_req_waitall,            // POT_RECORD_MPI_REQ_WAITALL,
            mpi_req_test_success,       // POT_RECORD_MPI_REQ_TEST_SUCCESS,
            mpi_req_pready,             // POT_RECORD_MPI_REQ_PREADY,

            NULL,                       // POT_RECORD_LINUX_RUSAGE,
        },
    },
    .conf = {
        .load = NULL,
        .init = NULL
    }
};
