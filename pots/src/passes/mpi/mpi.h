#ifndef __MPI_H__
# define __MPI_H__

# include <pot/logger.h>
# include <pot/records.h>

# include "uthash.h"

typedef struct  mpi_datatype_s
{
    UT_hash_handle hh;
}               mpi_datatype_t;

typedef enum    mpi_datatype_basic_type_t
{
    MPI_DATATYPE_BASIC_TYPE_INT = 0,
    MPI_DATATYPE_BASIC_TYPE_FLOAT,
    MPI_DATATYPE_BASIC_TYPE_DOUBLE,
    MPI_DATATYPE_BASIC_TYPE_CHAR,
    MPI_DATATYPE_BASIC_TYPE_BYTE
}               mpi_datatype_basic_type_t;

typedef struct  mpi_datatype_basic_s
{
    mpi_datatype_t parent;
    mpi_datatype_basic_type_t type;
}               mpi_datatype_basic_t;

typedef struct  mpi_datatype_derived_s
{
    mpi_datatype_t parent;
    // TODO
}               mpi_datatype_derived_t;

typedef enum    mpi_request_state_e
{
    MPI_REQUEST_STATE_UNINITIALIZED,
    MPI_REQUEST_STATE_INITIALIZED,
    MPI_REQUEST_STATE_STARTED,
    MPI_REQUEST_STATE_CONSUMED,
}               mpi_request_state_t;

typedef enum    mpi_request_type_s
{
    MPI_REQUEST_TYPE_SEND,
    MPI_REQUEST_TYPE_RECV,
    MPI_REQUEST_TYPE_SEND_PARTITIONNED,
    MPI_REQUEST_TYPE_RECV_PARTITIONNED,
}               mpi_request_type_t;

typedef enum    mpi_request_persistency_e
{
    MPI_REQUEST_NONPERSISTENT,
    MPI_REQUEST_PERSISTENT,
}               mpi_request_persistency_t;

struct mpi_request_s;

typedef struct  mpi_communicator_s
{
    // uthash handle
    UT_hash_handle hh;

}               mpi_communicator_t;

typedef struct  mpi_request_s
{
    mpi_request_type_t type;
    pot_mpi_request_t uid;
    mpi_request_state_t state;

    const void * buf;
    int npartites;
    int count_per_partite;
    mpi_datatype_t * datatype;
    union {
        int dest;
        int src;
        int rank;
    };
    int tag;

    mpi_request_persistency_t persistency;
    mpi_communicator_t * communicator;

    union {
        int pready;
        int parrived;
    };

    // uthash handle
    UT_hash_handle hh;

}               mpi_request_t;

typedef struct  mpi_session_s
{
    mpi_communicator_t * communicators;
    struct mpi_request_s * requests;
}               mpi_session_t;

typedef struct  mpi_process_s
{
    mpi_session_t session;
    mpi_datatype_t * datatypes;
    struct {
        struct {
            pot_mpi_request_t null;
            pot_mpi_request_t empty;
        } requests;
    } consts;
    struct {
        struct {
            int started;
        } requests;
    } counters;
}               mpi_process_t;

#endif
