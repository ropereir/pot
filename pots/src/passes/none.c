# include "pass.h"

# include <assert.h>
static_assert(PASS_NONE == 0, "PASS_NONE must be 0, as we rely on global variable being initialized to 0 for dependences");

pass_t pass_none = {
    .id   = PASS_NONE,
    .name = "none",
    .desc = "A pass that does nothing, serve as an example",
    .dependences = {},
    .inorder = {
        .start_callback = NULL,
        .fini_callback = NULL,
        .record_callback = {
            NULL,           // POT_RECORD_OMP_THREAD_BEGIN,
            NULL,           // POT_RECORD_OMP_THREAD_END,
            NULL,           // POT_RECORD_OMP_TASK_IMPLICIT,
            NULL,           // POT_RECORD_OMP_TASK_CREATE,
            NULL,           // POT_RECORD_OMP_TASK_LABEL,
            NULL,           // POT_RECORD_OMP_TASK_SCHEDULE,
            NULL,           // POT_RECORD_OMP_TASK_DELETE,
            NULL,           // POT_RECORD_OMP_TASK_DEPEND,
            NULL,           // POT_RECORD_OMP_TASK_DEPENDENCY,
            NULL,           // POT_RECORD_OMP_TASK_DISCARDED,
            NULL,           // POT_RECORD_OMP_SYNC_BEGIN,
            NULL,           // POT_RECORD_OMP_SYNC_END,
            NULL,           // POT_RECORD_OMP_GROUP_BEGIN,
            NULL,           // POT_RECORD_OMP_GROUP_CANCEL,
            NULL,           // POT_RECORD_OMP_GROUP_END,

            NULL,           // POT_RECORD_MPI_INIT,
            NULL,           // POT_RECORD_MPI_RANK,
            NULL,           // POT_RECORD_MPI_REQ_START,
            NULL,           // POT_RECORD_MPI_REQ_SEND,
            NULL,           // POT_RECORD_MPI_REQ_RECV,
            NULL,           // POT_RECORD_MPI_REQ_ISEND,
            NULL,           // POT_RECORD_MPI_REQ_SEND_INIT,
            NULL,           // POT_RECORD_MPI_REQ_IRECV,
            NULL,           // POT_RECORD_MPI_REQ_PSEND_INIT,
            NULL,           // POT_RECORD_MPI_REQ_PRECV_INIT,
            NULL,           // POT_RECORD_MPI_REQ_WAIT,
            NULL,           // POT_RECORD_MPI_REQ_WAITALL,
            NULL,           // POT_RECORD_MPI_REQ_PREADY,
            NULL,           // POT_RECORD_MPI_REQ_TEST_SUCCESS,

            NULL,           // POT_RECORD_LINUX_RUSAGE,
        },
    },
};
