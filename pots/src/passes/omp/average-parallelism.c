# include <pot/logger.h>

# include "clo.h"
# include "pass.h"
# include "simulator.h"

# include <assert.h>
# include <string.h>

// vars
static uint32_t LAST_N_READY_OR_RUNNING = -1;

static uint64_t THEN = 0,           NOW = 0;
static uint64_t T0 = UINT64_MAX,     TF = 0;

static double SUM = 0.0;

static void
update(simulator_t * simulator, process_t * process)
{
    omp_team_t * team = &(process->omp.team);
    assert(team);

    pot_record_t * record = process->current_thread->current_record;

    // count number of tasks ready or running
    uint64_t ntasks = process->omp.team.nexplicit_tasks_ready;
    FOREACH_OMP_THREAD_RUNNING_BEGIN(team, thread)
    {
        if (thread->in_explicit_task)
            ++ntasks;
    }
    FOREACH_OMP_THREAD_RUNNING_END(team, thread);

    // retrieve current time
    NOW = record->time;

    // the number of tasks ready of running changed
    if (LAST_N_READY_OR_RUNNING != ntasks)
    {
        // integrate on the constant interval
        if (THEN)
        {
            double precision = pot_get_timespec_precision(simulator->timespec);
            double dt = (NOW - THEN) / precision;
            SUM += ntasks * dt;
        }

        // update timers
        THEN = NOW;

        // update last number of ready tasks for integrating
        LAST_N_READY_OR_RUNNING = ntasks;
    }

    // update min/max time
    T0 = (NOW < T0) ? NOW : T0;
    TF = (NOW > TF) ? NOW : TF;
}

static void
finish(simulator_t * simulator, process_t * process)
{
    double precision = pot_get_timespec_precision(simulator->timespec);
    double dt = (TF - T0) / precision;
    double avg = SUM / dt;
    POT_INFO("Average parallelism = %lf over %lf s", avg, dt);
}

static_assert(2 <= PASS_MAX_DEPS, "`PASS_MAX_DEPS` is too small");

// Register the pass
pass_t pass_averageparallelism = {
    .id   = PASS_AVERAGEPARALLELISM,
    .name = "average-parallelism",
    .desc = "Compute the average parallelism",
    .dependences = { {BEFORE, PASS_OMP} },
    .inorder = {
        .start_callback = NULL,
        .fini_callback = finish,
        .record_callback = {
            update,             // POT_RECORD_OMP_THREAD_BEGIN,
            update,             // POT_RECORD_OMP_THREAD_END,
            update,             // POT_RECORD_OMP_TASK_IMPLICIT,
            update,             // POT_RECORD_OMP_TASK_CREATE,
            update,             // POT_RECORD_OMP_TASK_LABEL,
            update,             // POT_RECORD_OMP_TASK_SCHEDULE,
            update,             // POT_RECORD_OMP_TASK_DELETE,
            update,             // POT_RECORD_OMP_TASK_DEPEND,
            update,             // POT_RECORD_OMP_TASK_DEPENDENCY,
            update,             // POT_RECORD_OMP_TASK_DISCARDED,
            update,             // POT_RECORD_OMP_SYNC_BEGIN,
            update,             // POT_RECORD_OMP_SYNC_END,
            update,             // POT_RECORD_OMP_GROUP_BEGIN,
            update,             // POT_RECORD_OMP_GROUP_CANCEL,
            update,             // POT_RECORD_OMP_GROUP_END,

            NULL,               // POT_RECORD_MPI_INIT,
            NULL,               // POT_RECORD_MPI_RANK,
            NULL,               // POT_RECORD_MPI_REQ_START,
            NULL,               // POT_RECORD_MPI_REQ_SEND,
            NULL,               // POT_RECORD_MPI_REQ_RECV,
            NULL,               // POT_RECORD_MPI_REQ_ISEND,
            NULL,               // POT_RECORD_MPI_REQ_SEND_INIT,
            NULL,               // POT_RECORD_MPI_REQ_IRECV,
            NULL,               // POT_RECORD_MPI_REQ_PSEND_INIT,
            NULL,               // POT_RECORD_MPI_REQ_PRECV_INIT,
            NULL,               // POT_RECORD_MPI_REQ_WAIT,
            NULL,               // POT_RECORD_MPI_REQ_WAITALL,
            NULL,               // POT_RECORD_MPI_REQ_TEST_SUCCESS,
            NULL,               // POT_RECORD_MPI_REQ_PREADY,

            NULL,               // POT_RECORD_LINUX_RUSAGE,

        },
    },
    .conf = { NULL }
};
