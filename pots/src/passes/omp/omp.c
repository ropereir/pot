# include <pot/logger.h>

# include <assert.h>
# include <string.h>

# include "simulator.h"

/////////////////////
// PASS INTERFACES //
/////////////////////

void
start(simulator_t * sim, process_t * process)
{
    // init each thread
    omp_team_t * team = &(process->omp.team);
    team->nexplicit_tasks_ready = 0;
    team->nthreads_existing = 0;
    FOREACH_OMP_THREAD_BEGIN(team, thread)
    {
        thread->state = pot_omp_thread_state_not_running;
        thread->current_taskgroup = NULL;
        thread->tasks = NULL;
        thread->tasks_spinlock = 0;
        thread->in_explicit_task = 0;
        thread->current_task_uid = POT_TASK_UID_NULL;
    }
    FOREACH_OMP_THREAD_END(team, thread);
}

void
fini(simulator_t * sim, process_t * process)
{
    if (process->omp.team.nexplicit_tasks_ready)
        POT_ERROR("Simulator ended with %d explicit tasks ready on process %u",
                process->omp.team.nexplicit_tasks_ready, process->pid);
}

/////////////
// HELPERS //
/////////////

// return the task for the given uid
// WARNING : not thread safe is there is concurrent insertion
// There is no issue currently due to the two distinct load phases, but beware !
omp_task_t *
omp_task_get(process_t * process, pot_omp_task_uid_t uid)
{
    omp_thread_t * thread = process->omp.team.threads + uid.tid;

    unsigned hashv;
    HASH_VALUE(&uid.value, sizeof(uint64_t), hashv);

    omp_task_t * task;
    HASH_FIND_BYHASHVALUE(hh, thread->tasks, &uid, sizeof(pot_omp_task_uid_t), hashv, task);

    return task;
}

// Assume omp-thread are bound 1:1 to pthreads
omp_thread_t *
omp_thread_get(process_t * process)
{
    omp_team_t * team = &(process->omp.team);
    return team->threads + process->current_thread->tid;
}

// to call when a task become ready
static void
omp_task_ready(process_t * process, omp_task_t * task)
{
    omp_team_t * team = &(process->omp.team);
    assert(team);

    task->state = pot_omp_task_state_ready;
    if (task->flags.implicit == 0)
        ++team->nexplicit_tasks_ready;
}

// Resolve dependency pred -> succ
static void
omp_task_resolve_dependence(process_t * process, omp_task_t * pred, omp_task_t * succ)
{
    omp_team_t * team = &(process->omp.team);
    assert(team);

    assert(succ->state == pot_omp_task_state_initialized);

    if (--succ->pred_ref == 0)
        omp_task_ready(process, succ);
}


// the task completed
static void
omp_task_completed(process_t * process, omp_task_t * task)
{
    assert(task->state == pot_omp_task_state_cancelled ||
            task->state == pot_omp_task_state_executed ||
            task->state == pot_omp_task_state_detached);

    // check if new tasks are now ready
    ARRAY_FOREACH_BEGIN(&task->successors, omp_task_t **, succ_ptr)
    {
        omp_task_t * succ = *succ_ptr;
        omp_task_resolve_dependence(process, task, succ);
    }
    ARRAY_FOREACH_END(&task->successors, omp_task_t **, succ_ptr);

    task->state = pot_omp_task_state_completed;
}

// create a task
static inline omp_task_t *
omp_task_create(
    process_t * process,
    pot_omp_task_uid_t uid,
    uint64_t time,
    pot_omp_task_flag_t flags,
    int32_t priority,
    int32_t color,
    pot_omp_task_state_t state
) {

    omp_thread_t * thread = process->omp.team.threads + uid.tid;
    assert(thread);

    omp_task_t * parent = omp_task_get(process, thread->current_task_uid);

    omp_task_t * task = (omp_task_t *) malloc(sizeof(omp_task_t));
    assert(task);

    task->taskgroup     = NULL;
    task->create_time   = time;
    task->uid           = uid;
    task->parent        = parent;
    task->accesses      = NULL;
    array_init(&task->successors, 4, sizeof(omp_task_t *));
    array_init(&task->predecessors, 4, sizeof(omp_task_t *));
    task->pred_ref      = 0;
    array_init(&task->reported_successors, 4, sizeof(omp_task_t *));
    array_init(&task->schedules, 1, sizeof(omp_task_schedule_t));
    task->flags         = flags;
    task->priority      = priority;
    task->color         = color;
    task->state         = state;
    task->label         = NULL;

    unsigned hashv;
    HASH_VALUE(&task->uid.value, sizeof(uint64_t), hashv);

    omp_task_t * task_check;
    SPINLOCK_LOCK(thread->tasks_spinlock);
    {
        HASH_FIND_BYHASHVALUE(hh, thread->tasks, &task->uid, sizeof(pot_omp_task_uid_t), hashv, task_check);
        if (task_check)
            POT_WARN("Two tasks created with the same UID (%d, %d)... Ignoring the latest one", task->uid.tid, task->uid.tlid);
        else
            HASH_ADD_KEYPTR_BYHASHVALUE(hh, thread->tasks, &task->uid, sizeof(pot_omp_task_uid_t), hashv, task);

        assert((task_check = omp_task_get(process, task->uid)) != NULL);
    }
    SPINLOCK_UNLOCK(thread->tasks_spinlock);
    assert(task_check->uid.value == task->uid.value);

    return task;
}

// this routine ensures the given task uid was created. If not, then create and return it.
// Motivation behind this is that OMPT allows having 'task_create' or 'task_implicit' events raised after 'task_schedule' events,
static omp_task_t *
omp_ensure_task_exists(process_t * process, pot_omp_task_uid_t uid, pot_omp_task_state_t state)
{
    if (uid.value == POT_TASK_UID_NULL.value)
        return NULL;

    omp_task_t * task = omp_task_get(process, uid);
    if (task == NULL)
    {
        // the 'prev' task didn't received a 'task_create' or 'implicit_task' event.
        // Here, we cannot tell anything about that task.
        // Assume it implicit/tied, which may be wrong.
        // One way to sort it out would be to read the entire trace once before simulating, and pre-instanciate every tasks.
        // Maybe the standard spec should enforce 'task_create' to be raised first
        // Maybe we should read the entire trace
        // No solution is satisfactory to me, keep it like this and raise a warning for now.

        pot_omp_task_flag_t flags = { .implicit = 1, .tied = 0 };
        int priority = 0;   // we don't know yet
        int color = 0;      // we don't know yet
        uint64_t time = process->current_thread->current_record->time;

        POT_WARN("An event occured on task (%d, %d) before a 'task_create' or 'implicit_task' on that same task. Analysis precision may be impacted", uid.tid, uid.tlid);
        task = omp_task_create(process, uid, time, flags, priority, color, state);
    }
    assert(task);

    return task;
}

static void
omp_task_schedule(process_t * process, pot_omp_task_uid_t prev_uid, ompt_task_status_t prev_status, pot_omp_task_uid_t next_uid)
{
    omp_team_t * team = &(process->omp.team);
    assert(team);

    omp_thread_t * thread = omp_thread_get(process);
    assert(thread);

    omp_task_t * prev, * next;

    if (POT_TASK_UID_IS_NULL(prev_uid))
        prev = NULL;
    else
    {
        prev = omp_task_get(process, prev_uid);
        if (prev == NULL)
        {
            pot_omp_task_state_t state = pot_omp_task_state_running;
            prev = omp_ensure_task_exists(process, prev_uid, state);
        }
        assert(prev);
    }

    // ensure 'next' task exists
    if (POT_TASK_UID_IS_NULL(next_uid))
        next = NULL;
    else
    {
        next = omp_task_get(process, next_uid);
        if (next == NULL)
        {
            pot_omp_task_state_t state = pot_omp_task_state_ready;
            next = omp_ensure_task_exists(process, next_uid, state);
        }
        assert(next);
    }

    // TODO : some assertion to ensure coherency here could be cool
    // POT_INFO("Thread %d switched from (%d, %d) to (%d, %d)", process->current_thread->tid, thread->current_task_uid.tid, thread->current_task_uid.tlid, next_uid.tid, next_uid.tlid);
    thread->current_task_uid = next_uid;

    if (prev)
    {
        // save the schedule record
        omp_task_schedule_t * schedule = (omp_task_schedule_t *) array_last(&prev->schedules);
        if (schedule)
        {
            schedule->end = process->current_thread->current_record;
        }
        else
        {
            // save the record
            omp_task_schedule_t * schedule = (omp_task_schedule_t *) array_push(&next->schedules, NULL);
            schedule->begin = process->current_thread->current_record;
            schedule->end   = process->current_thread->current_record;
            schedule->tid   = process->current_thread->tid;
        }

        // compute pot state
        pot_omp_task_state_t prev_state;
        switch (prev_status)
        {
            case (ompt_task_switch):
            case (ompt_task_yield):
            {
                prev_state = pot_omp_task_state_suspended;
                break ;
            }

            case (ompt_task_cancel):
            {
                prev_state = pot_omp_task_state_cancelled;
                break ;
            }

            // TODO : ompt does not allow to distinguish 'executed' and 'completed' yet
            case (ompt_task_complete):
            {
                prev_state = pot_omp_task_state_executed;
                break ;
            }

            case (ompt_task_detach):
            {
                // task is waiting for an allow-completion event to be fulfilled
                prev_state = pot_omp_task_state_detached;
                break ;
            }

            case (ompt_task_early_fulfill):
            case (ompt_task_late_fulfill):
            default:
            {
                POT_FATAL("Impossible occured");
                break ;
            }
        }
        prev->state = prev_state;

        // if the task is explicit
        if (prev->flags.implicit == 0)
        {
            // if the task completed
            if (prev->state == pot_omp_task_state_executed ||
                    prev->state == pot_omp_task_state_cancelled)
            {
                // TODO : what if 'prev' is cancelled and detachable ?
                // case not handled currently, can't even tell if there
                // is a detach event
                //
                //  For now, asusme there is no detach event and jump
                //  to the completed state Jump to the completed state

                omp_task_completed(process, prev);
            }
            else if (prev->state == pot_omp_task_state_detached)
            {
                // nothing to do, will be managed in the late_fulfill
            }
            else
            {
                assert(prev->state == pot_omp_task_state_suspended);
                // POT_WARN("Task (%u, %u) was suspended, which is not
                // supported yet", prev->uid.tid, prev->uid.tlid);
                // assume the task is ready to be reready instantly,
                // which may not be right for the case of taskwait with dependences ?
                prev->state = pot_omp_task_state_ready;
                ++team->nexplicit_tasks_ready;
            }
        }
    }

    thread->in_explicit_task = 0;
    if (next)
    {
        // save the record
        omp_task_schedule_t * schedule = (omp_task_schedule_t *) array_push(&next->schedules, NULL);
        schedule->begin = process->current_thread->current_record;
        schedule->end   = (pot_record_t *) NULL;
        schedule->tid   = process->current_thread->tid;

        // if the task is explicit
        if (next->flags.implicit == 0)
        {
            // transition task
            switch (next->state)
            {
                case (pot_omp_task_state_uninitialized):
                {
                    POT_FATAL("Task (%d, %d) was not ready and got scheduled", next->uid.tid, next->uid.tlid);
                    break ;
                }

                case (pot_omp_task_state_initialized):
                {
                    POT_FATAL("Task (%d, %d) was not ready and got scheduled", next->uid.tid, next->uid.tlid);
                    break ;
                }

                case (pot_omp_task_state_ready):
                {
                    // nothing to do
                    break ;
                }

                case (pot_omp_task_state_running):
                {
                    POT_FATAL("Task (%d, %d) that was already running", next->uid.tid, next->uid.tlid);
                    break ;
                }

                case (pot_omp_task_state_cancelled):
                {
                    POT_FATAL("Task %lu that was scheduled after being cancelled", next->uid.value);
                    break ;
                }

                case (pot_omp_task_state_suspended):
                {
                    POT_INFO("Task %lu was suspended, and is now running before being ready", next->uid.value);
                    break ;
                }

                case (pot_omp_task_state_executed):
                {
                    POT_FATAL("Task %lu already executed", next->uid.value);
                    break ;
                }

                case (pot_omp_task_state_detached):
                {
                    POT_FATAL("Task %lu got scheduled after being detached", next->uid.value);
                    break ;
                }

                case (pot_omp_task_state_completed):
                {
                    POT_FATAL("Task %lu got scheduled again after completing", next->uid.value);
                    break ;
                }

                case (pot_omp_task_state_deleted):
                {
                    POT_FATAL("Task %lu got scheduled again after being deleted", next->uid.value);
                    break ;
                }

                default:
                {
                    POT_FATAL("wtf ?");
                    break ;
                }
            }
            assert(next->state == pot_omp_task_state_ready);
            next->state = pot_omp_task_state_running;
            --team->nexplicit_tasks_ready;
            thread->in_explicit_task = 1;
        }
    }
}

// return the accesses for the given address
static inline omp_task_accesses_t *
omp_task_accesses_get(omp_task_t * parent, uint64_t addr)
{
    unsigned hashv;
    HASH_VALUE(&addr, sizeof(uint64_t), hashv);

    omp_task_accesses_t * accesses;
    HASH_FIND_BYHASHVALUE(hh, parent->accesses, &addr, sizeof(uint64_t), hashv, accesses);

    if (accesses == NULL)
    {
        accesses = (omp_task_accesses_t *) malloc(sizeof(omp_task_accesses_t));
        accesses->addr          = addr;
        accesses->out           = NULL;
        accesses->last_out      = NULL;
        accesses->last_in       = NULL;
        accesses->last_outset   = NULL;
        array_init(&accesses->ins, 0, sizeof(omp_task_t *));
        array_init(&accesses->outsets, 0, sizeof(omp_task_t *));

        HASH_ADD_KEYPTR_BYHASHVALUE(hh, parent->accesses, &(accesses->addr), sizeof(uint64_t), hashv, accesses);
    }
    assert(accesses);

    return accesses;
}

// return true if the given task access is redundant for the given address
static int
omp_task_access_is_redundant(
    omp_task_t * task,
    omp_task_accesses_t * accesses,
    uint64_t addr,
    pot_omp_task_depend_type_t type)
{
    switch (type)
    {
        case (POT_DEPEND_TYPE_IN):
        {
            if (accesses->last_out == task || accesses->last_in == task)
                return 1;
            accesses->last_in = task;
            return 0;
        }

        case (POT_DEPEND_TYPE_OUT):
        {
            if (accesses->last_out == task)
                return 1;
            accesses->last_out = task;
            return 0;
        }

        case (POT_DEPEND_TYPE_OUTSET):
        {
            if (accesses->last_out == task || accesses->last_outset == task)
                return 1;
            accesses->last_outset = task;
            return 0;
        }

        default:
        {
            assert("Unknown task depend type" && 0);
            return 0;
        }
    }
}

// set the edge 'pred' -> 'succ'
static inline void
omp_task_set_edge(omp_task_t * pred, omp_task_t * succ)
{
    assert(pred != succ);

    // filter out multiple edges
    omp_task_t ** tasks = (omp_task_t **) array_last(&pred->successors);
    omp_task_t * last = tasks ? tasks[0] : NULL;
    if (last == succ)
        return ;
    array_push(&pred->successors, &succ);
    array_push(&succ->predecessors, &pred);

    if (pred->state < pot_omp_task_state_executed)
        ++succ->pred_ref;
}

static void
omp_task_fulfill_event(
    process_t * process,
    pot_omp_task_uid_t uid,
    ompt_task_status_t status
) {
    # if 0
    omp_task_t * task = omp_ensure_task_exists(process, uid, pot_omp_task_state_initialized);
    assert(task);
    # endif

    omp_task_t * task = omp_task_get(process, uid);
    assert(task);

    switch (status)
    {
        case (ompt_task_early_fulfill):
        {
            // nothing to do
            break ;
        }

        case (ompt_task_late_fulfill):
        {
            omp_task_completed(process, task);
            break ;
        }

        default:
        {
            POT_FATAL("Wrong status in a task_schedule callback from a task-fullfil event");
            break ;
        }
    }
}

/////////////
// RECORDS //
/////////////
void
process_record_omp_thread_begin(
    simulator_t * sim,
    process_t * process
) {
    pot_record_omp_thread_begin_t * record = (pot_record_omp_thread_begin_t *) process->current_thread->current_record;
    assert(record->tid < POT_MAX_THREADS);

    omp_team_t * team = &(process->omp.team);
    if (team->thread_exists[record->tid] == 0)
    {
        team->thread_exists[record->tid] = 1;
        ++team->nthreads_existing;
    }

    omp_thread_t * thread = team->threads + record->tid;
    thread->state = pot_omp_thread_state_running;
}

void
process_record_omp_thread_end(
    simulator_t * sim,
    process_t * process
) {
    pot_record_omp_thread_end_t * record = (pot_record_omp_thread_end_t *) process->current_thread->current_record;
    assert(record->tid < POT_MAX_THREADS);

    omp_team_t * team = &(process->omp.team);

    omp_thread_t * thread = team->threads + record->tid;
    thread->state = pot_omp_thread_state_not_running;
}

void
process_record_omp_task_implicit(
    simulator_t * sim,
    process_t * process
) {
    pot_record_omp_task_implicit_t * record = (pot_record_omp_task_implicit_t *) process->current_thread->current_record;
    pot_omp_task_uid_t uid = { .tid = process->current_thread->tid, .tlid = record->tlid };

    switch (record->endpoint)
    {
        case (ompt_scope_begin):
        {
            omp_task_t * task = omp_task_get(process, uid);
            pot_omp_task_flag_t flags = {
                .implicit = 1,
                .tied = 1
            };
            uint32_t priority = 0;
            uint32_t color = 0;

            if (task)
            {
                POT_WARN("Received an implicit_task begin event after another event related to the task (%d, %d)", uid.tid, uid.tlid);
                task->flags = flags;
                task->priority = priority;
                task->color = color;
            }
            else
            {
                task = omp_task_create(process, uid, record->parent.time, flags, priority, color, pot_omp_task_state_initialized);
                omp_task_ready(process, task);
            }


            omp_thread_t * thread = omp_thread_get(process);
            assert(thread);

            omp_task_schedule(process, thread->current_task_uid, ompt_task_switch, uid);

            break ;
        }

        case (ompt_scope_end):
        {
            // nothing to do ?
            break ;
        }

        case (ompt_scope_beginend):
        default:
        {
            POT_FATAL("wtf ?");
            break ;
        }
    }
}

void
process_record_omp_task_create(
    simulator_t * sim,
    process_t * process
) {
    pot_record_omp_task_create_t * record = (pot_record_omp_task_create_t *) process->current_thread->current_record;
    pot_omp_task_uid_t uid = { .tid = process->current_thread->tid, .tlid = record->tlid };

    omp_thread_t * thread = omp_thread_get(process);
    assert(thread);

    omp_task_t * task = omp_task_get(process, uid);
    if (task)
    {
        POT_WARN("Received a task_create event while the task was already scheduled");

        // fix the task info
        // task->create_time = record->parent.time;
        task->flags = record->flags;
        task->priority = record->priority;
        task->color = record->color;
        task->pred_ref = 0;

        thread->in_explicit_task = 1;
    }
    else
    {
        uint64_t time = record->parent.time;
        pot_omp_task_flag_t flags = record->flags;
        int priority = record->priority;
        int color = record->color;
        pot_omp_task_state_t state = pot_omp_task_state_initialized;

        task = omp_task_create(process, uid, time, flags, priority, color, state);
        if (record->has_dependences == 0)
            omp_task_ready(process, task);
        else
        {
            // wait until dependences are passed to determine whether the task is ready or not
            task->pred_ref = 1;
        }
    }
    assert(task);

    omp_taskgroup_t * taskgroup = thread->current_taskgroup;
    task->taskgroup = taskgroup;
    if (taskgroup)
        ++taskgroup->ntasks;
}

void
process_record_omp_task_label(
    simulator_t * sim,
    process_t * process
) {
    pot_record_omp_task_label_t * record = (pot_record_omp_task_label_t *) process->current_thread->current_record;
    pot_omp_task_uid_t uid = { .tid = process->current_thread->tid, .tlid = record->tlid };
    omp_task_t * task = omp_task_get(process, uid);
    assert(task);
    task->label = (char *) (record + 1);
}

void
process_record_omp_task_schedule(
    simulator_t * sim,
    process_t * process
) {
    pot_record_omp_task_schedule_t * record = (pot_record_omp_task_schedule_t *) process->current_thread->current_record;
    if (POT_TASK_UID_IS_NULL(record->next_uid))
        omp_task_fulfill_event(process, record->prev_uid, record->prev_status);
    else
        omp_task_schedule(process, record->prev_uid, record->prev_status, record->next_uid);
}

void
process_record_omp_task_delete(
    simulator_t * sim,
    process_t * process
) {
    POT_FATAL("Not implemented");
}

void
process_record_omp_task_depend(
    simulator_t * sim,
    process_t * process
) {
    pot_record_omp_task_depend_t * record = (pot_record_omp_task_depend_t *) process->current_thread->current_record;
    pot_omp_task_uid_t uid = { .tid = process->current_thread->tid, .tlid = record->tlid };
    uint32_t ndeps         = record->ndeps;
    pot_depend_t * deps    = (pot_depend_t *) (record + 1);

    // retrieve current task and its parent accesses
    omp_task_t * task = omp_task_get(process, uid);
    assert(task);

    omp_thread_t * thread = omp_thread_get(process);
    assert(task->parent->uid.value == thread->current_task_uid.value);

    omp_task_t * parent = omp_task_get(process, thread->current_task_uid);
    assert(parent);

    // for each deps
    for (uint32_t i = 0 ; i < ndeps ; ++i)
    {
        // type: addr
        uint64_t addr                      = deps[i].addr;
        pot_omp_task_depend_type_t type    = deps[i].type;

        // retrieve accesses for this data
        omp_task_accesses_t * accesses = omp_task_accesses_get(parent, addr);
        assert(accesses);

        // filter out redundancies
        if (!omp_task_access_is_redundant(task, accesses, addr, type))
        {
            // infer edge between 'task' and its predecessor

            // case 1.1 - the generated task is dependant of previous 'in'
            if (!array_is_empty(&accesses->ins) && (type == POT_DEPEND_TYPE_OUT || type == POT_DEPEND_TYPE_OUTSET))
            {
                # if 0
                if (type == POT_DEPEND_TYPE_OUTSET)
                {
                    /**
                     * in:      O O O   <- the predecessor
                     *           \|/
                     * out:       X     <- we insert this empty node
                     *           / \
                     * outset:  O   O   <- the task we are inserting
                     */
                    pot_omp_task_flag_t flags = { .implicit = 1, .tied = 0, .outset = 1 };
                    pot_omp_task_uid_t uid = { .tid = thread->tid, .tlid = --thread->next_implicit_omp_task_tlid };
                    accesses->out = task_create(process, uid, parent->uid, record->time, flags, 0, 0);

                    task_schedule_t * schedule = (task_schedule_t *) array_push(&accesses->out->schedules, NULL);
                    schedule->begin = record;
                    schedule->end = record;
                    schedule->tid = thread->tid;

                    ARRAY_FOREACH_BEGIN(&accesses->ins, omp_task_t **, in)
                    {
                        // prevent cyclic deps in case 'task' already had an 'in'
                        // dep type on the same addr previously
                        if (*in != task)
                            omp_task_set_edge(*in, accesses->out);
                    }
                    ARRAY_FOREACH_END(&accesses->ins, omp_task_t **, in);

                    // infer edge between 'task' and its predecessor
                    omp_task_set_edge(accesses->out, task);
                    array_clear(&accesses->ins);
                }
                else
                #endif
                {
                    ARRAY_FOREACH_BEGIN(&accesses->ins, omp_task_t **, in)
                        if (*in != task)
                            omp_task_set_edge(*in, task);
                    ARRAY_FOREACH_END(&accesses->ins, omp_task_t **, in)
                }
            } // 1.1

            // 1.2 - the generated task is dependent of previous 'outset'
            if (!array_is_empty(&accesses->outsets) && (type == POT_DEPEND_TYPE_IN || type == POT_DEPEND_TYPE_OUT))
            {
                # if 0
                if (type == POT_DEPEND_TYPE_IN)
                {
                    /**
                     * outset:          O O O   <- the predecessor
                     *                   \|/
                     * out:               X     <- we insert this empty node
                     *                   / \
                     * in:              O   O   <- the task we are inserting
                     */
                    pot_omp_task_flag_t flags = { .implicit = 1, .tied = 0, .outset = 1 };
                    pot_omp_task_uid_t uid = { .tid = thread->tid, .tlid = --thread->next_implicit_omp_task_tlid };
                    accesses->out = task_create(process, uid, parent->uid, record->time, flags, 0, 0);

                    task_schedule_t * schedule = (task_schedule_t *) array_push(&accesses->out->schedules, NULL);
                    schedule->begin = record;
                    schedule->end = record;
                    schedule->tid = thread->tid;

                    ARRAY_FOREACH_BEGIN(&accesses->outsets, omp_task_t **, outset)
                        omp_task_set_edge(*outset, accesses->out);
                    ARRAY_FOREACH_END(&accesses->outsets, omp_task_t *, outset);

                    omp_task_set_edge(accesses->out, task);
                    array_clear(&accesses->outsets);
                }
                else
                # endif
                {
                    ARRAY_FOREACH_BEGIN(&accesses->outsets, omp_task_t **, outset)
                        omp_task_set_edge(*outset, task);
                    ARRAY_FOREACH_END(&accesses->outsets, omp_task_t **, outset)
                }
            } // 1.2

            // 1.3 - the generated task is dependent of previous 'out'
            if (accesses->out && (type == POT_DEPEND_TYPE_OUT || type == POT_DEPEND_TYPE_IN || type == POT_DEPEND_TYPE_OUTSET))
            {
                if (type == POT_DEPEND_TYPE_OUT &&
                        (!array_is_empty(&accesses->ins) || !array_is_empty(&accesses->outsets)))
                {
                    // nothing to do, the task already depends on a previous 'in'
                    // or 'outset' that depend on the 'accesses->out'
                }
                else
                {
                    omp_task_set_edge(accesses->out, task);
                }
            }

            // save access for future task
            switch (type)
            {
                case (POT_DEPEND_TYPE_IN):
                {
                    array_push(&accesses->ins, &task);
                    break ;
                }

                case (POT_DEPEND_TYPE_OUT):
                {
                    array_clear(&accesses->ins);
                    array_clear(&accesses->outsets);
                    accesses->out = task;
                    break ;
                }

                case (POT_DEPEND_TYPE_OUTSET):
                {
                    array_push(&accesses->outsets, &task);
                    break ;
                }

                default:
                {
                    assert("Unknown task depend type" && 0);
                    break ;
                }
            }
        } // redundant check
    } // for each deps

    if (--task->pred_ref == 0)
        omp_task_ready(process, task);
}

void
process_record_omp_task_dependency(
    simulator_t * sim,
    process_t * process
) {
    // POT_ERROR("Not implemented");
}

void
process_record_omp_task_discarded(
    simulator_t * sim,
    process_t * process
) {
    POT_FATAL("Not implemented");
}

void
process_record_omp_sync_begin(
    simulator_t * sim,
    process_t * process
) {
    // empty dependence hmap
    omp_thread_t * thread = omp_thread_get(process);
    assert(thread);

    omp_task_t * task = omp_task_get(process, thread->current_task_uid);
    if (task)
    {
        omp_task_accesses_t * entry, * tmp;
        HASH_ITER(hh, task->accesses, entry, tmp)
        {
            HASH_DEL(task->accesses, entry);
            free(entry);
        }
        assert(HASH_COUNT(task->accesses) == 0);
        HASH_CLEAR(hh, task->accesses);
    }
}

void
process_record_omp_sync_end(
    simulator_t * sim,
    process_t * process
) {
    // nothing to do
}

void
process_record_omp_group_begin(
    simulator_t * sim,
    process_t * process
) {
    omp_taskgroup_t * taskgroup = (omp_taskgroup_t *) malloc(sizeof(omp_taskgroup_t));
    assert(taskgroup);

    taskgroup->ntasks = 0;

    omp_thread_t * thread = omp_thread_get(process);
    assert(thread);

    thread->current_taskgroup = taskgroup;
}

void
process_record_omp_group_cancel(
    simulator_t * sim,
    process_t * process
) {
    POT_FATAL("Not implemented");
}

void
process_record_omp_group_end(
    simulator_t * sim,
    process_t * process
) {
    omp_thread_t * thread = omp_thread_get(process);
    assert(thread);

    // we do not free the group, it may be referenced in some tasks
    assert(thread->current_taskgroup);
    thread->current_taskgroup = NULL;
}

pass_t pass_omp = {
    .id   = PASS_OMP,
    .name = "omp",
    .desc = "Keep track of OpenMP objects",
    .dependences = { {} },
    .inorder = {
        .start_callback = start,
        .fini_callback = fini,
        .record_callback = {
            process_record_omp_thread_begin,    // POT_RECORD_OMP_THREAD_BEGIN,
            process_record_omp_thread_end,      // POT_RECORD_OMP_THREAD_END,
            process_record_omp_task_implicit,   // POT_RECORD_OMP_TASK_IMPLICIT,
            process_record_omp_task_create,     // POT_RECORD_OMP_TASK_CREATE,
            process_record_omp_task_label,      // POT_RECORD_OMP_TASK_LABEL,
            process_record_omp_task_schedule,   // POT_RECORD_OMP_TASK_SCHEDULE,
            process_record_omp_task_delete,     // POT_RECORD_OMP_TASK_DELETE,
            process_record_omp_task_depend,     // POT_RECORD_OMP_TASK_DEPEND,
            process_record_omp_task_dependency, // POT_RECORD_OMP_TASK_DEPENDENCY,
            process_record_omp_task_discarded,  // POT_RECORD_OMP_TASK_DISCARDED,
            process_record_omp_sync_begin,      // POT_RECORD_OMP_SYNC_BEGIN,
            process_record_omp_sync_end,        // POT_RECORD_OMP_SYNC_END,
            process_record_omp_group_begin,     // POT_RECORD_OMP_GROUP_BEGIN,
            process_record_omp_group_cancel,    // POT_RECORD_OMP_GROUP_CANCEL,
            process_record_omp_group_end,       // POT_RECORD_OMP_GROUP_END,

            NULL,                               // POT_RECORD_MPI_INIT,
            NULL,                               // POT_RECORD_MPI_RANK,
            NULL,                               // POT_RECORD_MPI_REQ_START,
            NULL,                               // POT_RECORD_MPI_REQ_SEND,
            NULL,                               // POT_RECORD_MPI_REQ_RECV,
            NULL,                               // POT_RECORD_MPI_REQ_ISEND,
            NULL,                               // POT_RECORD_MPI_REQ_SEND_INIT,
            NULL,                               // POT_RECORD_MPI_REQ_IRECV,
            NULL,                               // POT_RECORD_MPI_REQ_PSEND_INIT,
            NULL,                               // POT_RECORD_MPI_REQ_PRECV_INIT,
            NULL,                               // POT_RECORD_MPI_REQ_WAIT,
            NULL,                               // POT_RECORD_MPI_REQ_WAITALL,
            NULL,                               // POT_RECORD_MPI_REQ_TEST_SUCCESS,
            NULL,                               // POT_RECORD_MPI_REQ_PREADY,

            NULL,                               // POT_RECORD_LINUX_RUSAGE,

        },
    },
};
