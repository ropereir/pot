#ifndef __OMP_H__
# define __OMP_H__

# include <pot/records.h>
# include <pot/spinlock.h>

# include "array.h"
# include "uthash.h"
# include "pass.h"

# include <stdbool.h>

// task accesses hmap for dependences
struct omp_task_s;
typedef struct  omp_task_accesses_t
{
    // dependency address
    uint64_t addr;

    // last task that had an 'out' for this address
    struct omp_task_s * out;

    // last task that had an 'in' for this address
    array_t ins;

    // last task that had an 'outset' for this address
    array_t outsets;

    // the last successor task that matched the 'out' dependency (for redundancy filtering)
    struct omp_task_s * last_out;

    // the last successor task that matched the 'in' dependency (for redundancy filtering)
    struct omp_task_s * last_in;

    // the last successor task that matched the 'outset' dependency (for redundancy filtering)
    struct omp_task_s * last_outset;

    // hmap handle
    UT_hash_handle hh;
}               omp_task_accesses_t;

typedef struct  omp_task_schedule_s
{
    pot_record_t * begin;
    pot_record_t * end;
    uint32_t tid;
}               omp_task_schedule_t;

typedef struct  omp_taskgroup_s
{
    uint32_t ntasks;
}               omp_taskgroup_t;

typedef struct  omp_event_handle_s
{
    int ref;
}               omp_event_handle_t;

typedef struct  omp_task_s
{
    // task uid
    pot_omp_task_uid_t uid;

    // the taskgroup
    omp_taskgroup_t * taskgroup;

    // parent task
    struct omp_task_s * parent;

    // accesses for dependencies
    omp_task_accesses_t * accesses;

    // successors
    array_t successors;

    // predecessors
    array_t predecessors;

    // successors that were created on completion of the task
    array_t reported_successors;

    // array of task_schedule_t
    array_t schedules;

    // ref counter to detect readiness
    int32_t pred_ref;

    // flags
    pot_omp_task_flag_t flags;

    // the creation timestamp
    uint64_t create_time;

    // state
    pot_omp_task_state_t state;

    // priority
    int32_t priority;

    // label
    char * label;

    // color
    uint32_t color;

    // entry in 'tasks' hmap
    UT_hash_handle hh;

}               omp_task_t;

// thread state
typedef enum    pot_omp_thread_state_e
{
    pot_omp_thread_state_not_running,
    pot_omp_thread_state_running,
}               pot_omp_thread_state_t;

// an omp thread
typedef struct  omp_thread_s
{
    // thread state
    pot_omp_thread_state_t state;

    // current taskgroup
    omp_taskgroup_t * current_taskgroup;

    // tasks create by this thread
    omp_task_t * tasks;
    spinlock_t tasks_spinlock;

    // current task for inorder passes
    pot_omp_task_uid_t current_task_uid;

    // true if in an explicit task
    uint8_t in_explicit_task;

}               omp_thread_t;

// an omp thread
typedef struct  omp_team_s
{
    // list of omp threads in the team
    omp_thread_t threads[POT_MAX_THREADS];
    bool thread_exists[POT_MAX_THREADS];
    atomic_int nthreads_existing;

    // number of tasks in 'ready' state
    uint32_t nexplicit_tasks_ready;

}               omp_team_t;

// an omp process
typedef struct  omp_process_s
{
    // the current team (TODO : pot currently only supports 1 team)
    omp_team_t team;

}               omp_process_t;

#endif /* __OMP_H__ */
