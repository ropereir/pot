# include <pot/logger.h>

# include "clo.h"
# include "pass.h"
# include "simulator.h"

# include <assert.h>
# include <string.h>

// config
static char * FILEPATH;
static char SEPARATOR;

// vars
static FILE * FD;
static uint32_t LAST_NREADY = -1;

static void
conf_init(json_object * conf)
{
    json_object_object_add(conf, "export", json_object_new_string("ready-over-time.csv"));
    json_object_object_add(conf, "separator", json_object_new_string(","));
}

static void
conf_load(json_object * conf)
{
    const char * separator = json_object_get_string(json_object_object_get(conf, "separator"));
    if (separator && separator[0])
        SEPARATOR = separator[0];

    char * exportt = (char *) json_object_get_string(json_object_object_get(conf, "export"));
    assert(exportt);

    // absolute path
    if (*exportt == '/')
    {
        FILEPATH = exportt;
    }
    else
    {
        FILEPATH = (char *) malloc(strlen(CLO.input) + strlen(exportt) + 1);
        strcpy(FILEPATH, CLO.input);
        strcat(FILEPATH, exportt);
    }
}

static void
update(simulator_t * simulator, process_t * process)
{
    if (FD)
    {
        if (LAST_NREADY != process->omp.team.nexplicit_tasks_ready)
        {
            pot_record_t * record = process->current_thread->current_record;
            fprintf(FD, "%lu%c%u\n", record->time, SEPARATOR, process->omp.team.nexplicit_tasks_ready);
            LAST_NREADY = process->omp.team.nexplicit_tasks_ready;
        }
    }
}

static void
start(simulator_t * simulator, process_t * process)
{
    // print file
    FD = fopen(FILEPATH, "w");
    if (FD == NULL)
    {
        POT_ERROR("Failed to create csv file");
        perror(FILEPATH);
        return ;
    }
    fprintf(FD, "%s%c%s\n", "time", SEPARATOR, "nexplicit_tasks_ready");
    POT_INFO("Created file '%s'", FILEPATH);
}

static void
finish(simulator_t * simulator, process_t * process)
{
    fclose(FD);
    POT_INFO("Saved to '%s'", FILEPATH);
}

// Register the pass
pass_t pass_readyovertime = {
    .id   = PASS_READYOVERTIME,
    .name = "ready-over-time",
    .desc = "Export the ready queue variation over time",
    .dependences = { {BEFORE, PASS_OMP} },
    .inorder = {
        .start_callback = start,
        .fini_callback = finish,
        .record_callback = {
            update,             // POT_RECORD_OMP_THREAD_BEGIN,
            update,             // POT_RECORD_OMP_THREAD_END,
            update,             // POT_RECORD_OMP_TASK_IMPLICIT,
            update,             // POT_RECORD_OMP_TASK_CREATE,
            update,             // POT_RECORD_OMP_TASK_LABEL,
            update,             // POT_RECORD_OMP_TASK_SCHEDULE,
            update,             // POT_RECORD_OMP_TASK_DELETE,
            update,             // POT_RECORD_OMP_TASK_DEPEND,
            update,             // POT_RECORD_OMP_TASK_DEPENDENCY,
            update,             // POT_RECORD_OMP_TASK_DISCARDED,
            update,             // POT_RECORD_OMP_SYNC_BEGIN,
            update,             // POT_RECORD_OMP_SYNC_END,
            update,             // POT_RECORD_OMP_GROUP_BEGIN,
            update,             // POT_RECORD_OMP_GROUP_CANCEL,
            update,             // POT_RECORD_OMP_GROUP_END,

            NULL,               // POT_RECORD_MPI_INIT,
            NULL,               // POT_RECORD_MPI_RANK,
            NULL,               // POT_RECORD_MPI_REQ_START,
            NULL,               // POT_RECORD_MPI_REQ_SEND,
            NULL,               // POT_RECORD_MPI_REQ_RECV,
            NULL,               // POT_RECORD_MPI_REQ_ISEND,
            NULL,               // POT_RECORD_MPI_REQ_SEND_INIT,
            NULL,               // POT_RECORD_MPI_REQ_IRECV,
            NULL,               // POT_RECORD_MPI_REQ_PSEND_INIT,
            NULL,               // POT_RECORD_MPI_REQ_PRECV_INIT,
            NULL,               // POT_RECORD_MPI_REQ_WAIT,
            NULL,               // POT_RECORD_MPI_REQ_WAITALL,
            NULL,               // POT_RECORD_MPI_REQ_TEST_SUCCESS,
            NULL,               // POT_RECORD_MPI_REQ_PREADY,

            NULL,               // POT_RECORD_LINUX_RUSAGE,
        },
    },
    .conf = {
        .init = conf_init,
        .load = conf_load,
    }
};
