# include <pot/logger.h>

# include "pass.h"
# include "simulator.h"

static void
finish(simulator_t * simulator, process_t * process)
{
    // graph metrics
    uint32_t ntasks_implicit = 0;
    uint32_t ntasks_explicit = 0;
    uint32_t narcs      = 0;
    uint32_t narcs_rt   = 0;

    FOREACH_PROCESS_BEGIN(simulator, process)
    {
        FOREACH_OMP_TASK_BEGIN(process, task)
        {
            if (task->flags.implicit)
                ++ntasks_implicit;
            else
                ++ntasks_explicit;
            narcs       += task->successors.n;
            narcs_rt    += task->reported_successors.n;
        }
        FOREACH_OMP_TASK_END(process, task);
    }
    FOREACH_PROCESS_END(simulator, process);

    // Show metrics
    POT_INFO("tasks - (total, explicit) = (%u, %u)", ntasks_implicit + ntasks_explicit, ntasks_explicit);
    POT_INFO("edges - (maximum, reported) = (%u, %u)", narcs, narcs_rt);
}

// Register the pass
pass_t pass_stats = {
    .id   = PASS_STATS,
    .name = "stats",
    .desc = "Compute basic tasks statistics",
    .dependences = { {AFTER, PASS_OMP } },
    .inorder = {
        .start_callback = NULL,
        .fini_callback = finish,
        .record_callback = { NULL },
    },
};
