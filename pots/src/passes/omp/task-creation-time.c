# include <pot/logger.h>

# include "pass.h"
# include "simulator.h"

// time on first task create and last task create
static uint64_t t0 = 0;
static uint64_t tf = 0;

static void
update(simulator_t * simulator, process_t * process)
{
    pot_record_t * record = process->current_thread->current_record;
    if (t0 == 0)
        t0 = record->time;
    tf = record->time;
}

static void
finish(simulator_t * simulator, process_t * process)
{
    double precision = pot_get_timespec_precision(simulator->timespec);
    POT_INFO("task creation time - %lf", (tf - t0) / precision);
}

// Register the pass
pass_t pass_taskcreationtime = {
    .id   = PASS_TASKCREATIONTIME,
    .name = "task-creation-time",
    .desc = "Compute the time from the first to the last task create",
    .dependences = { {AFTER, PASS_OMP} },
    .inorder = {
        .start_callback = NULL,
        .fini_callback = finish,
        .record_callback = {
            NULL,               // POT_RECORD_OMP_THREAD_BEGIN,
            NULL,               // POT_RECORD_OMP_THREAD_END,
            NULL,               // POT_RECORD_OMP_THREAD_IMPLICIT,
            update,             // POT_RECORD_OMP_TASK_CREATE,
            NULL,               // POT_RECORD_OMP_TASK_LABEL,
            NULL,               // POT_RECORD_OMP_TASK_SCHEDULE,
            NULL,               // POT_RECORD_OMP_TASK_DELETE,
            NULL,               // POT_RECORD_OMP_TASK_DEPEND,
            NULL,               // POT_RECORD_OMP_TASK_DEPENDENCY,
            NULL,               // POT_RECORD_OMP_TASK_DISCARDED,
            NULL,               // POT_RECORD_OMP_SYNC_BEGIN,
            NULL,               // POT_RECORD_OMP_SYNC_END,
            NULL,               // POT_RECORD_OMP_GROUP_BEGIN,
            NULL,               // POT_RECORD_OMP_GROUP_CANCEL,
            NULL,               // POT_RECORD_OMP_GROUP_END,

            NULL,               // POT_RECORD_MPI_INIT,
            NULL,               // POT_RECORD_MPI_RANK,
            NULL,               // POT_RECORD_MPI_REQ_START,
            NULL,               // POT_RECORD_MPI_REQ_SEND,
            NULL,               // POT_RECORD_MPI_REQ_RECV,
            NULL,               // POT_RECORD_MPI_REQ_ISEND,
            NULL,               // POT_RECORD_MPI_REQ_SEND_INIT,
            NULL,               // POT_RECORD_MPI_REQ_IRECV,
            NULL,               // POT_RECORD_MPI_REQ_PSEND_INIT,
            NULL,               // POT_RECORD_MPI_REQ_PRECV_INIT,
            NULL,               // POT_RECORD_MPI_REQ_WAIT,
            NULL,               // POT_RECORD_MPI_REQ_WAITALL,
            NULL,               // POT_RECORD_MPI_REQ_TEST_SUCCESS,
            NULL,               // POT_RECORD_MPI_REQ_PREADY,

            NULL,               // POT_RECORD_LINUX_RUSAGE,
        },
    },
};
