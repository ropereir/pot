# include <pot/logger.h>

# include "clo.h"
# include "pass.h"
# include "simulator.h"

# include <assert.h>
# include <inttypes.h>
# include <string.h>

// config
static char * FILEPATH;
static char SEPARATOR;

static void
conf_init(json_object * conf)
{
    json_object_object_add(conf, "export", json_object_new_string("tikki.csv"));
    json_object_object_add(conf, "separator", json_object_new_string(","));
}

static void
conf_load(json_object * conf)
{
    const char * separator = json_object_get_string(json_object_object_get(conf, "separator"));
    if (separator && separator[0])
        SEPARATOR = separator[0];

    char * exportt = (char *) json_object_get_string(json_object_object_get(conf, "export"));
    assert(exportt);

    // absolute path
    if (*exportt == '/')
    {
        FILEPATH = exportt;
    }
    else
    {
        FILEPATH = (char *) malloc(strlen(CLO.input) + strlen(exportt) + 1);
        strcpy(FILEPATH, CLO.input);
        strcat(FILEPATH, exportt);
    }
}

static void
finish(simulator_t * simulator, process_t * process)
{
    // print file
    POT_INFO("Exporting tikki CSV to %s ...", FILEPATH);

    FILE * f = fopen(FILEPATH, "w");
    if (f == NULL)
    {
        POT_ERROR("Failed to create csv file");
        perror(FILEPATH);
        return ;
    }

    static const char * fields[] = {
        "Resource",
        "Numa",
        "Start",
        "End",
        "Duration",
        "Explicit",
        "Aff",
        "Strict",
        "Tag",
        "Key0",
        "Key1",
        "Name",
        "TaskId",
        "ParentTaskId",
        "Work",
        "Time",
        "Creation",
    };

    int nfields = sizeof(fields) / sizeof(void *);
    for (int i = 0 ; i < nfields - 1 ; ++i)
        fprintf(f, "%s%c", fields[i], SEPARATOR);
    fprintf(f, "%s\n", fields[nfields - 1]);

    FOREACH_PROCESS_BEGIN(simulator, process)
    {
        FOREACH_OMP_TASK_BEGIN(process, task)
        {
            // schedule event
            ARRAY_FOREACH_BEGIN(&task->schedules, omp_task_schedule_t *, schedule)
            {
                if (!schedule->begin || !schedule->end)
                {
                    POT_WARN("Invalid schedule for task (%u, %u), ignoring it", task->uid.tid, task->uid.tlid);
                    continue ;
                }

                fprintf(f, "%u%c", schedule->tid, SEPARATOR);  /* Resource */
                fprintf(f, "0%c", SEPARATOR);  /* Numa */
                fprintf(f, "%lu%c", schedule->begin->time, SEPARATOR);  /* Start */
                fprintf(f, "%lu%c", schedule->end->time, SEPARATOR);    /* End */
                fprintf(f, "%lu%c", schedule->end->time - schedule->begin->time, SEPARATOR); /* Duration */
                fprintf(f, "%d%c", task->flags.implicit ? 0 : 1, SEPARATOR); /* Explicit */
                fprintf(f, "0%c", SEPARATOR); /* Aff */
                fprintf(f, "0%c", SEPARATOR); /* Strict */
                fprintf(f, "0%c", SEPARATOR); /* Tag */
                fprintf(f, "0%c", SEPARATOR); /* Key0 */
                fprintf(f, "0%c", SEPARATOR); /* Key1 */
                fprintf(f, "%s%c", task->label, SEPARATOR); /* Name */
                fprintf(f, "%" PRIu64 "%c", task->uid.value, SEPARATOR); /* TaskId */
                fprintf(f, "%" PRIu64 "%c", task->parent->uid.value, SEPARATOR); /* ParentTaskId */
                fprintf(f, "0%c", SEPARATOR); /* Work */
                fprintf(f, "0%c", SEPARATOR); /* Time */
                fprintf(f, "%lu", task->create_time); /* Creation Time */
                fprintf(f, "\n");
            }
            ARRAY_FOREACH_END(&task->schedules, omp_task_schedule_t *, schedule);
        }
        FOREACH_OMP_TASK_END(process, task);
    }
    FOREACH_PROCESS_END(context, process);

    fclose(f);
}

// Register the pass
pass_t pass_tikki = {
    .id   = PASS_TIKKI,
    .name = "tikki",
    .desc = "Export the trace to tikki .csv format",
    .dependences = { {AFTER, PASS_OMP} },
    .inorder = {
        .start_callback = NULL,
        .fini_callback = finish,
        .record_callback = { NULL },
    },
    .conf = {
        .init = conf_init,
        .load = conf_load,
    }
};
