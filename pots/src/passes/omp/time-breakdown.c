# include <pot/logger.h>

# include "clo.h"
# include "pass.h"
# include "simulator.h"

# include <inttypes.h>
# include <string.h>

# include <assert.h>


// time spent by threads
//  work     - executing explicit tasks
//  overhead - executing implicit tasks with    explicit tasks ready
//  idle     - executing implicit tasks with no explicit tasks ready
static uint64_t work     = 0;
static uint64_t overhead = 0;
static uint64_t idle     = 0;

// Time step
static uint64_t now = 0;
static uint64_t then = 0;

// config
static char * FILEPATH;
static char SEPARATOR;

static void
conf_init(json_object * conf)
{
    json_object_object_add(conf, "export", json_object_new_string("time-breakdown.csv"));
    json_object_object_add(conf, "separator", json_object_new_string(","));
}

static void
conf_load(json_object * conf)
{
    const char * separator = json_object_get_string(json_object_object_get(conf, "separator"));
    if (separator && separator[0])
        SEPARATOR = separator[0];

    char * exportt = (char *) json_object_get_string(json_object_object_get(conf, "export"));
    assert(exportt);

    // absolute path
    if (*exportt == '/')
    {
        FILEPATH = exportt;
    }
    else
    {
        FILEPATH = (char *) malloc(strlen(CLO.input) + strlen(exportt) + 1);
        strcpy(FILEPATH, CLO.input);
        strcat(FILEPATH, exportt);
    }
}

static void
update(simulator_t * simulator, process_t * process)
{
    pot_record_t * record = process->current_thread->current_record;
    omp_team_t * team = &(process->omp.team);

    now = record->time;
    if (then && then != now)
    {
        uint64_t dt = now - then;
        FOREACH_OMP_THREAD_RUNNING_BEGIN(team, thread);
        {
            if (thread->in_explicit_task)
                work += dt;
            else
                if (team->nexplicit_tasks_ready == 0)
                    idle += dt;
                else
                    overhead += dt;
        }
        FOREACH_OMP_THREAD_RUNNING_END(team, thread);
    }
    then = now;
}

static void
start(simulator_t * simulator, process_t * process)
{
    work        = 0;
    overhead    = 0;
    idle        = 0;
}

static void
finish(simulator_t * simulator, process_t * process)
{
    double precision = pot_get_timespec_precision(simulator->timespec);
    POT_INFO("time breakdown pass (in s.) - (work, overhead, idle) = (%lf, %lf, %lf)",
            (double)work/precision, (double)overhead/precision, (double)idle/precision);
    // print file
    POT_INFO("Exporting time breakdown CSV to %s ...", FILEPATH);

    FILE * f = fopen(FILEPATH, "w");
    if (f == NULL)
    {
        POT_ERROR("Failed to create csv file");
        perror(FILEPATH);
        return ;
    }

    static const char * fields[] = {
        "work",
        "overhead",
        "idle",
    };

    int nfields = sizeof(fields) / sizeof(void *);
    for (int i = 0 ; i < nfields - 1 ; ++i)
        fprintf(f, "%s%c", fields[i], SEPARATOR);
    fprintf(f, "%s\n", fields[nfields - 1]);
    fprintf(f, "%lf%c", (double)work/precision, SEPARATOR);
    fprintf(f, "%lf%c",(double)overhead/precision, SEPARATOR);
    fprintf(f, "%lf",(double)idle/precision);
    fprintf(f, "\n");
    fclose(f);
}

// Register the pass
pass_t pass_timebreakdown = {
    .id   = PASS_TIMEBREAKDOWN,
    .name = "time-breakdown",
    .desc = "Compute the work/idle/overhead time breakdown",
    .dependences = { { BEFORE, PASS_OMP } },
    .inorder = {
        .start_callback = start,
        .fini_callback = finish,
        .record_callback = {
            update,             // POT_RECORD_OMP_THREAD_BEGIN,
            update,             // POT_RECORD_OMP_THREAD_END,
            update,             // POT_RECORD_OMP_TASK_IMPLICIT,
            update,             // POT_RECORD_OMP_TASK_CREATE,
            update,             // POT_RECORD_OMP_TASK_LABEL,
            update,             // POT_RECORD_OMP_TASK_SCHEDULE,
            update,             // POT_RECORD_OMP_TASK_DELETE,
            update,             // POT_RECORD_OMP_TASK_DEPEND,
            update,             // POT_RECORD_OMP_TASK_DEPENDENCY,
            update,             // POT_RECORD_OMP_TASK_DISCARDED,
            update,             // POT_RECORD_OMP_SYNC_BEGIN,
            update,             // POT_RECORD_OMP_SYNC_END,
            update,             // POT_RECORD_OMP_GROUP_BEGIN,
            update,             // POT_RECORD_OMP_GROUP_CANCEL,
            update,             // POT_RECORD_OMP_GROUP_END,

            NULL,               // POT_RECORD_MPI_INIT,
            NULL,               // POT_RECORD_MPI_RANK,
            NULL,               // POT_RECORD_MPI_REQ_START,
            NULL,               // POT_RECORD_MPI_REQ_SEND,
            NULL,               // POT_RECORD_MPI_REQ_RECV,
            NULL,               // POT_RECORD_MPI_REQ_ISEND,
            NULL,               // POT_RECORD_MPI_REQ_SEND_INIT,
            NULL,               // POT_RECORD_MPI_REQ_IRECV,
            NULL,               // POT_RECORD_MPI_REQ_PSEND_INIT,
            NULL,               // POT_RECORD_MPI_REQ_PRECV_INIT,
            NULL,               // POT_RECORD_MPI_REQ_WAIT,
            NULL,               // POT_RECORD_MPI_REQ_WAITALL,
            NULL,               // POT_RECORD_MPI_REQ_TEST_SUCCESS,
            NULL,               // POT_RECORD_MPI_REQ_PREADY,

            NULL,               // POT_RECORD_LINUX_RUSAGE,
        },
    },
    .conf = {
        .init = conf_init,
        .load = conf_load,
    }

};
