# include <pot/logger.h>

# include "clo.h"
# include "pass.h"
# include "simulator.h"

# include <assert.h>
# include <inttypes.h>
# include <string.h>

# define NB_MAX_TASKS 256
// config
static char * FILEPATH;
static char SEPARATOR;

struct data_t {
	char* name;
	long unsigned duration;
	char is_explicit;
};

typedef struct data_t data;

static void
conf_init(json_object * conf)
{
	json_object_object_add(conf, "export", json_object_new_string("time-per-task.csv"));
	json_object_object_add(conf, "separator", json_object_new_string(","));
}

static void
conf_load(json_object * conf)
{
	const char * separator = json_object_get_string(json_object_object_get(conf, "separator"));
	if (separator && separator[0])
		SEPARATOR = separator[0];

	char * exportt = (char *) json_object_get_string(json_object_object_get(conf, "export"));
	assert(exportt);

	// absolute path
	if (*exportt == '/')
	{
		FILEPATH = exportt;
	}
	else
	{
		FILEPATH = (char *) malloc(strlen(CLO.input) + strlen(exportt) + 1);
		strcpy(FILEPATH, CLO.input);
		strcat(FILEPATH, exportt);
	}
}

static void
finish(simulator_t * simulator, process_t * process)
{
	double precision = pot_get_timespec_precision(simulator->timespec);
	// print file
	POT_INFO("Exporting time-per-task CSV to %s ...", FILEPATH);

	FILE * f = fopen(FILEPATH, "w");
	if (f == NULL)
	{
		POT_ERROR("Failed to create csv file");
		perror(FILEPATH);
		return ;
	}

	static const char * fields[] = {
		"Name",
		"Duration",
		"Explicit",
	};

	int nfields = sizeof(fields) / sizeof(void *);
	for (int i = 0 ; i < nfields - 1 ; ++i)
		fprintf(f, "%s%c", fields[i], SEPARATOR);
	fprintf(f, "%s\n", fields[nfields - 1]);

	int nb_tasks = 0;
	data* tab_tasks = (data*) malloc(NB_MAX_TASKS * sizeof(data));

	FOREACH_PROCESS_BEGIN(simulator, process)
	{
		FOREACH_OMP_TASK_BEGIN(process, task)
		{
			// schedule event
			int find = 0;
			int task_id = 0;
			char* name = (char*)malloc(256 * sizeof(char));
			for(int i=0; i<nb_tasks; i++)
			{
				if(!task->label)
					strcpy(name, "no-name");
				else
					strcpy(name, task->label);
				if (strcmp(name, tab_tasks[i].name)==0)
				{
					find = 1;
					task_id = i;
					break;
				}
			}

			if (find == 0)
			{
				data d;
				d.name = (char*)malloc(256 * sizeof(char));
				if(!task->label)
					strcpy(d.name, "no-name");
				else
					strcpy(d.name, task->label);
				d.is_explicit = task->flags.implicit ? 0 : 1;
				d.duration = 0;
				task_id = nb_tasks++;
				tab_tasks[task_id] = d;
			}


			ARRAY_FOREACH_BEGIN(&task->schedules, omp_task_schedule_t *, schedule)
			{
				if (!schedule->begin || !schedule->end)
				{
					POT_WARN("Invalid schedule for task (%u, %u), ignoring it", task->uid.tid, task->uid.tlid);
					continue ;
				}
				tab_tasks[task_id].duration += schedule->end->time - schedule->begin->time;
			}
			ARRAY_FOREACH_END(&task->schedules, omp_task_schedule_t *, schedule);
		}
		FOREACH_OMP_TASK_END(process, task);
	}
	FOREACH_PROCESS_END(context, process);

	POT_INFO("Time per task (Precision=%f)", precision);
	for(int i=0; i<nb_tasks; i++)
	{
		POT_INFO("[%d] %s = %f s",tab_tasks[i].is_explicit,tab_tasks[i].name,tab_tasks[i].duration/precision);
		fprintf(f, "%s%c", tab_tasks[i].name, SEPARATOR); /* Name */
		fprintf(f, "%f%c", tab_tasks[i].duration/precision, SEPARATOR); /* Duration */
		fprintf(f, "%d%c", tab_tasks[i].is_explicit, SEPARATOR); /* Explicit */
		fprintf(f, "\n");
	}
	fclose(f);
}

// Register the pass
pass_t pass_timepertask = {
    .id   = PASS_TIMEPERTASK,
	.name = "time-per-task",
	.desc = "Export the trace to time-per-task .csv format",
	.dependences = { { AFTER, PASS_OMP } },
	.inorder = {
		.start_callback = NULL,
		.fini_callback = finish,
		.record_callback = { NULL },
	},
	.conf = {
		.init = conf_init,
		.load = conf_load,
	}
};
