# include <pot/logger.h>
# include <pot/records.h>
# include <pot/spinlock.h>

# include "array.h"
# include "clo.h"
# include "simulator.h"

# include <assert.h>
# include <fcntl.h>
# include <fts.h>
# include <stdint.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <unistd.h>

static void
read_trace_file(simulator_t * simulator, char * filename)
{
    FILE * f = fopen(filename, "r");
    if (f == NULL)
    {
        perror(filename);
        POT_ERROR("Failed to read '%s'", filename);
        return ;
    }

    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    if (fsize < sizeof(pot_file_header_t))
    {
        POT_WARN("'%s' is not a valid POT file", filename);
        return ;
    }

    uint8_t * content = (uint8_t *) malloc(fsize);
    if (!content)
    {
        POT_ERROR("Failed to load '%s' into memory", filename);
        return ;
    }
    fread(content, fsize, 1, f);
    fclose(f);

//    POT_DEBUG("Read file '%s' of size %lu", file, fsize);

    pot_file_header_t * header = (pot_file_header_t *) content;
    if (header->magic != POT_FILE_MAGIC)
    {
        POT_WARN("'%s' is not a valid POT file", filename);
        free(content);
        return ;
    }

    if (header->version != POT_FILE_VERSION)
        POT_WARN("Trace '%s' and post-processor file protocol versions differ", filename);

    if (header->tid >= POT_MAX_THREADS)
    {
        free(content);
        POT_WARN("Trace '%s' has an invalid thread id, it should be >= 0 and < %u\n", filename, POT_MAX_THREADS);
        return ;
    }

    if (simulator->timespec == POT_TIME_SPEC_UNKNOWN)
        simulator->timespec = header->timespec;
    else
    {
        if (simulator->timespec != header->timespec)
        {
            free(content);
            POT_ERROR("Trace file '%s' was recorded with a different timestamp precision, skipping", filename);
            return ;
        }
    }

    // retrieve the process
    unsigned hashv;
    HASH_VALUE(&header->pid, sizeof(uint32_t), hashv);

    process_t * process = NULL;
    SPINLOCK_LOCK(simulator->processes_spinlock);
    {
        HASH_FIND_BYHASHVALUE(hh, simulator->processes, &header->pid, sizeof(uint32_t), hashv, process);
        if (process == NULL)
        {
            process = (process_t *) malloc(sizeof(process_t));
            assert(process);
            memset(process, 0, sizeof(process_t));
            process->pid = header->pid;

            for (uint32_t tid = 0 ; tid < POT_MAX_THREADS ; ++tid)
            {
                thread_t * thread = process->threads + tid;
                thread->tid = tid;
                array_init(&thread->files, 4, sizeof(file_t));
            }

            HASH_ADD_KEYPTR_BYHASHVALUE(hh, simulator->processes, &header->pid, sizeof(uint32_t), hashv, process);
        }
        assert(process);

        // mark thread as existing
        ++process->nthreads_existing;
        process->thread_exists[header->tid] = 1;

        // push thread file
        thread_t * thread = process->threads + header->tid;
        file_t * file = (file_t *) array_push(&thread->files, NULL);
        assert(file);
        file->content = content;
        file->size = fsize;
        file->name = filename;
        file->current_record = NULL;
    }
    SPINLOCK_UNLOCK(simulator->processes_spinlock);
}

static void
read_trace_dir(simulator_t * simulator, char * tracedir)
{
    char * path[2] = { tracedir, NULL };
    int options = FTS_COMFOLLOW | FTS_LOGICAL | FTS_NOCHDIR;

    FTS * ftsp;
    if ((ftsp = fts_open(path, options, NULL)) == NULL)
    {
        perror(tracedir);
        POT_ERROR("Could not traverse %s", tracedir);
        return ;
    }

    /* Initialize ftsp with as many argv[] parts as possible. */
    FTSENT * p = fts_children(ftsp, 0);
    if (p == NULL)
    {
        perror(tracedir);
        POT_ERROR("Could not traverse %s", tracedir);
        return ;
    }
    int empty = 1;

    while ((p = fts_read(ftsp)) != NULL)
    {
        switch (p->fts_info)
        {
            case FTS_F:
            {
                empty = 0;

                char * s = strdup(p->fts_path);
                # pragma omp task default(none) shared(simulator) firstprivate(s)
                    read_trace_file(simulator, s);
                break;
            }

            case FTS_D:
            default:
            {
                break;
            }
        }
    }

    # pragma omp taskwait

    if (empty)
        POT_WARN("Trace directory is empty");

    fts_close(ftsp);
}

void
read_trace(simulator_t * simulator, char * tracedir)
{
    POT_INFO("Reading trace from '%s'", tracedir);
    read_trace_dir(simulator, tracedir);
}
