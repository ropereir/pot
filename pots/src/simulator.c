# include <pot/logger.h>
# include <pot/records.h>
# include <pot/spinlock.h>

# include "array.h"
# include "clo.h"
# include "pass.h"
# include "simulator.h"

# include <assert.h>
# include <stdint.h>
# include <stdlib.h>
# include <string.h>

// return a process for the given pid
process_t *
process_get(simulator_t * simulator, uint32_t pid)
{
    unsigned hashv;
    HASH_VALUE(&pid, sizeof(uint32_t), hashv);

    process_t * process;
    HASH_FIND_BYHASHVALUE(hh, simulator->processes, &pid, sizeof(uint64_t), hashv, process);

    return process;
}

// Utilities to go through records 'in order'
static void
__sim_process_inorder_next_record(
    /* INOUT */ process_t * process
) {
    thread_t * next_thread = NULL;
    file_t * next_file = NULL;
    pot_record_t * next_record = NULL;

    FOREACH_THREAD_BEGIN(process, thread)
    {
        ARRAY_FOREACH_BEGIN(&thread->files, file_t *, file)
        {
            assert(file->content);

            // get the next record for that file of that thread
            pot_record_t * record;
            if (file->current_record == NULL)
                record = (pot_record_t *) (file->content + sizeof(pot_file_header_t));
            else
                record = (pot_record_t *) ((uint8_t *)file->current_record + pot_record_sizeof(file->current_record));

            assert(record);

            // reached end of file
            if ((uint8_t *)record == file->content + file->size)
                continue ;

            // check whether this record is the next in time
            if (next_record == NULL || record->time < next_record->time)
            {
                next_record = record;
                next_thread = thread;
                next_file   = file;
            }
        }
        ARRAY_FOREACH_END(&thread->files, file_t *, file);
    }
    FOREACH_THREAD_END(process, thread);

    // found the next record
    if (next_thread)
    {
        process->current_thread = next_thread;
        next_thread->current_record = next_record;
        next_file->current_record = next_record;
    }
    // all records had been processed
    else
    {
        process->current_thread = NULL;
    }
}

static inline void
__sim_process_instanciate(
   process_t * process
) {
    process->current_thread = NULL;
    memset(process->pls, 0, sizeof(process->pls));

    FOREACH_THREAD_BEGIN(process, thread)
    {
        thread->state = pot_thread_state_not_running;
        thread->current_record = NULL;
        memset(thread->pls, 0, sizeof(thread->pls));
    }
    FOREACH_THREAD_END(process, thread);
}

static void
__sim_process_inorder_each_record(
    simulator_t * simulator,
    process_t * process
) {
    while (1)
    {
        // updates
        //  - 'process->current_process->current_thread'
        //  - 'process->current_process->current_thread->current_record'
        __sim_process_inorder_next_record(process);
        if (process->current_thread == NULL)
            break ;

        thread_t * thread = process->current_thread;
        pot_record_t * record = thread->current_record;
        assert(record);
        assert(record->type >= 0 && record->type < POT_RECORD_TYPE_COUNT);

        // run update passes
        FOREACH_ENABLED_PASS_SORTED_BEGIN(pass)
        {
            if (pass->inorder.record_callback[record->type])
                pass->inorder.record_callback[record->type](simulator, process);
        }
        FOREACH_ENABLED_PASS_END(pass);
    }
}

// Simulate execution from an event to the closest next one on any threads
static void
__sim_process_inorder(simulator_t * simulator, process_t * process)
{
    POT_INFO("In order simulation of process %u", process->pid);

    // prepare simulator
    __sim_process_instanciate(process);
    simulator->current_process = process;

    // init passes
    int has_per_record_callback = 0;
    FOREACH_ENABLED_PASS_SORTED_BEGIN(pass)
    {
        if (pass->inorder.start_callback)
            pass->inorder.start_callback(simulator, process);

        if (!has_per_record_callback)
        {
            for (int i = 0 ; i < POT_RECORD_TYPE_COUNT ; ++i)
                if (pass->inorder.record_callback[i])
                    has_per_record_callback = 1;
        }
    }
    FOREACH_ENABLED_PASS_END(pass);

    // per record passes
    if (has_per_record_callback)
    {
        // create a new process instance and start in-order simulator
        __sim_process_inorder_each_record(simulator, process);
    }

    // finish passes
    FOREACH_ENABLED_PASS_SORTED_BEGIN(pass)
    {
        if (pass->inorder.fini_callback)
            pass->inorder.fini_callback(simulator, process);
    }
    FOREACH_ENABLED_PASS_END(pass);
}

static void
__sim_trace(simulator_t * simulator)
{
    // (imagine a cursor moving along a gantt chart)
    // for each process, play recorded events
    FOREACH_PROCESS_BEGIN(simulator, process)
    {
        # pragma omp task default(none) shared(simulator) firstprivate(process) depend(in: process)
            __sim_process_inorder(simulator, process);
    }
    FOREACH_PROCESS_END(simulator, process);

    # pragma omp taskwait
}

void
simulate(void)
{
    // init simulator
    simulator_t simulator;
    simulator.timespec = POT_TIME_SPEC_UNKNOWN;
    simulator.processes = NULL;
    simulator.processes_spinlock = 0;
    read_trace(&simulator, CLO.input);

    // simulate trace
    __sim_trace(&simulator);
}
