#ifndef __SIMULATOR_H__
# define __SIMULATOR_H__

# include <pot/records.h>
# include <pot/spinlock.h>

# include "array.h"
# include "uthash.h"

# include "pass.h"
# include "passes/omp/omp.h"
# include "passes/mpi/mpi.h"

# include <stdbool.h>

// trace file reading
typedef struct  file_s
{
    // file infos
    uint8_t * content;
    uint64_t size;
    char * name;

    // current record for inorder passes
    pot_record_t * current_record;

}               file_t;

typedef struct  thread_s
{
    // the trace file
    array_t files;

    // the thread id
    uint32_t tid;

    // the thread state
    pot_thread_state_t state;

    // current record for inorder passes
    pot_record_t * current_record;

    // pass local storage
    void * pls[__PASS_COUNT__];

}               thread_t;

// a process
typedef struct  process_s
{
    // process pid
    uint32_t pid;

    // threads
    thread_t threads[POT_MAX_THREADS];
    bool thread_exists[POT_MAX_THREADS];
    atomic_int nthreads_existing;

    // current thread
    thread_t * current_thread;

    // omp info (TODO: move me in PLS)
    omp_process_t omp;

    // mpi info (TODO: move me in PLS)
    mpi_process_t mpi;

    // pass local storage
    void * pls[__PASS_COUNT__];

    // hmap handle
    UT_hash_handle hh;
}               process_t;

typedef struct  simulator_s
{
    // time precision
    pot_time_spec_t timespec;

    // processes hmap
    process_t * processes;
    spinlock_t processes_spinlock;

    // current process
    process_t * current_process;

}               simulator_t;

// for each process
# define FOREACH_PROCESS_BEGIN(CTX, NAME)                                                           \
    do {                                                                                            \
        for (process_t * NAME = (CTX)->processes; NAME != NULL; NAME = (process_t *) NAME->hh.next) \
        {
# define FOREACH_PROCESS_END(CTX, NAME)     \
        }                                   \
    } while(0)

// for each threads (abstract)
# define _FOREACH_THREAD_BEGIN(PROCESS, NAME, T)                                                                    \
    do {                                                                                                            \
        uint32_t threads_checked = 0;                                                                               \
        for (uint32_t tid = 0 ; tid < POT_MAX_THREADS && threads_checked < (PROCESS)->nthreads_existing ; ++tid)    \
        {                                                                                                           \
            T * NAME = (PROCESS)->threads + tid;                                                                    \
            if ((PROCESS)->thread_exists[tid])                                                                      \
                ++threads_checked;                                                                                  \
            else                                                                                                    \
                continue ;

# define _FOREACH_THREAD_END(PROCESS, NAME, T)                                                                      \
        }                                                                                                           \
    } while (0)

# define FOREACH_THREAD_BEGIN(PROCESS, NAME)        _FOREACH_THREAD_BEGIN(PROCESS, NAME, thread_t)
# define FOREACH_THREAD_END(PROCESS, NAME)          _FOREACH_THREAD_END(PROCESS, NAME, thread_t)

# define FOREACH_OMP_THREAD_BEGIN(TEAM, NAME)    _FOREACH_THREAD_BEGIN(TEAM, NAME, omp_thread_t)
# define FOREACH_OMP_THREAD_END(TEAM, NAME)      _FOREACH_THREAD_END(TEAM, NAME, omp_thread_t)

# define FOREACH_OMP_THREAD_RUNNING_BEGIN(TEAM, NAME)               \
    do {                                                            \
        FOREACH_OMP_THREAD_BEGIN(TEAM, NAME);                       \
        {                                                           \
            if (NAME->state == pot_omp_thread_state_not_running)    \
                continue ;
# define FOREACH_OMP_THREAD_RUNNING_END(PROCESS, NAME)              \
        }                                                           \
        FOREACH_OMP_THREAD_END(TEAM, NAME);                         \
    } while (0)

// for each tasks
# define FOREACH_OMP_TASK_BEGIN(PROCESS, NAME)                                                          \
    do {                                                                                                \
        omp_team_t * team = &(PROCESS->omp.team);                                                       \
        FOREACH_OMP_THREAD_BEGIN(team, _thread);                                                        \
        {                                                                                               \
            for (omp_task_t * NAME = _thread->tasks; NAME != NULL; NAME = (omp_task_t *) NAME->hh.next) \
            {
# define FOREACH_OMP_TASK_END(PROCESS, NAME)                                                            \
            }                                                                                           \
        }                                                                                               \
        FOREACH_OMP_THREAD_END(team, _thread);                                                          \
    } while(0)

void simulate(void);
process_t * process_get(simulator_t * simulator, uint32_t pid);
void read_trace(simulator_t * simulator, char * tracedir);

#endif /* __SIMULATOR_H__ */
