# include <pot/logger.h>
# include <pot/records.h>

# include <assert.h>
# include <dlfcn.h>
# include <pthread.h>

static int (*lib_pthread_create)(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg) = NULL;

typedef struct  pot_pthread_arg_s
{
    void * (*start_routine) (void *);
    void * arg;
}               pot_pthread_arg_t;

static void *
pot_pthread_start_routine_override(void * vpot_thread_arg)
{
    pot_pthread_arg_t * pot_thread_arg = (pot_pthread_arg_t *) vpot_thread_arg;

    void * (*start_routine) (void *) = pot_thread_arg->start_routine;
    void * arg = pot_thread_arg->arg;
    free(pot_thread_arg);

    void * r = start_routine(arg);
    pot_thread_flush();

    return r;
}

int
pthread_create(
    pthread_t * thread,
    const pthread_attr_t * attr,
    void * (*start_routine) (void *),
    void * arg
) {
    if (!lib_pthread_create)
        lib_pthread_create = dlsym(RTLD_NEXT, "pthread_create");

    pot_pthread_arg_t * pot_pthread_arg = (pot_pthread_arg_t *) malloc(sizeof(pot_pthread_arg_t));
    assert(pot_pthread_arg);
    pot_pthread_arg->start_routine = start_routine;
    pot_pthread_arg->arg = arg;

    int r = lib_pthread_create(thread, attr, pot_pthread_start_routine_override, pot_pthread_arg);
    return r;
}

static int (*lib_pthread_join)(pthread_t thread, void **retval) = NULL;

int
pthread_join(pthread_t thread, void ** retval)
{
    if (!lib_pthread_join)
        lib_pthread_join = dlsym(RTLD_NEXT, "pthread_join");

    pot_thread_flush();
    int r = lib_pthread_join(thread, retval);
    return r;
}
